package drds.data_propagate.parse;

import drds.data_propagate.common.AbstractLifeCycle;
import drds.data_propagate.entry.Entry;
import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.entry.position.EntryPosition;
import drds.data_propagate.parse.binlog_event_position_manager.AbstractBinLogEventPositionManager;
import drds.data_propagate.parse.exception.ParseException;
import drds.data_propagate.sink.EventSink;
import drds.data_propagate.sink.exception.SinkException;
import org.junit.Ignore;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Ignore
public class MysqlBinlogDumpPerformanceTest {

    public static void main(String args[]) {
        final MysqlEventParser controller = new MysqlEventParser();
        final EntryPosition startPosition = new EntryPosition("mysql-bin.000007", 89796293L, 100L);
        controller.setConnectionCharset(Charset.forName("UTF-8"));
        controller.setSlaveId(3344L);
        controller.setHeartBeatEnable(false);
        controller.setFilterQueryDml(true);
        controller
                .setMasterAuthenticationInfo(new AuthenticationInfo(new InetSocketAddress("100.81.154.142", 3306), "canal", "canal"));
        controller.setMasterPosition(startPosition);
        controller.setEnableTableMetaDataCache(false);
        controller.setDestination("example");
        controller.setTsdbSpringXml("classpath:spring/table_meta_data/h2-table_meta_data.xml");
        // controller.setEventFilter(new AviaterRegexFilter("test\\..*"));
        // controller.setEventBlackFilter(new
        // AviaterRegexFilter("canal_tsdb\\..*"));
        controller.setParallel(true);
        controller.setParallelBufferSize(256);
        controller.setParallelThreadSize(16);
        controller.setGtidMode(false);
        final AtomicLong sum = new AtomicLong(0);
        final AtomicLong last = new AtomicLong(0);
        final AtomicLong start = new AtomicLong(System.currentTimeMillis());
        final AtomicLong end = new AtomicLong(0);
        controller.setEventSink(new AbstractEventSinkTest<List<Entry>>() {

            public boolean sink(List<Entry> entrys, InetSocketAddress remoteAddress, String destination)
                    throws SinkException, InterruptedException {

                sum.addAndGet(entrys.size());
                long current = sum.get();
                if (current - last.get() >= 100000) {
                    end.set(System.currentTimeMillis());
                    long tps = ((current - last.get()) * 1000) / (end.get() - start.get());
                    System.out
                            .println(" total : " + sum + " , cost : " + (end.get() - start.get()) + " , tps : " + tps);
                    last.set(current);
                    start.set(end.get());
                }
                return true;
            }

        });
        controller.setBinLogEventPositionManager(new AbstractBinLogEventPositionManager() {

            @Override
            public BinLogEventPosition getLatestBinLogEventPosition(String destination) {
                return null;
            }

            @Override
            public void persistBinLogEventPosition(String destination, BinLogEventPosition binLogEventPosition) throws ParseException {
            }
        });

        controller.start();

        try {
            Thread.sleep(100 * 1000 * 1000L);
        } catch (InterruptedException e) {
        }
        controller.stop();
    }

    public static abstract class AbstractEventSinkTest<T> extends AbstractLifeCycle implements EventSink<T> {

        public void interrupt() {
        }
    }
}
