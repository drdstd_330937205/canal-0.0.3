package drds.data_propagate.parse.group;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.entry.Entry;
import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.entry.position.EntryPosition;
import drds.data_propagate.parse.AbstractBinlogParser;
import drds.data_propagate.parse.AuthenticationInfo;
import drds.data_propagate.parse.BinlogParser;
import drds.data_propagate.parse.MysqlEventParser;
import drds.data_propagate.parse.binlog_event_position_manager.AbstractBinLogEventPositionManager;
import drds.data_propagate.parse.exception.ParseException;
import drds.data_propagate.sink.entry.EntryEventSink;
import drds.data_propagate.sink.entry.group.GroupEventSink;
import org.junit.Ignore;
import org.junit.Test;

import java.net.InetSocketAddress;

public class GroupEventPaserTest {

    private static final String DETECTING_SQL = "insert into retl.xdual values(1,now()) on duplicateFromEffectiveInitialIndexOffset primaryKey update x=now()";
    private static final String MYSQL_ADDRESS = "127.0.0.1";
    private static final String USERNAME = "xxxxx";
    private static final String PASSWORD = "xxxxx";

    @Ignore
    @Test
    public void testMysqlWithMysql() {
        // MemoryEventStore eventStore = new
        // MemoryEventStore();
        // eventStore.setQueueSize(8196);

        GroupEventSink eventSink = new GroupEventSink(3);
        eventSink.setNotUseTransactionTimelineBarrier(false);
        eventSink.setEventStore(new DummyEventStore());
        eventSink.start();

        // 构造第一个mysql
        MysqlEventParser mysqlEventPaser1 = buildEventParser(3344);
        mysqlEventPaser1.setEventSink(eventSink);
        // 构造第二个mysql
        MysqlEventParser mysqlEventPaser2 = buildEventParser(3345);
        mysqlEventPaser2.setEventSink(eventSink);
        // 构造第二个mysql
        MysqlEventParser mysqlEventPaser3 = buildEventParser(3346);
        mysqlEventPaser3.setEventSink(eventSink);
        // 启动
        mysqlEventPaser1.start();
        mysqlEventPaser2.start();
        mysqlEventPaser3.start();

        try {
            Thread.sleep(30 * 10 * 1000L);
        } catch (InterruptedException e) {
        }

        mysqlEventPaser1.stop();
        mysqlEventPaser2.stop();
        mysqlEventPaser3.stop();
    }

    private MysqlEventParser buildEventParser(int slaveId) {
        MysqlEventParser mysqlEventPaser = new MysqlEventParser();
        EntryPosition defaultPosition = buildPosition("mysql-bin.000001", 6163L, 1322803601000L);
        mysqlEventPaser.setDestination("group-" + slaveId);
        mysqlEventPaser.setSlaveId(slaveId);
        mysqlEventPaser.setHeartBeatEnable(false);
        mysqlEventPaser.setHeartBeatSql(DETECTING_SQL);
        mysqlEventPaser.setMasterAuthenticationInfo(buildAuthentication());
        mysqlEventPaser.setMasterPosition(defaultPosition);
        mysqlEventPaser.setBinlogParser(buildParser(buildAuthentication()));
        mysqlEventPaser.setEventSink(new EntryEventSink());
        mysqlEventPaser.setBinLogEventPositionManager(new AbstractBinLogEventPositionManager() {

            @Override
            public BinLogEventPosition getLatestBinLogEventPosition(String destination) {
                return null;
            }

            @Override
            public void persistBinLogEventPosition(String destination, BinLogEventPosition binLogEventPosition) throws ParseException {
                System.out.println(binLogEventPosition);
            }
        });
        return mysqlEventPaser;
    }

    private BinlogParser buildParser(AuthenticationInfo info) {
        return new AbstractBinlogParser<BinLogEvent>() {

            @Override
            public Entry parse(BinLogEvent binLogEvent, boolean isSeek) throws ParseException {
                return null;
            }
        };
    }

    private EntryPosition buildPosition(String binlogFile, Long offest, Long timestamp) {
        return new EntryPosition(binlogFile, offest, timestamp);
    }

    private AuthenticationInfo buildAuthentication() {
        return new AuthenticationInfo(new InetSocketAddress(MYSQL_ADDRESS, 3306), USERNAME, PASSWORD);
    }
}
