package drds.data_propagate.parse.group;

import drds.data_propagate.entry.EntryType;
import drds.data_propagate.entry.position.Position;
import drds.data_propagate.store.Event;
import drds.data_propagate.store.EventStore;
import drds.data_propagate.store.Events;
import drds.data_propagate.store.StoreException;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class DummyEventStore implements EventStore<Event> {

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String messgae = "{0} [{1}:{2}:{3}]";

    public void ack(Position position) throws StoreException {

    }

    public Events get(Position start, int batchSize) throws InterruptedException, StoreException {
        return null;
    }

    public Events get(Position start, int batchSize, long timeout, TimeUnit unit)
            throws InterruptedException, StoreException {
        return null;
    }

    public Position getFirstBinLogEventPosition() throws StoreException {
        return null;
    }

    public Position getLatestBinLogEventPosition() throws StoreException {
        return null;
    }

    public void rollback() throws StoreException {

    }

    public Events tryGet(Position start, int batchSize) throws StoreException {
        return null;
    }

    public boolean isStart() {
        return false;
    }

    public void start() {

    }

    public void stop() {

    }

    public void cleanAll() throws StoreException {
    }

    public void cleanUntil(Position position) throws StoreException {

    }

    public void put(Event data) throws InterruptedException, StoreException {
        put(Arrays.asList(data));
    }

    public boolean put(Event data, long timeout, TimeUnit unit) throws InterruptedException, StoreException {
        return put(Arrays.asList(data), timeout, unit);
    }

    public boolean tryPutOneTime(Event data) throws StoreException {
        return tryPutOneTime(Arrays.asList(data));
    }

    public void put(List<Event> datas) throws InterruptedException, StoreException {
        for (Event data : datas) {
            Date date = new Date(data.getExecuteTime());
            SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
            if (data.getEntryType() == EntryType.transaction_begin
                    || data.getEntryType() == EntryType.transaction_end) {
                // System.out.println(MessageFormat.format(messgae, new Object[]
                // { Thread.currentThread().getName(),
                // headerPacket.getLogfilename(), headerPacket.getLogfileoffset(),
                // format.format(date),
                // data.getEntry().getEntryType(), "" }));
                System.out.println(data.getEntryType());

            } else {
                System.out.println(MessageFormat.format(messgae, new Object[]{Thread.currentThread().getName(),
                        data.getJournalName(), String.valueOf(data.getPosition()), format.format(date)}));
            }
        }
    }

    public boolean put(List<Event> datas, long timeout, TimeUnit unit) throws InterruptedException, StoreException {
        for (Event data : datas) {
            Date date = new Date(data.getExecuteTime());
            SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
            if (data.getEntryType() == EntryType.transaction_begin
                    || data.getEntryType() == EntryType.transaction_end) {
                // System.out.println(MessageFormat.format(messgae, new Object[]
                // { Thread.currentThread().getName(),
                // headerPacket.getLogfilename(), headerPacket.getLogfileoffset(),
                // format.format(date),
                // data.getEntry().getEntryType(), "" }));
                System.out.println(data.getEntryType());

            } else {
                System.out.println(MessageFormat.format(messgae, new Object[]{Thread.currentThread().getName(),
                        data.getJournalName(), String.valueOf(data.getPosition()), format.format(date)}));
            }
        }
        return true;
    }

    public boolean tryPutOneTime(List<Event> datas) throws StoreException {
        System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        for (Event data : datas) {

            Date date = new Date(data.getExecuteTime());
            SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
            if (data.getEntryType() == EntryType.transaction_begin
                    || data.getEntryType() == EntryType.transaction_end) {
                // System.out.println(MessageFormat.format(messgae, new Object[]
                // { Thread.currentThread().getName(),
                // headerPacket.getLogfilename(), headerPacket.getLogfileoffset(),
                // format.format(date),
                // data.getEntry().getEntryType(), "" }));
                System.out.println(data.getEntryType());

            } else {
                System.out.println(MessageFormat.format(messgae, new Object[]{Thread.currentThread().getName(),
                        data.getJournalName(), String.valueOf(data.getPosition()), format.format(date)}));
            }

        }
        System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
        return true;
    }

}
