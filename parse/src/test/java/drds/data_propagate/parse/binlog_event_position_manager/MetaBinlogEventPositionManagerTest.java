package drds.data_propagate.parse.binlog_event_position_manager;

import drds.data_propagate.common.zookeeper.ZkClientx;
import drds.data_propagate.common.zookeeper.ZookeeperPathUtils;
import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.entry.position.EntryPosition;
import drds.data_propagate.entry.position.PositionRange;
import drds.data_propagate.entry.position.SlavePosition;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;

import java.net.InetSocketAddress;
import java.util.Date;

@Ignore
public class MetaBinlogEventPositionManagerTest extends AbstractLogPositionManagerTest {

    private static final String MYSQL_ADDRESS = "127.0.0.1";
    private ZkClientx zkclientx = new ZkClientx(cluster1 + ";" + cluster2);

    @Before
    public void setUp() {
        String path = ZookeeperPathUtils.getDestinationPath(destination);
        zkclientx.deleteRecursive(path);
    }

    @After
    public void tearDown() {
        String path = ZookeeperPathUtils.getDestinationPath(destination);
        zkclientx.deleteRecursive(path);
    }


    private PositionRange<BinLogEventPosition> buildRange(int number) {
        BinLogEventPosition start = new BinLogEventPosition();
        start.setSlaveId(new SlavePosition(new InetSocketAddress(MYSQL_ADDRESS, 3306), 1234L));
        start.setEntryPosition(new EntryPosition("mysql-bin.000000" + number, 106L, new Date().getTime()));

        BinLogEventPosition end = new BinLogEventPosition();
        end.setSlaveId(new SlavePosition(new InetSocketAddress(MYSQL_ADDRESS, 3306), 1234L));
        end.setEntryPosition(
                new EntryPosition("mysql-bin.000000" + (number + 1), 106L, (new Date().getTime()) + 1000 * 1000L));
        return new PositionRange<BinLogEventPosition>(start, end);
    }
}
