package drds.data_propagate.parse.binlog_event_position_manager;

import drds.data_propagate.entry.position.BinLogEventPosition;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

@Ignore
public class FileMixedBinlogEventPositionManagerTest extends AbstractLogPositionManagerTest {

    private static final String tmp = System.getProperty("java.io.tmpdir", "/tmp");
    private static final File dataDir = new File(tmp, "canal");

    @Before
    public void setUp() {
        try {
            FileUtils.deleteDirectory(dataDir);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testAll() {
        MemoryBinLogEventPositionManager memoryLogPositionManager = new MemoryBinLogEventPositionManager();

        FileMixedBinLogEventPositionManager logPositionManager = new FileMixedBinLogEventPositionManager(dataDir, 1000,
                memoryLogPositionManager);
        logPositionManager.start();

        BinLogEventPosition position2 = doTest(logPositionManager);
        sleep(1500);

        FileMixedBinLogEventPositionManager logPositionManager2 = new FileMixedBinLogEventPositionManager(dataDir, 1000,
                memoryLogPositionManager);
        logPositionManager2.start();

        BinLogEventPosition getPosition2 = logPositionManager2.getLatestBinLogEventPosition(destination);
        Assert.assertEquals(position2, getPosition2);

        logPositionManager.stop();
        logPositionManager2.stop();
    }
}
