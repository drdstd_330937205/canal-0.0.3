package drds.data_propagate.parse.binlog_event_position_manager;

import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class MemoryBinlogEventPositionManagerTest extends AbstractLogPositionManagerTest {

    @Test
    public void testAll() {
        MemoryBinLogEventPositionManager logPositionManager = new MemoryBinLogEventPositionManager();
        logPositionManager.start();
        doTest(logPositionManager);
        logPositionManager.stop();
    }
}
