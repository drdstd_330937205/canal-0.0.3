package drds.data_propagate.parse.binlog_event_position_manager;

import drds.data_propagate.common.zookeeper.ZkClientx;
import drds.data_propagate.common.zookeeper.ZookeeperPathUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class ZooKeeperBinlogEventPositionManagerTest extends AbstractLogPositionManagerTest {

    private ZkClientx zkclientx = new ZkClientx(cluster1 + ";" + cluster2);

    @Before
    public void setUp() {
        String path = ZookeeperPathUtils.getDestinationPath(destination);
        zkclientx.deleteRecursive(path);
    }

    @After
    public void tearDown() {
        String path = ZookeeperPathUtils.getDestinationPath(destination);
        zkclientx.deleteRecursive(path);
    }

    @Ignore
    @Test
    public void testAll() {
        ZooKeeperBinLogEventPositionManager logPositionManager = new ZooKeeperBinLogEventPositionManager(zkclientx);
        logPositionManager.start();

        doTest(logPositionManager);
        logPositionManager.stop();
    }
}
