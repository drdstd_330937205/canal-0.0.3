package drds.data_propagate.parse.binlog_event_position_manager;

import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.entry.position.EntryPosition;
import drds.data_propagate.entry.position.SlavePosition;
import org.junit.Assert;
import org.junit.Ignore;

import java.net.InetSocketAddress;
import java.util.Date;

@Ignore
public class AbstractLogPositionManagerTest extends AbstractZkTest {

    private static final String MYSQL_ADDRESS = "127.0.0.1";

    public BinLogEventPosition doTest(BinLogEventPositionManager binLogEventPositionManager) {
        BinLogEventPosition getPosition = binLogEventPositionManager.getLatestBinLogEventPosition(destination);
        Assert.assertNull(getPosition);

        BinLogEventPosition postion1 = buildPosition(1);
        binLogEventPositionManager.persistBinLogEventPosition(destination, postion1);
        BinLogEventPosition getPosition1 = binLogEventPositionManager.getLatestBinLogEventPosition(destination);
        Assert.assertEquals(postion1, getPosition1);

        BinLogEventPosition postion2 = buildPosition(2);
        binLogEventPositionManager.persistBinLogEventPosition(destination, postion2);
        BinLogEventPosition getPosition2 = binLogEventPositionManager.getLatestBinLogEventPosition(destination);
        Assert.assertEquals(postion2, getPosition2);
        return postion2;
    }

    protected BinLogEventPosition buildPosition(int number) {
        BinLogEventPosition position = new BinLogEventPosition();
        position.setSlaveId(new SlavePosition(new InetSocketAddress(MYSQL_ADDRESS, 3306), 1234L));
        position.setEntryPosition(new EntryPosition("mysql-bin.000000" + number, 106L, new Date().getTime()));
        return position;
    }
}
