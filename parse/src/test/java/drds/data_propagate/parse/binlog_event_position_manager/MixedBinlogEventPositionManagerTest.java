package drds.data_propagate.parse.binlog_event_position_manager;

import drds.data_propagate.common.zookeeper.ZkClientx;
import drds.data_propagate.common.zookeeper.ZookeeperPathUtils;
import drds.data_propagate.entry.position.BinLogEventPosition;
import org.junit.*;

@Ignore
public class MixedBinlogEventPositionManagerTest extends AbstractLogPositionManagerTest {

    private ZkClientx zkclientx = new ZkClientx(cluster1 + ";" + cluster2);

    @Before
    public void setUp() {
        String path = ZookeeperPathUtils.getDestinationPath(destination);
        zkclientx.deleteRecursive(path);
    }

    @After
    public void tearDown() {
        String path = ZookeeperPathUtils.getDestinationPath(destination);
        zkclientx.deleteRecursive(path);
    }

    @Test
    public void testAll() {
        MemoryBinLogEventPositionManager memoryLogPositionManager = new MemoryBinLogEventPositionManager();
        ZooKeeperBinLogEventPositionManager zookeeperLogPositionManager = new ZooKeeperBinLogEventPositionManager(zkclientx);

        MixedBinLogEventPositionManager logPositionManager = new MixedBinLogEventPositionManager(zkclientx);
        logPositionManager.start();

        BinLogEventPosition position2 = doTest(logPositionManager);
        sleep(1000);

        MixedBinLogEventPositionManager logPositionManager2 = new MixedBinLogEventPositionManager(zkclientx);
        logPositionManager2.start();

        BinLogEventPosition getPosition2 = logPositionManager2.getLatestBinLogEventPosition(destination);
        Assert.assertEquals(position2, getPosition2);

        logPositionManager.stop();
        logPositionManager2.stop();
    }
}
