package drds.data_propagate.parse.mysql;

import drds.data_propagate.entry.Entry;
import drds.data_propagate.entry.EntryType;
import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.entry.position.EntryPosition;
import drds.data_propagate.entry.position.SlavePosition;
import drds.data_propagate.parse.AuthenticationInfo;
import drds.data_propagate.parse.MysqlEventParser;
import drds.data_propagate.parse.binlog_event_position_manager.AbstractBinLogEventPositionManager;
import drds.data_propagate.parse.exception.ParseException;
import drds.data_propagate.parse.helper.TimeoutChecker;
import drds.data_propagate.parse.stub.AbstractEventSinkTest;
import drds.data_propagate.sink.exception.SinkException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Ignore
public class MysqlEventParserTest {

    private static final String DETECTING_SQL = "insert into retl.xdual values(1,now()) on duplicateFromEffectiveInitialIndexOffset primaryKey update x=now()";
    private static final String MYSQL_ADDRESS = "127.0.0.1";
    private static final String USERNAME = "canal";
    private static final String PASSWORD = "canal";

    @Test
    public void test_position() throws InterruptedException {
        final TimeoutChecker timeoutChecker = new TimeoutChecker();
        final AtomicLong entryCount = new AtomicLong(0);
        final EntryPosition entryPosition = new EntryPosition();

        final MysqlEventParser controller = new MysqlEventParser();
        final EntryPosition defaultPosition = buildPosition("mysql-bin.000003", 4690L, 1505481064000L);

        controller.setSlaveId(3344L);
        controller.setHeartBeatEnable(true);
        controller.setHeartBeatSql(DETECTING_SQL);
        controller.setMasterPosition(defaultPosition);
        controller.setMasterAuthenticationInfo(buildAuthentication());
        controller.setEventSink(new AbstractEventSinkTest<List<Entry>>() {

            @Override
            public boolean sink(List<Entry> entrys, InetSocketAddress remoteAddress, String destination)
                    throws SinkException {
                for (Entry entry : entrys) {
                    if (entry.getEntryType() != EntryType.heartbeat) {
                        entryCount.incrementAndGet();
                        String logfilename = entry.getEntryHeader().getLogFileName();
                        long logfileoffset = entry.getEntryHeader().getLogfileOffset();
                        long executeTime = entry.getEntryHeader().getExecuteTime();
                        entryPosition.setJournalName(logfilename);
                        entryPosition.setPosition(logfileoffset);
                        entryPosition.setTimestamp(executeTime);
                        break;
                    }
                }

                if (entryCount.get() > 0) {
                    controller.stop();
                    timeoutChecker.stop();
                    timeoutChecker.touch();
                }
                return true;
            }
        });

        controller.setBinLogEventPositionManager(new AbstractBinLogEventPositionManager() {

            @Override
            public BinLogEventPosition getLatestBinLogEventPosition(String destination) {
                return null;
            }

            @Override
            public void persistBinLogEventPosition(String destination, BinLogEventPosition binLogEventPosition) throws ParseException {
                System.out.println(binLogEventPosition);
            }
        });

        controller.start();

        timeoutChecker.waitForIdle();

        if (controller.isStart()) {
            controller.stop();
        }

        // check
        Assert.assertTrue(entryCount.get() > 0);

        // 对比第一条数据和起始的position相同
        Assert.assertEquals(entryPosition, defaultPosition);
    }

    @Test
    public void test_timestamp() throws InterruptedException {
        final TimeoutChecker timeoutChecker = new TimeoutChecker(3000 * 1000);
        final AtomicLong entryCount = new AtomicLong(0);
        final EntryPosition entryPosition = new EntryPosition();

        final MysqlEventParser controller = new MysqlEventParser();
        final EntryPosition defaultPosition = buildPosition(null, null, 1475116855000L);
        controller.setSlaveId(3344L);
        controller.setHeartBeatEnable(false);
        controller.setHeartBeatSql(DETECTING_SQL);
        controller.setMasterAuthenticationInfo(buildAuthentication());
        controller.setMasterPosition(defaultPosition);
        controller.setEventSink(new AbstractEventSinkTest<List<Entry>>() {

            @Override
            public boolean sink(List<Entry> entrys, InetSocketAddress remoteAddress, String destination)
                    throws SinkException {
                for (Entry entry : entrys) {
                    if (entry.getEntryType() != EntryType.heartbeat) {
                        entryCount.incrementAndGet();

                        String logfilename = entry.getEntryHeader().getLogFileName();
                        long logfileoffset = entry.getEntryHeader().getLogfileOffset();
                        long executeTime = entry.getEntryHeader().getExecuteTime();

                        entryPosition.setJournalName(logfilename);
                        entryPosition.setPosition(logfileoffset);
                        entryPosition.setTimestamp(executeTime);
                        break;
                    }
                }

                if (entryCount.get() > 0) {
                    controller.stop();
                    timeoutChecker.stop();
                    timeoutChecker.touch();
                }
                return true;
            }
        });

        controller.setBinLogEventPositionManager(new AbstractBinLogEventPositionManager() {

            public void persistBinLogEventPosition(String destination, BinLogEventPosition binLogEventPosition) {
                System.out.println(binLogEventPosition);
            }

            public BinLogEventPosition getLatestBinLogEventPosition(String destination) {
                return null;
            }
        });

        controller.start();
        timeoutChecker.waitForIdle();

        if (controller.isStart()) {
            controller.stop();
        }

        // check
        Assert.assertTrue(entryCount.get() > 0);

        // 对比第一条数据和起始的position相同
        Assert.assertEquals(entryPosition.getJournalName(), "mysql-bin.000001");
        Assert.assertTrue(entryPosition.getPosition() <= 6163L);
        Assert.assertTrue(entryPosition.getTimestamp() <= defaultPosition.getTimestamp());
    }

    @Test
    public void test_ha() throws InterruptedException {
        final TimeoutChecker timeoutChecker = new TimeoutChecker(30 * 1000);
        final AtomicLong entryCount = new AtomicLong(0);
        final EntryPosition entryPosition = new EntryPosition();

        final MysqlEventParser controller = new MysqlEventParser();
        final EntryPosition defaultPosition = buildPosition("mysql-bin.000001", 6163L, 1322803601000L);
        controller.setSlaveId(3344L);
        controller.setHeartBeatEnable(false);
        controller.setMasterAuthenticationInfo(buildAuthentication());
        controller.setMasterPosition(defaultPosition);
        controller.setEventSink(new AbstractEventSinkTest<List<Entry>>() {

            @Override
            public boolean sink(List<Entry> entrys, InetSocketAddress remoteAddress, String destination)
                    throws SinkException {
                for (Entry entry : entrys) {
                    if (entry.getEntryType() != EntryType.heartbeat) {

                        entryCount.incrementAndGet();

                        String logfilename = entry.getEntryHeader().getLogFileName();
                        long logfileoffset = entry.getEntryHeader().getLogfileOffset();
                        long executeTime = entry.getEntryHeader().getExecuteTime();

                        entryPosition.setJournalName(logfilename);
                        entryPosition.setPosition(logfileoffset);
                        entryPosition.setTimestamp(executeTime);
                        break;
                    }
                }

                if (entryCount.get() > 0) {
                    controller.stop();
                    timeoutChecker.stop();
                    timeoutChecker.touch();
                }
                return true;
            }
        });

        controller.setBinLogEventPositionManager(new AbstractBinLogEventPositionManager() {

            public void persistBinLogEventPosition(String destination, BinLogEventPosition binLogEventPosition) {
                System.out.println(binLogEventPosition);
            }

            public BinLogEventPosition getLatestBinLogEventPosition(String destination) {
                BinLogEventPosition masterBinLogEventPosition = new BinLogEventPosition();
                masterBinLogEventPosition.setSlaveId(new SlavePosition(new InetSocketAddress("127.0.0.1", 3306), 1234L));
                masterBinLogEventPosition.setEntryPosition(new EntryPosition(1322803601000L));
                return masterBinLogEventPosition;
            }
        });

        controller.start();
        timeoutChecker.waitForIdle();

        if (controller.isStart()) {
            controller.stop();
        }

        // check
        Assert.assertTrue(entryCount.get() > 0);

        // 对比第一条数据和起始的position相同
        Assert.assertEquals(entryPosition.getJournalName(), "mysql-bin.000001");
        Assert.assertTrue(entryPosition.getPosition() <= 6163L);
        Assert.assertTrue(entryPosition.getTimestamp() <= defaultPosition.getTimestamp());
    }

    @Test
    public void test_no_position() throws InterruptedException { // 在某个文件下，找不到对应的timestamp数据，会使用106L
        // position进行数据抓取
        final TimeoutChecker timeoutChecker = new TimeoutChecker(3 * 60 * 1000);
        final AtomicLong entryCount = new AtomicLong(0);
        final EntryPosition entryPosition = new EntryPosition();

        final MysqlEventParser controller = new MysqlEventParser();
        final EntryPosition defaultPosition = buildPosition("mysql-bin.000001", null,
                new Date().getTime() + 1000 * 1000L);
        controller.setSlaveId(3344L);
        controller.setHeartBeatEnable(false);
        controller.setMasterAuthenticationInfo(buildAuthentication());
        controller.setMasterPosition(defaultPosition);
        controller.setEventSink(new AbstractEventSinkTest<List<Entry>>() {

            @Override
            public boolean sink(List<Entry> entrys, InetSocketAddress remoteAddress, String destination)
                    throws SinkException {
                for (Entry entry : entrys) {
                    if (entry.getEntryType() != EntryType.heartbeat) {
                        entryCount.incrementAndGet();

                        // String logfilename =
                        // entry.getHeaderPacket().getLogFileName();
                        // long logfileoffset =
                        // entry.getHeaderPacket().getLogfileOffset();
                        long executeTime = entry.getEntryHeader().getExecuteTime();

                        // entryPosition.setJournalName(logfilename);
                        // entryPosition.setPosition(logfileoffset);
                        entryPosition.setTimestamp(executeTime);
                        break;
                    }
                }

                if (entryCount.get() > 0) {
                    controller.stop();
                    timeoutChecker.stop();
                    timeoutChecker.touch();
                }
                return true;
            }
        });

        controller.setBinLogEventPositionManager(new AbstractBinLogEventPositionManager() {

            public void persistBinLogEventPosition(String destination, BinLogEventPosition binLogEventPosition) {
                System.out.println(binLogEventPosition);
            }

            @Override
            public BinLogEventPosition getLatestBinLogEventPosition(String destination) {
                return null;
            }
        });

        controller.start();
        timeoutChecker.waitForIdle();

        if (controller.isStart()) {
            controller.stop();
        }

        // check
        Assert.assertTrue(entryCount.get() > 0);

        // 对比第一条数据和起始的position相同
        // Assert.assertEquals(logfilename, "mysql-bin.000001");
        // Assert.assertEquals(106L, logfileoffset);
        Assert.assertTrue(entryPosition.getTimestamp() < defaultPosition.getTimestamp());
    }

    // ======================== helper method =======================

    private EntryPosition buildPosition(String binlogFile, Long offest, Long timestamp) {
        return new EntryPosition(binlogFile, offest, timestamp);
    }

    private AuthenticationInfo buildAuthentication() {
        return new AuthenticationInfo(new InetSocketAddress(MYSQL_ADDRESS, 3306), USERNAME, PASSWORD);
    }
}
