package drds.data_propagate.parse.mysql;

import drds.data_propagate.entry.*;
import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.entry.position.EntryPosition;
import drds.data_propagate.filter.aviater.AviaterRegexFilter;
import drds.data_propagate.parse.AuthenticationInfo;
import drds.data_propagate.parse.MysqlEventParser;
import drds.data_propagate.parse.binlog_event_position_manager.AbstractBinLogEventPositionManager;
import drds.data_propagate.parse.exception.ParseException;
import drds.data_propagate.parse.stub.AbstractEventSinkTest;
import drds.data_propagate.sink.exception.SinkException;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.List;

@Ignore
public class MysqlDumpTest {

    @Test
    public void testSimple() {
        final MysqlEventParser controller = new MysqlEventParser();
        final EntryPosition startPosition = new EntryPosition("mysql-bin.000001", 4L);
        // startPosition.setGtid("f1ceb61a-a5d5-11e7-bdee-107c3dbcf8a7:1-17");
        controller.setConnectionCharset(Charset.forName("UTF-8"));
        controller.setSlaveId(3344L);
        controller.setHeartBeatEnable(false);
        controller.setMasterAuthenticationInfo(new AuthenticationInfo(new InetSocketAddress("127.0.0.1", 3306), "root", "hello"));
        controller.setMasterPosition(startPosition);
        controller.setEnableTableMetaDataCache(true);
        controller.setDestination("example");
        controller.setTsdbSpringXml("classpath:table_meta_data/h2-table_meta_data.xml");
        controller.setEventFilter(new AviaterRegexFilter("test\\..*"));
        controller.setEventBlackFilter(new AviaterRegexFilter("canal_tsdb\\..*"));
        controller.setParallel(true);
        controller.setParallelBufferSize(256);
        controller.setParallelThreadSize(2);
        controller.setGtidMode(false);
        controller.setEventSink(new AbstractEventSinkTest<List<Entry>>() {

            public boolean sink(List<Entry> entryList, InetSocketAddress remoteAddress, String destination)
                    throws SinkException, InterruptedException {

                for (Entry entry : entryList) {
                    if (entry.getEntryType() == EntryType.transaction_begin
                            || entry.getEntryType() == EntryType.transaction_end
                            || entry.getEntryType() == EntryType.heartbeat) {
                        continue;
                    }

                    RowChange rowChage = null;
                    try {
                        //????rowChage = RowChange.parseFrom(entry.getValue());
                    } catch (Exception e) {
                        throw new RuntimeException(
                                "ERROR ## parser of eromanga-event has an error , data:" + entry.toString(), e);
                    }

                    EventType eventType = rowChage.getEventType();
                    System.out.println(String.format("================> binlog_event[%s:%s] , name[%s,%s] , eventType : %s",
                            entry.getEntryHeader().getLogFileName(), entry.getEntryHeader().getLogfileOffset(),
                            entry.getEntryHeader().getSchemaName(), entry.getEntryHeader().getTableName(), eventType));

                    if (eventType == EventType.query || rowChage.isDdl()) {
                        System.out.println(" sql ----> " + rowChage.getSql());
                    }

                    printXAInfo(rowChage.getPropsList());
                    for (RowData rowData : rowChage.getRowDataList()) {
                        if (eventType == EventType.delete) {
                            print(rowData.getBeforeColumnList());
                        } else if (eventType == EventType.insert) {
                            print(rowData.getAfterColumnList());
                        } else {
                            System.out.println("-------> before");
                            print(rowData.getBeforeColumnList());
                            System.out.println("-------> after");
                            print(rowData.getAfterColumnList());
                        }
                    }
                }

                return true;
            }

        });
        controller.setBinLogEventPositionManager(new AbstractBinLogEventPositionManager() {

            @Override
            public BinLogEventPosition getLatestBinLogEventPosition(String destination) {
                return null;
            }

            @Override
            public void persistBinLogEventPosition(String destination, BinLogEventPosition binLogEventPosition) throws ParseException {
                System.out.println(binLogEventPosition);
            }
        });

        controller.start();

        try {
            Thread.sleep(100 * 1000 * 1000L);
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }
        controller.stop();
    }

    private void print(List<Column> columns) {
        for (Column column : columns) {
            System.out.println(column.getName() + " : " + column.getValue() + "    update=" + column.isUpdated());
        }
    }

    private void printXAInfo(List<KeyValuePair> keyValuePairs) {
        if (keyValuePairs == null) {
            return;
        }

        String xaType = null;
        String xaXid = null;
        for (KeyValuePair keyValuePair : keyValuePairs) {
            String key = keyValuePair.getKey();
            if (StringUtils.endsWithIgnoreCase(key, "XA_TYPE")) {
                xaType = (String) keyValuePair.getValue();
            } else if (StringUtils.endsWithIgnoreCase(key, "XA_XID")) {
                xaXid = (String) keyValuePair.getValue();
            }
        }

        if (xaType != null && xaXid != null) {
            System.out.println(" ------> " + xaType + " " + xaXid);
        }
    }
}
