package drds.data_propagate.parse.stub;

import drds.data_propagate.common.AbstractLifeCycle;
import drds.data_propagate.sink.EventSink;

public abstract class AbstractEventSinkTest<T> extends AbstractLifeCycle implements EventSink<T> {

    public void interrupt() {
        // do nothing
    }
}
