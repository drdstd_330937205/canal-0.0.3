package drds.data_propagate.parse;

import drds.data_propagate.entry.Entry;
import drds.data_propagate.entry.EntryHeader;
import drds.data_propagate.entry.EntryType;
import org.junit.Assert;
import org.junit.Test;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CachedEventListTest {

    private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String messgae = "{0} [{1}:{2}:{3}] {4}.{5}";

    private static Entry buildEntry(String binlogFile, long offset, long timestamp) {
        EntryHeader entryHeaderBuilder = new EntryHeader();
        entryHeaderBuilder.setLogFileName(binlogFile);
        entryHeaderBuilder.setLogfileOffset(offset);
        entryHeaderBuilder.setExecuteTime(timestamp);
        Entry entryBuilder = new Entry();
        entryBuilder.setEntryHeader(entryHeaderBuilder);
        return entryBuilder;
    }

    private static Entry buildEntry(String binlogFile, long offset, long timestamp, EntryType type) {
        EntryHeader entryHeaderBuilder = new EntryHeader();
        entryHeaderBuilder.setLogFileName(binlogFile);
        entryHeaderBuilder.setLogfileOffset(offset);
        entryHeaderBuilder.setExecuteTime(timestamp);
        Entry entryBuilder = new Entry();
        entryBuilder.setEntryHeader(entryHeaderBuilder);
        entryBuilder.setEntryType(type);
        return entryBuilder;
    }

    @Test
    public void testTransactionFlush() {
        final int bufferSize = 64;
        final int transactionSize = 5;
        EventList buffer = new EventList();
        buffer.setQueueSize(bufferSize);
        buffer.setEntryListFlushCallback(new EntryListFlushCallback() {

            public void flush(List<Entry> entryList) throws InterruptedException {
                Assert.assertEquals(transactionSize, entryList.size());
                System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                for (Entry data : entryList) {

                    EntryHeader entryHeader = data.getEntryHeader();
                    Date date = new Date(entryHeader.getExecuteTime());
                    SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
                    if (data.getEntryType() == EntryType.transaction_begin
                            || data.getEntryType() == EntryType.transaction_end) {
                        System.out.println(data.getEntryType());

                    } else {
                        System.out.println(MessageFormat.format(messgae,
                                new Object[]{Thread.currentThread().getName(), entryHeader.getLogFileName(),
                                        entryHeader.getLogfileOffset(), format.format(date), entryHeader.getSchemaName(),
                                        entryHeader.getTableName()}));
                    }

                }
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
            }
        });
        buffer.start();

        try {
            for (int i = 0; i < transactionSize * 10; i++) {
                if (i % transactionSize == 0) {
                    buffer.add(buildEntry("1", 1L + i, 40L + i, EntryType.transaction_begin));
                } else if ((i + 1) % transactionSize == 0) {
                    buffer.add(buildEntry("1", 1L + i, 40L + i, EntryType.transaction_end));
                } else {
                    buffer.add(buildEntry("1", 1L + i, 40L + i));
                }
            }
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }

        buffer.stop();
    }

    @Test
    public void testForceFlush() {
        final int bufferSize = 64;
        EventList buffer = new EventList();
        buffer.setQueueSize(bufferSize);
        buffer.setEntryListFlushCallback(new EntryListFlushCallback() {

            public void flush(List<Entry> entryList) throws InterruptedException {
                Assert.assertEquals(bufferSize, entryList.size());
                System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
                for (Entry data : entryList) {

                    EntryHeader entryHeader = data.getEntryHeader();
                    Date date = new Date(entryHeader.getExecuteTime());
                    SimpleDateFormat format = new SimpleDateFormat(DATE_FORMAT);
                    if (data.getEntryType() == EntryType.transaction_begin
                            || data.getEntryType() == EntryType.transaction_end) {
                        // System.out.println(MessageFormat.format(messgae, new
                        // Object[] {
                        // Thread.currentThread().getName(),
                        // headerPacket.getLogfilename(), headerPacket.getLogfileoffset(),
                        // format.format(date),
                        // data.getEntry().getEntryType(), "" }));
                        System.out.println(data.getEntryType());

                    } else {
                        System.out.println(MessageFormat.format(messgae,
                                new Object[]{Thread.currentThread().getName(), entryHeader.getLogFileName(),
                                        entryHeader.getLogfileOffset(), format.format(date), entryHeader.getSchemaName(),
                                        entryHeader.getTableName()}));
                    }

                }
                System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n");
            }
        });
        buffer.start();

        try {
            for (int i = 0; i < bufferSize * 2 + 1; i++) {
                buffer.add(buildEntry("1", 1L + i, 40L + i));
            }
        } catch (InterruptedException e) {
            Assert.fail(e.getMessage());
        }

        buffer.stop();
    }
}
