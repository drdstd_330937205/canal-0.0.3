package drds.data_propagate.parse;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.BinLogEventDecoder;
import drds.data_propagate.binlog_event.BinLogEventsContext;
import drds.data_propagate.driver.Connector;
import drds.data_propagate.driver.UpdateExecutor;
import drds.data_propagate.driver.packets.HeaderPacket;
import drds.data_propagate.driver.packets.client.command_packet.BinlogDumpPacket;
import drds.data_propagate.driver.utils.PacketManager;
import org.junit.Ignore;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.concurrent.atomic.AtomicLong;

@Ignore
public class MysqlBinlogEventPerformanceTest {

    protected static Charset charset = Charset.forName("utf-8");

    public static void main(String args[]) {
        PacketReader packetReader = new PacketReader();
        try {
            Connector connector = new Connector(new InetSocketAddress("127.0.0.1", 3306), "root", "hello");
            connector.connect();
            updateSettings(connector);
            sendBinlogDump(connector, "mysql-bin.000006", 120L, 3);
            packetReader.start(connector.getSocketChannel());
            BinLogEventDecoder binLogEventDecoder = new BinLogEventDecoder(BinLogEvent.unknown_event, BinLogEvent.enum_end_event);
            BinLogEventsContext context = new BinLogEventsContext();
            AtomicLong sum = new AtomicLong(0);
            long start = System.currentTimeMillis();
            long last = 0;
            long end = 0;
            while (packetReader.fetchNextBinlogEvent()) {
                binLogEventDecoder.decode(packetReader, context);
                sum.incrementAndGet();
                long current = sum.get();
                if (current - last >= 100000) {
                    end = System.currentTimeMillis();
                    long tps = ((current - last) * 1000) / (end - start);
                    System.out.println(" total : " + sum + " , cost : " + (end - start) + " , tps : " + tps);
                    last = current;
                    start = end;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                packetReader.close();
            } catch (IOException e) {
            }
        }
    }

    private static void sendBinlogDump(Connector connector, String binlogfilename, Long binlogPosition, int slaveId)
            throws IOException {
        BinlogDumpPacket binlogDumpCmd = new BinlogDumpPacket();
        binlogDumpCmd.binlogFileName = binlogfilename;
        binlogDumpCmd.binlogPosition = binlogPosition;
        binlogDumpCmd.slaveServerId = slaveId;
        byte[] cmdBody = binlogDumpCmd.encode();

        HeaderPacket binlogDumpHeader = new HeaderPacket();
        binlogDumpHeader.setPacketBodyLength(cmdBody.length);
        binlogDumpHeader.setPacketSequenceNumber((byte) 0x00);
        PacketManager.writePackets(connector.getSocketChannel(), binlogDumpHeader.encode(), cmdBody);
    }

    private static void updateSettings(Connector connector) throws IOException {
        update("set @master_binlog_checksum= '@@global.binlog_checksum'", connector);
        update("SET @mariadb_slave_capability='" + BinLogEvent.maria_slave_capability_mine + "'", connector);
    }

    public static void update(String cmd, Connector connector) throws IOException {
        UpdateExecutor exector = new UpdateExecutor(connector);
        exector.update(cmd);
    }

}
