package drds.data_propagate.parse.k;

import drds.data_propagate.parse.table_meta_data.MemoryTableMetaDataStore;
import drds.data_propagate.parse.table_meta_data.TableMetaData;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;

/*import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;*/

/**
 * @author agapple 2017年8月1日 下午7:15:54
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"/tsdb/h2-tsdb.xml"})
@Ignore
public class MemoryTableMeta_DataStore_Data_DDL_Test {

    @Test
    public void test1() throws Throwable {
        MemoryTableMetaDataStore memoryTableMetaDataStore = new MemoryTableMetaDataStore();
        URL url = Thread.currentThread().getContextClassLoader().getResource("dummy.txt");
        File dummyFile = new File(url.getFile());
        File create = new File(dummyFile.getParent() + "/ddl", "ddl_test1.sql");
        String sql = StringUtils.join(IOUtils.readLines(new FileInputStream(create)), "\n");
        memoryTableMetaDataStore.apply(null, "test", sql, null);

        TableMetaData meta = memoryTableMetaDataStore.find("yushitai_test", "card_record");
        System.out.println(meta);
        Assert.assertNotNull(meta.getColumnMetaData("customization_id"));

        meta = memoryTableMetaDataStore.find("yushitai_test", "_card_record_gho");
        Assert.assertNull(meta);
    }

    @Test
    public void test2() throws Throwable {
        MemoryTableMetaDataStore memoryTableMetaDataStore = new MemoryTableMetaDataStore();
        URL url = Thread.currentThread().getContextClassLoader().getResource("dummy.txt");
        File dummyFile = new File(url.getFile());
        File create = new File(dummyFile.getParent() + "/ddl", "ddl_test2.sql");
        String sql = StringUtils.join(IOUtils.readLines(new FileInputStream(create)), "\n");
        memoryTableMetaDataStore.apply(null, "test", sql, null);

        TableMetaData meta = memoryTableMetaDataStore.find("yushitai_test", "card_record");
        System.out.println(meta);
        Assert.assertEquals(meta.getColumnMetaData("id").isPrimaryKey(), true);
        Assert.assertEquals(meta.getColumnMetaData("name").isUniqueIndex(), true);
    }

    @Test
    public void test3() throws Throwable {
        MemoryTableMetaDataStore memoryTableMetaDataStore = new MemoryTableMetaDataStore();
        URL url = Thread.currentThread().getContextClassLoader().getResource("dummy.txt");
        File dummyFile = new File(url.getFile());
        File create = new File(dummyFile.getParent() + "/ddl", "ddl_test3.sql");
        String sql = StringUtils.join(IOUtils.readLines(new FileInputStream(create)), "\n");
        memoryTableMetaDataStore.apply(null, "test", sql, null);

        TableMetaData meta = memoryTableMetaDataStore.find("test", "quniya4");
        System.out.println(meta);
        Assert.assertTrue(meta.getColumnMetaDataList().get(0).getColumnName().equalsIgnoreCase("id"));
    }
}
