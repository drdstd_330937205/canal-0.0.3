package drds.data_propagate.parse.k;

import drds.data_propagate.parse.table_meta_data._do_.MetaDataSnapshotDo;
import drds.data_propagate.parse.table_meta_data.dao.MetaDataDataSnapshotDao;
import org.junit.Ignore;
import org.junit.Test;

import javax.annotation.Resource;

/*
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
*/

/**
 * Created by wanshao Date: 2017/9/20 Time: 下午5:00
 **/
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"/tsdb/h2-tsdb.xml"})
public class MetaDataSnapshotDaoTest {

    @Resource
    MetaDataDataSnapshotDao metaDataSnapshotDao;

    @Ignore
    @Test
    public void testSimple() {
        MetaDataSnapshotDo metaDataSnapshotDo = new MetaDataSnapshotDo();
        metaDataSnapshotDo.setDestination("test");
        metaDataSnapshotDo.setBinlogFileName("000001");
        metaDataSnapshotDo.setBinlogEventOffest(4L);
        metaDataSnapshotDo.setMasterId("1");
        metaDataSnapshotDo.setBinlogTimestamp(System.currentTimeMillis() - 7300 * 1000);
        metaDataSnapshotDo.setData("test");
        metaDataSnapshotDao.insert(metaDataSnapshotDo);

        MetaDataSnapshotDo snapshotDO = metaDataSnapshotDao.findByTimestamp("test", System.currentTimeMillis());
        System.out.println(snapshotDO);

        int count = metaDataSnapshotDao.deleteByTimestamp("test", 7200);
        System.out.println(count);
    }

}
