package drds.data_propagate.parse.k;

import drds.data_propagate.parse.table_meta_data._do_.MetaDataHistoryDo;
import drds.data_propagate.parse.table_meta_data.dao.MetaDataDataHistoryDao;
import org.junit.Ignore;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

/*import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;*/

/**
 * Created by wanshao Date: 2017/9/20 Time: 下午5:00
 **/
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"/tsdb/h2-tsdb.xml"})
@Ignore
public class MetaDataHistoryDaoTest {

    @Resource
    MetaDataDataHistoryDao metaDataHistoryDao;

    @Test
    public void testSimple() {
        MetaDataHistoryDo historyDO = new MetaDataHistoryDo();
        historyDO.setDestination("test");
        historyDO.setBinlogFileName("000001");
        historyDO.setBinlogEventOffest(4L);
        historyDO.setMasterId("1");
        historyDO.setBinlogEventExecuteTimestamp(System.currentTimeMillis() - 7300 * 1000);
        historyDO.setSqlSchema("test");
        historyDO.setUseSchema("test");
        historyDO.setSqlTable("testTable");
        historyDO.setSqlTable("drop tableName testTable");
        metaDataHistoryDao.insert(historyDO);

        int count = metaDataHistoryDao.deleteByTimestamp("test", 7200);
        System.out.println(count);

        List<MetaDataHistoryDo> metaDataHistoryDoList = metaDataHistoryDao.findByTimestamp("test", 0L, System.currentTimeMillis());
        for (MetaDataHistoryDo metaDataHistoryDo : metaDataHistoryDoList) {
            System.out.println(metaDataHistoryDo.getId());
        }
    }

}
