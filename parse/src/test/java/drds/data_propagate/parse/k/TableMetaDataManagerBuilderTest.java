package drds.data_propagate.parse.k;

import drds.data_propagate.parse.table_meta_data.TableMetaDataClassPathXmlApplicationContextManager;
import drds.data_propagate.parse.table_meta_data.TableMetaDataStore;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.util.Assert;

/**
 * @author agapple 2017年10月12日 上午10:50:00
 * @since 1.0.25
 */
public class TableMetaDataManagerBuilderTest {
    @Ignore
    @Test
    public void testSimple() {
        TableMetaDataStore tableMetaDataStore = TableMetaDataClassPathXmlApplicationContextManager.build("test",
                "classpath:table_meta_data/mysql-table_meta_data.xml");
        Assert.notNull(tableMetaDataStore);
        TableMetaDataClassPathXmlApplicationContextManager.destory("test");
    }
}
