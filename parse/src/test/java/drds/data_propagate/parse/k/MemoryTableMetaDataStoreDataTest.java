package drds.data_propagate.parse.k;

import drds.data_propagate.parse.table_meta_data.MemoryTableMetaDataStore;
import drds.data_propagate.parse.table_meta_data.TableMetaData;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;

//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author agapple 2017年8月1日 下午7:15:54
 */
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"/tsdb/mysql-tsdb.xml"})
@Ignore
public class MemoryTableMetaDataStoreDataTest {

    @Test
    public void testSimple() throws Throwable {
        MemoryTableMetaDataStore memoryTableMetaDataStore = new MemoryTableMetaDataStore();
        URL url = Thread.currentThread().getContextClassLoader().getResource("dummy.txt");
        File dummyFile = new File(url.getFile());
        File create = new File(dummyFile.getParent() + "/ddl", "create.sql");
        String sql = StringUtils.join(IOUtils.readLines(new FileInputStream(create)), "\n");
        memoryTableMetaDataStore.apply(null, "test", sql, null);

        TableMetaData meta = memoryTableMetaDataStore.find("test", "test");
        System.out.println(meta);
        Assert.assertTrue(meta.getColumnMetaData("ID").isPrimaryKey());
    }
}
