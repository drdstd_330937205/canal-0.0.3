package drds.data_propagate.parse;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.BinLogEventDecoder;
import drds.data_propagate.binlog_event.BinLogEventsContext;
import drds.data_propagate.binlog_event.Buffer;
import drds.data_propagate.binlog_event.event.RowsEventBuffer;
import drds.data_propagate.binlog_event.event.TableMapEvent;
import drds.data_propagate.binlog_event.event.TableMapEvent.ColumnInfo;
import drds.data_propagate.binlog_event.event.rows_event.DeleteRowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.RowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.UpdateRowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.WriteRowsEvent;
import drds.data_propagate.driver.Connector;
import drds.data_propagate.driver.UpdateExecutor;
import drds.data_propagate.driver.packets.HeaderPacket;
import drds.data_propagate.driver.packets.client.command_packet.BinlogDumpPacket;
import drds.data_propagate.driver.utils.PacketManager;
import org.junit.Ignore;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.BitSet;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;

@Ignore
public class MysqlBinlogParsePerformanceTest {

    protected static Charset charset = Charset.forName("utf-8");

    public static void main(String args[]) {
        PacketReader packetReader = new PacketReader();
        try {
            Connector connector = new Connector(new InetSocketAddress("127.0.0.1", 3306), "root", "hello");
            connector.connect();
            updateSettings(connector);
            sendBinlogDump(connector, "mysql-bin.000006", 120L, 3);
            packetReader.start(connector.getSocketChannel());
            final BlockingQueue<Buffer> buffer = new ArrayBlockingQueue<Buffer>(1024);
            Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {
                        consumer(buffer);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();

            while (packetReader.fetchNextBinlogEvent()) {
                buffer.put(packetReader.duplicateFromEffectiveInitialIndexOffset());
                packetReader.moveEffectiveInitialIndexAndLimit(packetReader.limit());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                packetReader.close();
            } catch (IOException e) {
            }
        }
    }

    public static void consumer(BlockingQueue<Buffer> buffer) throws IOException, InterruptedException {
        BinLogEventDecoder binLogEventDecoder = new BinLogEventDecoder(BinLogEvent.unknown_event, BinLogEvent.enum_end_event);
        BinLogEventsContext context = new BinLogEventsContext();

        AtomicLong sum = new AtomicLong(0);
        long start = System.currentTimeMillis();
        long last = 0;
        long end = 0;
        while (true) {
            BinLogEvent event = null;
            event = binLogEventDecoder.decode(buffer.take(), context);
            int eventType = event.getHeader().getEventType();
            switch (eventType) {
                case BinLogEvent.rotate_event:
                    break;
                case BinLogEvent.write_rows_event_v1:
                case BinLogEvent.write_rows_event:
                    parseRowsEvent((WriteRowsEvent) event, sum);
                    break;
                case BinLogEvent.update_rows_event_v1:
                case BinLogEvent.partial_update_rows_event:
                case BinLogEvent.update_rows_event:
                    parseRowsEvent((UpdateRowsEvent) event, sum);
                    break;
                case BinLogEvent.delete_rows_event_v1:
                case BinLogEvent.delete_rows_event:
                    parseRowsEvent((DeleteRowsEvent) event, sum);
                    break;
                case BinLogEvent.xid_event:
                    sum.incrementAndGet();
                    break;
                case BinLogEvent.query_event:
                    sum.incrementAndGet();
                    break;
                default:
                    break;
            }

            long current = sum.get();
            if (current - last >= 100000) {
                end = System.currentTimeMillis();
                long tps = ((current - last) * 1000) / (end - start);
                System.out.println(" total : " + sum + " , cost : " + (end - start) + " , tps : " + tps);
                last = current;
                start = end;
            }
        }
    }

    private static void sendBinlogDump(Connector connector, String binlogfilename, Long binlogPosition, int slaveId)
            throws IOException {
        BinlogDumpPacket binlogDumpCmd = new BinlogDumpPacket();
        binlogDumpCmd.binlogFileName = binlogfilename;
        binlogDumpCmd.binlogPosition = binlogPosition;
        binlogDumpCmd.slaveServerId = slaveId;
        byte[] cmdBody = binlogDumpCmd.encode();

        HeaderPacket binlogDumpHeader = new HeaderPacket();
        binlogDumpHeader.setPacketBodyLength(cmdBody.length);
        binlogDumpHeader.setPacketSequenceNumber((byte) 0x00);
        PacketManager.writePackets(connector.getSocketChannel(), binlogDumpHeader.encode(), cmdBody);
    }

    private static void updateSettings(Connector connector) throws IOException {
        update("set @master_binlog_checksum= '@@global.binlog_checksum'", connector);
        update("SET @mariadb_slave_capability='" + BinLogEvent.maria_slave_capability_mine + "'", connector);
    }

    public static void update(String cmd, Connector connector) throws IOException {
        UpdateExecutor exector = new UpdateExecutor(connector);
        exector.update(cmd);
    }

    public static void parseRowsEvent(RowsEvent event, AtomicLong sum) {
        try {
            RowsEventBuffer buffer = event.getRowsEventBuffer(charset.name());
            BitSet columns = event.getColumnBitSet();
            BitSet changeColumns = event.getChangeColumnBitSet();
            while (buffer.nextOneRow(columns)) {
                int type = event.getHeader().getEventType();
                if (BinLogEvent.write_rows_event_v1 == type || BinLogEvent.write_rows_event == type) {
                    parseOneRow(event, buffer, columns, true);
                } else if (BinLogEvent.delete_rows_event_v1 == type || BinLogEvent.delete_rows_event == type) {
                    parseOneRow(event, buffer, columns, false);
                } else {
                    parseOneRow(event, buffer, columns, false);
                    if (!buffer.nextOneRow(changeColumns, true)) {
                        break;
                    }
                    parseOneRow(event, buffer, changeColumns, true);
                }

                sum.incrementAndGet();
            }
        } catch (Exception e) {
            throw new RuntimeException("parse row data failed.", e);
        }
    }

    public static void parseOneRow(RowsEvent event, RowsEventBuffer buffer, BitSet cols, boolean isAfter)
            throws UnsupportedEncodingException {
        TableMapEvent map = event.getTableMapEvent();
        if (map == null) {
            throw new RuntimeException("not found TableMap with tid=" + event.getTableId());
        }

        final int columnCnt = map.getColumnCount();
        final ColumnInfo[] columnInfo = map.getColumnInfos();
        for (int i = 0; i < columnCnt; i++) {
            if (!cols.get(i)) {
                continue;
            }

            ColumnInfo info = columnInfo[i];
            buffer.nextValue(null, i, info.type, info.meta);
            if (buffer.isFNull()) {
            } else {
                buffer.getValue();
            }
        }
    }
}
