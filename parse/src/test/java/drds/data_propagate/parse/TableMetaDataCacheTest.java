package drds.data_propagate.parse;

import drds.data_propagate.driver.packets.server.ResultSetPacket;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;

@Ignore
public class TableMetaDataCacheTest {

    @Test
    public void testSimple() throws IOException {
        DumperImpl dumper = new DumperImpl(new InetSocketAddress("127.0.0.1", 3306), "root", "hello");
        try {
            dumper.connect();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }

        List<ResultSetPacket> packets = dumper.querys("show create tableName test.ljh_test");
        String createDDL = null;
        if (packets.get(0).getValueList().size() > 0) {
            createDDL = packets.get(0).getValueList().get(1);
        }

        System.out.println(createDDL);

        // TableMetaDataCache cache = new TableMetaDataCache(dumper);
        // TableMetaData meta = cache.getTableMetaData("otter1", "otter_stability1");
        // Assert.assertNotNull(meta);
        // for (ColumnMetaData field : meta.getColumnMetaDataList()) {
        // System.out.println("filed :" + field.getColumnName() + " , isPrimaryKey : "
        // + field.isPrimaryKey() + " , isNull : "
        // + field.isNullable());
        // }
    }
}
