package drds.data_propagate.parse;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.BinLogEventDecoder;
import drds.data_propagate.binlog_event.BinLogEventsContext;
import drds.data_propagate.binlog_event.event.*;
import drds.data_propagate.binlog_event.event.TableMapEvent.ColumnInfo;
import drds.data_propagate.binlog_event.event.mariadb.AnnotateRowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.DeleteRowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.RowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.UpdateRowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.WriteRowsEvent;
import drds.data_propagate.driver.Connector;
import drds.data_propagate.driver.QueryExecutor;
import drds.data_propagate.driver.UpdateExecutor;
import drds.data_propagate.driver.packets.HeaderPacket;
import drds.data_propagate.driver.packets.client.command_packet.BinlogDumpPacket;
import drds.data_propagate.driver.packets.client.command_packet.RegisterSlavePacket;
import drds.data_propagate.driver.packets.server.ErrorPacket;
import drds.data_propagate.driver.packets.server.ResultSetPacket;
import drds.data_propagate.driver.utils.PacketManager;
import drds.data_propagate.parse.exception.ParseException;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.util.BitSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static drds.data_propagate.parse.PacketReader.master_heartbeat_period_seconds;

@Ignore
public class DirectAbstractPacketReaderTest {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected String binlogFileName = "mysql-bin.000001";
    protected Charset charset = Charset.forName("utf-8");
    private int binlogChecksum;

    @Test
    public void testSimple() {
        PacketReader packetReader = new PacketReader();
        try {
            Connector connector = new Connector(new InetSocketAddress("127.0.0.1", 3306), "root", "hello");
            connector.connect();
            updateSettings(connector);
            loadBinlogChecksum(connector);
            sendRegisterSlave(connector, 3);
            sendBinlogDump(connector, "mysql-bin.000001", 4L, 3);

            packetReader.start(connector.getSocketChannel());

            BinLogEventDecoder binLogEventDecoder = new BinLogEventDecoder(BinLogEvent.unknown_event, BinLogEvent.enum_end_event);
            BinLogEventsContext context = new BinLogEventsContext();
            context.setFormatDescriptionEvent(new FormatDescriptionEvent(4, binlogChecksum));

            while (packetReader.fetchNextBinlogEvent()) {
                BinLogEvent event = null;
                event = binLogEventDecoder.decode(packetReader, context);

                if (event == null) {
                    throw new RuntimeException("parse failed");
                }

                int eventType = event.getHeader().getEventType();
                switch (eventType) {
                    case BinLogEvent.rotate_event:
                        // binlogFileName = ((RotateEvent)
                        // event).getFilename();
                        System.out.println(((RotateEvent) event).getFilename());
                        break;
                    case BinLogEvent.write_rows_event_v1:
                    case BinLogEvent.write_rows_event:
                        parseRowsEvent((WriteRowsEvent) event);
                        break;
                    case BinLogEvent.update_rows_event_v1:
                    case BinLogEvent.partial_update_rows_event:
                    case BinLogEvent.update_rows_event:
                        parseRowsEvent((UpdateRowsEvent) event);
                        break;
                    case BinLogEvent.delete_rows_event_v1:
                    case BinLogEvent.delete_rows_event:
                        parseRowsEvent((DeleteRowsEvent) event);
                        break;
                    case BinLogEvent.query_event:
                        parseQueryEvent((QueryEvent) event);
                        break;
                    case BinLogEvent.rows_query_log_event:
                        parseRowsQueryEvent((RowsQueryEvent) event);
                        break;
                    case BinLogEvent.annotate_rows_event:
                        break;
                    case BinLogEvent.xid_event:
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        } finally {
            try {
                packetReader.close();
            } catch (IOException e) {
                Assert.fail(e.getMessage());
            }
        }

    }

    private void sendRegisterSlave(Connector connector, int slaveId) throws IOException {
        RegisterSlavePacket cmd = new RegisterSlavePacket();
        cmd.host = connector.getInetSocketAddress().getAddress().getHostAddress();
        cmd.password = connector.getPassword();
        cmd.username = connector.getUsername();
        cmd.serverId = slaveId;
        byte[] cmdBody = cmd.encode();

        HeaderPacket header = new HeaderPacket();
        header.setPacketBodyLength(cmdBody.length);
        header.setPacketSequenceNumber((byte) 0x00);
        PacketManager.writePackets(connector.getSocketChannel(), header.encode(), cmdBody);

        header = PacketManager.readHeader(connector.getSocketChannel(), 4);
        byte[] body = PacketManager.readBytes(connector.getSocketChannel(), header.getPacketBodyLength());
        assert body != null;
        if (body[0] < 0) {
            if (body[0] == -1) {
                ErrorPacket err = new ErrorPacket();
                err.decode(body);
                throw new IOException("Error When doing Register slave:" + err.toString());
            } else {
                throw new IOException("unpexpected packet with field_count=" + body[0]);
            }
        }
    }

    private void sendBinlogDump(Connector connector, String binlogfilename, Long binlogPosition, int slaveId)
            throws IOException {
        BinlogDumpPacket binlogDumpCmd = new BinlogDumpPacket();
        binlogDumpCmd.binlogFileName = binlogfilename;
        binlogDumpCmd.binlogPosition = binlogPosition;
        binlogDumpCmd.slaveServerId = slaveId;
        byte[] cmdBody = binlogDumpCmd.encode();

        HeaderPacket binlogDumpHeader = new HeaderPacket();
        binlogDumpHeader.setPacketBodyLength(cmdBody.length);
        binlogDumpHeader.setPacketSequenceNumber((byte) 0x00);
        PacketManager.writePackets(connector.getSocketChannel(), binlogDumpHeader.encode(), cmdBody);
    }

    private void updateSettings(Connector connector) throws IOException {
        try {
            update("set wait_timeout=9999999", connector);
        } catch (Exception e) {
            logger.warn("update wait_timeout failed", e);
        }
        try {
            update("set net_write_timeout=1800", connector);
        } catch (Exception e) {
            logger.warn("update net_write_timeout failed", e);
        }

        try {
            update("set net_read_timeout=1800", connector);
        } catch (Exception e) {
            logger.warn("update net_read_timeout failed", e);
        }

        try {
            // 设置服务端返回结果时不做编码转化，直接按照数据库的二进制编码进行发送，由客户端自己根据需求进行编码转化
            update("set names 'binary'", connector);
        } catch (Exception e) {
            logger.warn("update names failed", e);
        }

        try {
            // mysql5.6针对checksum支持需要设置session变量
            // 如果不设置会出现错误： Slave can not handle replication events with the
            // checksum that master is configured decode log
            // 但也不能乱设置，需要和mysql server的checksum配置一致，不然RotateLogEvent会出现乱码
            update("set @master_binlog_checksum= @@global.binlog_checksum", connector);
        } catch (Exception e) {
            logger.warn("update master_binlog_checksum failed", e);
        }

        try {
            // 参考:https://github.com/alibaba/canal/issues/284
            // mysql5.6需要设置slave_uuid避免被server kill链接
            update("set @slave_uuid=uuid()", connector);
        } catch (Exception e) {
            if (!StringUtils.contains(e.getMessage(), "Unknown system variable")) {
                logger.warn("update slave_uuid failed", e);
            }
        }

        try {
            // mariadb针对特殊的类型，需要设置session变量
            update("SET @mariadb_slave_capability='" + BinLogEvent.maria_slave_capability_mine + "'", connector);
        } catch (Exception e) {
            logger.warn("update mariadb_slave_capability failed", e);
        }

        try {
            long period = TimeUnit.SECONDS.toNanos(master_heartbeat_period_seconds);
            update("SET @master_heartbeat_period=" + period, connector);
        } catch (Exception e) {
            logger.warn("update master_heartbeat_period failed", e);
        }
    }

    private void loadBinlogChecksum(Connector connector) {
        ResultSetPacket resultSetPacket = null;
        try {
            resultSetPacket = query("select @@global.binlog_checksum", connector);
        } catch (IOException e) {
            throw new ParseException(e);
        }

        List<String> columnValues = resultSetPacket.getValueList();
        if (columnValues != null && columnValues.size() >= 1 && columnValues.get(0).toUpperCase().equals("CRC32")) {
            binlogChecksum = BinLogEvent.binlog_checksum_alg_crc32;
        } else {
            binlogChecksum = BinLogEvent.binlog_checksum_alg_off;
        }
    }

    public ResultSetPacket query(String cmd, Connector connector) throws IOException {
        QueryExecutor exector = new QueryExecutor(connector);
        return exector.query(cmd);
    }

    public void update(String cmd, Connector connector) throws IOException {
        UpdateExecutor exector = new UpdateExecutor(connector);
        exector.update(cmd);
    }

    protected void parseQueryEvent(QueryEvent event) {
        System.out.println(String.format("================> binlog_event[%s:%s] , name[%s]", binlogFileName,
                event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength(),
                event.getCatalog()));
        System.out.println("sql : " + event.getQueryString());
    }

    protected void parseRowsQueryEvent(RowsQueryEvent event) throws Exception {
        System.out.println(String.format("================> binlog_event[%s:%s]", binlogFileName,
                event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength()));
        System.out.println("sql : " + new String(event.getRowsQuery().getBytes("ISO-8859-1"), charset.name()));
    }

    protected void parseAnnotateRowsEvent(AnnotateRowsEvent event) throws Exception {
        System.out.println(String.format("================> binlog_event[%s:%s]", binlogFileName,
                event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength()));
        System.out.println("sql : " + new String(event.getRowsQuery().getBytes("ISO-8859-1"), charset.name()));
    }

    protected void parseXidEvent(XidEvent event) throws Exception {
        System.out.println(String.format("================> binlog_event[%s:%s]", binlogFileName,
                event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength()));
        System.out.println("xid : " + event.getXid());
    }

    protected void parseRowsEvent(RowsEvent event) {
        try {
            System.out.println(String.format("================> binlog_event[%s:%s] , name[%s,%s]", binlogFileName,
                    event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength(),
                    event.getTableMapEvent().getSchemaName(), event.getTableMapEvent().getTableName()));
            RowsEventBuffer buffer = event.getRowsEventBuffer(charset.name());
            BitSet columns = event.getColumnBitSet();
            BitSet changeColumns = event.getChangeColumnBitSet();
            while (buffer.nextOneRow(columns)) {
                // 处理row记录
                int type = event.getHeader().getEventType();
                if (BinLogEvent.write_rows_event_v1 == type || BinLogEvent.write_rows_event == type) {
                    // insert的记录放在before字段中
                    parseOneRow(event, buffer, columns, true);
                } else if (BinLogEvent.delete_rows_event_v1 == type || BinLogEvent.delete_rows_event == type) {
                    // delete的记录放在before字段中
                    parseOneRow(event, buffer, columns, false);
                } else {
                    // update需要处理before/after
                    System.out.println("-------> before");
                    parseOneRow(event, buffer, columns, false);
                    if (!buffer.nextOneRow(changeColumns, true)) {
                        break;
                    }
                    System.out.println("-------> after");
                    parseOneRow(event, buffer, changeColumns, true);
                }

            }
        } catch (Exception e) {
            throw new RuntimeException("parse row data failed.", e);
        }
    }

    protected void parseOneRow(RowsEvent event, RowsEventBuffer buffer, BitSet cols, boolean isAfter)
            throws UnsupportedEncodingException {
        TableMapEvent map = event.getTableMapEvent();
        if (map == null) {
            throw new RuntimeException("not found TableMap with tid=" + event.getTableId());
        }

        final int columnCnt = map.getColumnCount();
        final ColumnInfo[] columnInfo = map.getColumnInfos();

        for (int i = 0; i < columnCnt; i++) {
            if (!cols.get(i)) {
                continue;
            }

            ColumnInfo info = columnInfo[i];
            buffer.nextValue(null, i, info.type, info.meta);

            if (buffer.isFNull()) {
                //
            } else {
                final Serializable value = buffer.getValue();
                if (value instanceof byte[]) {
                    System.out.println(new String((byte[]) value));
                } else {
                    System.out.println(value);
                }
            }
        }

    }

}
