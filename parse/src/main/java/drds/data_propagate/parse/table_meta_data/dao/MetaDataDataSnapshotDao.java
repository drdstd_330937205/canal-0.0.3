package drds.data_propagate.parse.table_meta_data.dao;

import com.google.common.collect.Maps;
import drds.data_propagate.parse.table_meta_data._do_.MetaDataSnapshotDo;

import java.util.HashMap;


public class MetaDataDataSnapshotDao extends MetaDataBaseDao {

    public Long insert(MetaDataSnapshotDo snapshotDO) {
        return (Long) getSqlMapClientTemplate().insert("meta_snapshot.insert", snapshotDO);
    }

    public Long update(MetaDataSnapshotDo snapshotDO) {
        return (Long) getSqlMapClientTemplate().insert("meta_snapshot.update", snapshotDO);
    }

    public MetaDataSnapshotDo findByTimestamp(String destination, Long timestamp) {
        HashMap params = Maps.newHashMapWithExpectedSize(2);
        params.put("timestamp", timestamp == null ? 0L : timestamp);
        params.put("destination", destination);

        return (MetaDataSnapshotDo) getSqlMapClientTemplate().queryForObject("meta_snapshot.findByTimestamp", params);
    }

    public Integer deleteByName(String destination) {
        HashMap params = Maps.newHashMapWithExpectedSize(2);
        params.put("destination", destination);
        return getSqlMapClientTemplate().delete("meta_snapshot.deleteByName", params);
    }

    /**
     * 删除interval秒之前的数据
     */
    public Integer deleteByTimestamp(String destination, int interval) {
        HashMap params = Maps.newHashMapWithExpectedSize(2);
        long timestamp = System.currentTimeMillis() - interval * 1000;
        params.put("timestamp", timestamp);
        params.put("destination", destination);
        return getSqlMapClientTemplate().delete("meta_snapshot.deleteByTimestamp", params);
    }

    protected void initDao() throws Exception {
        initTable("meta_snapshot");
    }

}
