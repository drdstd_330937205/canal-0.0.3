package drds.data_propagate.parse.table_meta_data;


public interface TableMetaDataFactory {

    /**
     * 代理一下tableMetaTSDB的获取,使用隔离的spring定义
     */
    public TableMetaDataStore build(String destination, String springXml);

    public void destory(String destination);
}
