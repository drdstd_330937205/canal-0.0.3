package drds.data_propagate.parse.table_meta_data;

public class TableMetaDataFactoryImpl implements TableMetaDataFactory {

    /**
     * 代理一下tableMetaTSDB的获取,使用隔离的spring定义
     */
    public TableMetaDataStore build(String destination, String springXml) {
        return TableMetaDataClassPathXmlApplicationContextManager.build(destination, springXml);
    }

    public void destory(String destination) {
        TableMetaDataClassPathXmlApplicationContextManager.destory(destination);
    }
}
