package drds.data_propagate.parse.table_meta_data;

import drds.data_propagate.entry.position.EntryPosition;

import java.util.Map;

/**
 * 表结构的时间序列存储
 */
public interface TableMetaDataStore {

    /**
     * 初始化
     */
    public boolean init(String destination);

    /**
     * 销毁资源
     */
    public void destory();

    /**
     * 获取当前的表结构
     */
    public TableMetaData find(String schemaName, String tableName);

    /**
     * 添加ddl到时间序列库中
     */
    public boolean apply(EntryPosition entryPosition, String schemaName, String ddl, String extra);

    /**
     * 回滚到指定位点的表结构
     */
    public boolean rollback(EntryPosition entryPosition);

    /**
     * 生成快照内容
     */
    public Map<String/* schema */, String> snapshot();

}
