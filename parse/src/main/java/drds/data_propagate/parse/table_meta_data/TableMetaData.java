package drds.data_propagate.parse.table_meta_data;

import drds.data_propagate.binlog_event.event.TableMapEvent;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述数据meta对象,mysql binlog中对应的{@linkplain TableMapEvent}包含的信息不全
 *
 * <pre>
 * 1. 主键信息
 * 2. column name
 * 3. unsigned字段
 * </pre>
 */
public class TableMetaData {
    @Setter
    @Getter
    private String schemaName;
    @Setter
    @Getter
    private String tableName;
    @Setter
    @Getter
    private List<ColumnMetaData> columnMetaDataList = new ArrayList<ColumnMetaData>();
    @Setter
    @Getter
    private String ddl; // 表结构的DDL语句

    public TableMetaData() {

    }

    public TableMetaData(String schemaName, String tableName, List<ColumnMetaData> columnMetaDataList) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.columnMetaDataList = columnMetaDataList;
    }

    public String getFullName() {
        return schemaName + "." + tableName;
    }


    public ColumnMetaData getColumnMetaData(String columnName) {
        for (ColumnMetaData columnMetaData : columnMetaDataList) {
            if (columnMetaData.getColumnName().equalsIgnoreCase(columnName)) {
                return columnMetaData;
            }
        }

        throw new RuntimeException("unknow column : " + columnName);
    }

    public List<ColumnMetaData> getPrimaryKeyColumnMetaDataList() {
        List<ColumnMetaData> columnMetaDataList = new ArrayList<ColumnMetaData>();
        for (ColumnMetaData columnMetaData : this.columnMetaDataList) {
            if (columnMetaData.isPrimaryKey()) {
                columnMetaDataList.add(columnMetaData);
            }
        }

        return columnMetaDataList;
    }


    public void addColumnMetaData(ColumnMetaData columnMetaData) {
        this.columnMetaDataList.add(columnMetaData);
    }


}
