package drds.data_propagate.parse.table_meta_data.dao;

import com.google.common.collect.Maps;
import drds.data_propagate.parse.table_meta_data._do_.MetaDataHistoryDo;

import java.util.HashMap;
import java.util.List;


public class MetaDataDataHistoryDao extends MetaDataBaseDao {

    public Long insert(MetaDataHistoryDo metaDataHistoryDo) {
        return (Long) getSqlMapClientTemplate().insert("meta_history.insert", metaDataHistoryDo);
    }

    public List<MetaDataHistoryDo> findByTimestamp(String destination,
                                                   Long snapshotTimestamp, Long timestamp) {
        HashMap params = Maps.newHashMapWithExpectedSize(2);
        params.put("destination", destination);
        params.put("snapshotTimestamp", snapshotTimestamp == null ? 0L : snapshotTimestamp);
        params.put("timestamp", timestamp == null ? 0L : timestamp);
        return (List<MetaDataHistoryDo>) getSqlMapClientTemplate().queryForList("meta_history.findByTimestamp", params);
    }

    public Integer deleteByName(String destination) {
        HashMap params = Maps.newHashMapWithExpectedSize(2);
        params.put("destination", destination);
        return getSqlMapClientTemplate().delete("meta_history.deleteByName", params);
    }

    /**
     * 删除interval秒之前的数据
     */
    public Integer deleteByTimestamp(String destination, int interval) {
        HashMap params = Maps.newHashMapWithExpectedSize(2);
        long timestamp = System.currentTimeMillis() - interval * 1000;
        params.put("timestamp", timestamp);
        params.put("destination", destination);
        return getSqlMapClientTemplate().delete("meta_history.deleteByTimestamp", params);
    }

    protected void initDao() throws Exception {
        initTable("meta_history");
    }
}
