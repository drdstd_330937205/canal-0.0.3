package drds.data_propagate.parse.table_meta_data;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

public class ColumnMetaData {
    @Setter
    @Getter
    private String columnName;
    @Setter
    @Getter
    private String columnType;
    @Setter
    @Getter
    private boolean nullable;
    @Setter
    @Getter
    private boolean primaryKey;
    @Setter
    @Getter
    private String defaultValue;
    @Setter
    @Getter
    private String extra;
    @Setter
    @Getter
    private boolean uniqueIndex;

    public ColumnMetaData() {

    }

    public ColumnMetaData(String columnName, String columnType, boolean nullable, boolean primaryKey, String defaultValue) {
        this.columnName = columnName;
        this.columnType = columnType;
        this.nullable = nullable;
        this.primaryKey = primaryKey;
        this.defaultValue = defaultValue;
    }


    public boolean isUnsigned() {
        return StringUtils.containsIgnoreCase(columnType, "unsigned");
    }


}
