package drds.data_propagate.parse.table_meta_data._do_;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

public class MetaDataHistoryDo {
    @Setter
    @Getter
    private Long id;
    @Setter
    @Getter
    private Date createDate;
    @Setter
    @Getter
    private Date updateDate;
    @Setter
    @Getter
    private String destination;
    @Setter
    @Getter
    private String binlogFileName;
    @Setter
    @Getter
    private Long binlogEventOffest;
    @Setter
    @Getter
    private String masterId;
    @Setter
    @Getter
    private Long binlogEventExecuteTimestamp;
    @Setter
    @Getter
    private String useSchema;
    @Setter
    @Getter
    private String sqlSchema;
    @Setter
    @Getter
    private String sqlTable;
    @Setter
    @Getter
    private String sql;
    @Setter
    @Getter
    private String sqlType;
    @Setter
    @Getter
    private String extra;


}
