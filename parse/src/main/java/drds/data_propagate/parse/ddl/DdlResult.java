package drds.data_propagate.parse.ddl;

import drds.data_propagate.entry.EventType;
import lombok.Getter;
import lombok.Setter;

public class DdlResult {
    @Setter
    @Getter
    private String schemaName;
    @Setter
    @Getter
    private String tableName;
    @Setter
    @Getter
    private String oriSchemaName; // rename ddl中的源表
    @Setter
    @Getter
    private String oriTableName; // rename ddl中的目标表
    @Setter
    @Getter
    private EventType eventType;
    @Setter
    @Getter
    private DdlResult renameTableResult; // 多个rename table的存储

    /*
     * RENAME TABLE tbl_name TO new_tbl_name [, tbl_name2 TO new_tbl_name2] ...
     */

    public DdlResult() {
    }

    public DdlResult(String schemaName) {
        this.schemaName = schemaName;
    }

    public DdlResult(String schemaName, String tableName) {
        this.schemaName = schemaName;
        this.tableName = tableName;
    }

    public DdlResult(String schemaName, String tableName, String oriSchemaName, String oriTableName) {
        this.schemaName = schemaName;
        this.tableName = tableName;
        this.oriSchemaName = oriSchemaName;
        this.oriTableName = oriTableName;
    }


    @Override
    public DdlResult clone() {
        DdlResult ddlResult = new DdlResult();
        ddlResult.setOriSchemaName(oriSchemaName);
        ddlResult.setOriTableName(oriTableName);
        ddlResult.setSchemaName(schemaName);
        ddlResult.setTableName(tableName);
        // result.setEventType(eventType);
        return ddlResult;
    }

}
