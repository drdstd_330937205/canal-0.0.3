package drds.data_propagate.parse;

import drds.data_propagate.common.LifeCycle;
import drds.data_propagate.entry.Entry;
import drds.data_propagate.parse.exception.ParseException;


public interface BinlogParser<BinLogEvent> extends LifeCycle {

    Entry parse(BinLogEvent binLogEvent, boolean isSeek) throws ParseException;

    void reset();
}
