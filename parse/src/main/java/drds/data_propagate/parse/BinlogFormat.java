package drds.data_propagate.parse;

public enum BinlogFormat {

    STATEMENT("STATEMENT"), ROW("ROW"), MIXED("MIXED");

    private String value;

    private BinlogFormat(String value) {
        this.value = value;
    }

    public static BinlogFormat valuesOf(String value) {
        BinlogFormat[] binlogFormats = values();
        for (BinlogFormat binlogFormat : binlogFormats) {
            if (binlogFormat.value.equalsIgnoreCase(value)) {
                return binlogFormat;
            }
        }
        return null;
    }

}
