package drds.data_propagate.parse;


public enum BinlogImage {

    FULL("FULL"), MINIMAL("MINIMAL"), NOBLOB("NOBLOB");

    private String value;

    private BinlogImage(String value) {
        this.value = value;
    }

    public static BinlogImage valuesOf(String value) {
        BinlogImage[] binlogImages = values();
        for (BinlogImage binlogImage : binlogImages) {
            if (binlogImage.value.equalsIgnoreCase(value)) {
                return binlogImage;
            }
        }
        return null;
    }


}
