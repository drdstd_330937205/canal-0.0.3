package drds.data_propagate.parse;

import com.lmax.disruptor.EventFactory;

public class MessageEventFactory implements EventFactory<MessageEvent> {

    public MessageEvent newInstance() {
        return new MessageEvent();
    }
}
