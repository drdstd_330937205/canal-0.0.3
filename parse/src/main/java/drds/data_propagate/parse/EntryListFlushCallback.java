package drds.data_propagate.parse;

import drds.data_propagate.entry.Entry;

import java.util.List;

/**
 * 事务刷新机制
 */
public interface EntryListFlushCallback {

    public void flush(List<Entry> entryList) throws InterruptedException;
}
