package drds.data_propagate.parse;

import drds.data_propagate.common.AbstractLifeCycle;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * 组合多个EventParser进行合并处理，group只是做为一个delegate处理
 */
public class GroupEventParser extends AbstractLifeCycle implements EventParser {
    @Setter
    @Getter
    private List<EventParser> eventParserList = new ArrayList<EventParser>();

    public void start() {
        super.start();
        // 统一启动
        for (EventParser eventParser : eventParserList) {
            if (!eventParser.isStart()) {
                eventParser.start();
            }
        }
    }

    public void stop() {
        super.stop();
        // 统一关闭
        for (EventParser eventParser : eventParserList) {
            if (eventParser.isStart()) {
                eventParser.stop();
            }
        }
    }


}
