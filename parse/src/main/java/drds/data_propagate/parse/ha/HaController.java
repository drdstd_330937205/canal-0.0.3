package drds.data_propagate.parse.ha;

import drds.data_propagate.common.LifeCycle;
import drds.data_propagate.parse.exception.DataPropagateHAException;

/**
 * HA 控制器实现
 */
public interface HaController extends LifeCycle {

    public void start() throws DataPropagateHAException;

    public void stop() throws DataPropagateHAException;
}
