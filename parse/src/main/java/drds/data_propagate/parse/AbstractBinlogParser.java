package drds.data_propagate.parse;

import drds.data_propagate.common.AbstractLifeCycle;
import drds.data_propagate.entry.Entry;
import drds.data_propagate.parse.exception.ParseException;
import drds.data_propagate.parse.table_meta_data.TableMetaData;

public abstract class AbstractBinlogParser<Event> extends AbstractLifeCycle implements BinlogParser<Event> {

    public void reset() {
    }

    public Entry parse(Event event, TableMetaData tableMetaData) throws ParseException {
        return null;
    }

    public Entry parse(Event event) throws ParseException {
        return null;
    }

    public void stop() {
        reset();
        super.stop();
    }

}
