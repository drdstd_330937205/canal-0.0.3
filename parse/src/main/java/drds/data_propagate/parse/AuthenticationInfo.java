package drds.data_propagate.parse;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.net.InetSocketAddress;

/**
 * 数据库认证信息
 */
public class AuthenticationInfo {
    @Setter
    @Getter
    private InetSocketAddress inetSocketAddress; // 主库信息
    @Setter
    @Getter
    private String username; // 帐号
    @Setter
    @Getter
    private String password; // 密码
    @Setter
    @Getter
    private String defaultDatabaseName;// 默认链接的数据库

    public AuthenticationInfo() {
        super();
    }

    public AuthenticationInfo(InetSocketAddress inetSocketAddress, String username, String password) {
        this(inetSocketAddress, username, password, "");
    }

    public AuthenticationInfo(InetSocketAddress inetSocketAddress, String username, String password,
                              String defaultDatabaseName) {
        this.inetSocketAddress = inetSocketAddress;
        this.username = username;
        this.password = password;
        this.defaultDatabaseName = defaultDatabaseName;
    }


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((inetSocketAddress == null) ? 0 : inetSocketAddress.hashCode());
        result = prime * result + ((defaultDatabaseName == null) ? 0 : defaultDatabaseName.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof AuthenticationInfo)) {
            return false;
        }
        AuthenticationInfo other = (AuthenticationInfo) obj;
        if (inetSocketAddress == null) {
            if (other.inetSocketAddress != null) {
                return false;
            }
        } else if (!inetSocketAddress.equals(other.inetSocketAddress)) {
            return false;
        }
        if (defaultDatabaseName == null) {
            if (other.defaultDatabaseName != null) {
                return false;
            }
        } else if (!defaultDatabaseName.equals(other.defaultDatabaseName)) {
            return false;
        }
        if (password == null) {
            if (other.password != null) {
                return false;
            }
        } else if (!password.equals(other.password)) {
            return false;
        }
        if (username == null) {
            if (other.username != null) {
                return false;
            }
        } else if (!username.equals(other.username)) {
            return false;
        }
        return true;
    }

}
