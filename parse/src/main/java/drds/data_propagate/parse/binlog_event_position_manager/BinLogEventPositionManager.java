package drds.data_propagate.parse.binlog_event_position_manager;

import drds.data_propagate.common.LifeCycle;
import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.parse.exception.ParseException;


public interface BinLogEventPositionManager extends LifeCycle {

    BinLogEventPosition getLatestBinLogEventPosition(String destination);

    void persistBinLogEventPosition(String destination, BinLogEventPosition binLogEventPosition) throws ParseException;

}
