package drds.data_propagate.parse.binlog_event_position_manager;

import com.google.common.collect.MapMaker;
import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.parse.exception.ParseException;

import java.util.Map;
import java.util.Set;


public class MemoryBinLogEventPositionManager extends AbstractBinLogEventPositionManager {

    private Map<String, BinLogEventPosition> destinationToBinLogEventPositionMap;

    @Override
    public void start() {
        super.start();
        destinationToBinLogEventPositionMap = new MapMaker().makeMap();
    }

    @Override
    public void stop() {
        super.stop();
        destinationToBinLogEventPositionMap.clear();
    }

    @Override
    public BinLogEventPosition getLatestBinLogEventPosition(String destination) {
        return destinationToBinLogEventPositionMap.get(destination);
    }

    @Override
    public void persistBinLogEventPosition(String destination, BinLogEventPosition binLogEventPosition) throws ParseException {
        destinationToBinLogEventPositionMap.put(destination, binLogEventPosition);
    }

    public Set<String> destinations() {
        return destinationToBinLogEventPositionMap.keySet();
    }

}
