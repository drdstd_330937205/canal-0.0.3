package drds.data_propagate.parse;

import drds.data_propagate.driver.packets.GtidSet;
import drds.data_propagate.parse.multistage_coordinator.MultistageCoordinator;

import java.io.IOException;

public interface Dumper {
    public long queryServerId() throws IOException;

    public void connect() throws IOException;

    public void disconnect() throws IOException;

    public void reconnect() throws IOException;

    Dumper fork();

    //MultistageCoordinator
    public void dump(MultistageCoordinator multistageCoordinator, GtidSet gtidSet) throws IOException;

    public void dump(MultistageCoordinator multistageCoordinator, long timestamp) throws IOException;

    public void dump(MultistageCoordinator multistageCoordinator, String binlogFileName, Long binlogPosition) throws IOException;


    //Sink
    public void dump(Sink sink, GtidSet gtidSet) throws IOException;

    public void dump(Sink sink, long timestamp) throws IOException;

    public void dump(Sink sink, String binlogFileName, Long binlogPosition) throws IOException;


    /**
     * 用于快速数据查找,和dump的区别在于，seek会只给出部分的数据
     */
    public void seek(String binlogFileName, Long binlogPosition, String gtid, Sink sink)
            throws IOException;


}
