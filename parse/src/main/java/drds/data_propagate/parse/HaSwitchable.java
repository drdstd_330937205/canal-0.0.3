package drds.data_propagate.parse;

/**
 * 支持可切换的数据复制控制器
 */
public interface HaSwitchable {

    public void doSwitch();

    public void doSwitch(AuthenticationInfo newAuthenticationInfo);
}
