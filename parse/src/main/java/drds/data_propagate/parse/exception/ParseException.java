package drds.data_propagate.parse.exception;

import drds.data_propagate.common.DataPropagateException;

public class ParseException extends DataPropagateException {

    private static final long serialVersionUID = -7288830284122672209L;

    public ParseException(String errorCode) {
        super(errorCode);
    }

    public ParseException(String errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    public ParseException(String errorCode, String errorDesc) {
        super(errorCode + ":" + errorDesc);
    }

    public ParseException(String errorCode, String errorDesc, Throwable cause) {
        super(errorCode + ":" + errorDesc, cause);
    }

    public ParseException(Throwable cause) {
        super(cause);
    }

}
