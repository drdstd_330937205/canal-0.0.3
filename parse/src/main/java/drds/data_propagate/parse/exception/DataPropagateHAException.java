package drds.data_propagate.parse.exception;

import drds.data_propagate.common.DataPropagateException;

public class DataPropagateHAException extends DataPropagateException {

    private static final long serialVersionUID = -7288830284122672209L;

    public DataPropagateHAException(String errorCode) {
        super(errorCode);
    }

    public DataPropagateHAException(String errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    public DataPropagateHAException(String errorCode, String errorDesc) {
        super(errorCode + ":" + errorDesc);
    }

    public DataPropagateHAException(String errorCode, String errorDesc, Throwable cause) {
        super(errorCode + ":" + errorDesc, cause);
    }

    public DataPropagateHAException(Throwable cause) {
        super(cause);
    }

}
