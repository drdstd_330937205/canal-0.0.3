package drds.data_propagate.parse;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import drds.data_propagate.entry.Entry;
import drds.data_propagate.parse.table_meta_data.TableMetaData;
import lombok.Getter;
import lombok.Setter;

public class MessageEvent {

    @Setter
    @Getter
    private Buffer buffer;
    @Setter
    @Getter
    private Entry entry;
    @Setter
    @Getter
    private boolean needDmlParse = false;
    @Setter
    @Getter
    private TableMetaData tableMetaData;
    @Setter
    @Getter
    private BinLogEvent binLogEvent;

}
