package drds.data_propagate.parse;

public interface ParserExceptionHandler {

    void handle(Throwable e);
}
