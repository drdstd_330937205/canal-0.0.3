package drds.data_propagate.parse;


public interface Sink<Event> {

    public boolean sink(Event event);
}
