package drds.data_propagate.parse.multistage_coordinator;

import com.lmax.disruptor.ExceptionHandler;

public class ExceptionHandlerImpl implements ExceptionHandler {

    @Override
    public void handleEventException(final Throwable ex, final long sequence, final Object event) {
    }

    @Override
    public void handleOnStartException(final Throwable ex) {
    }

    @Override
    public void handleOnShutdownException(final Throwable ex) {
    }
}
