package drds.data_propagate.instance;

/**
 * @author zebin.xuzb @ 2012-7-12
 * @version 1.0.0
 */
public interface InstanceGenerator {

    /**
     * 通过 destination 产生特定的 {@link Instance}
     *
     * @param destination
     * @return
     */
    Instance generate(String destination);
}
