package drds.data_propagate.instance;

import drds.data_propagate.common.LifeCycle;
import drds.data_propagate.entry.ClientId;
import drds.data_propagate.metadata.MetaDataManager;
import drds.data_propagate.parse.EventParser;
import drds.data_propagate.sink.EventSink;
import drds.data_propagate.store.EventStore;

/**
 * 代表单个canal实例，比如一个destination会独立一个实例
 */
public interface Instance extends LifeCycle {

    String getDestination();

    EventParser getEventParser();

    EventSink getEventSink();

    EventStore getEventStore();

    MetaDataManager getMetaDataManager();

    /**
     * 客户端发生订阅/取消订阅行为
     */
    boolean subscribe(ClientId identity);


}
