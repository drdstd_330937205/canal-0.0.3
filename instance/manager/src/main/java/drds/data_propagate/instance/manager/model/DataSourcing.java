package drds.data_propagate.instance.manager.model;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.net.InetSocketAddress;

/**
 * 数据来源描述
 */
public class DataSourcing implements Serializable {

    private static final long serialVersionUID = -1770648468678085234L;
    @Setter
    @Getter

    private SourcingType type;
    @Setter
    @Getter

    private InetSocketAddress dbAddress;

    public DataSourcing() {

    }

    public DataSourcing(SourcingType type, InetSocketAddress dbAddress) {
        this.type = type;
        this.dbAddress = dbAddress;
    }


}
