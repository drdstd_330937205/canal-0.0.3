package drds.data_propagate.instance.manager.model;

public enum StorageScavengeMode {
    /**
     * 在存储满的时候触发
     */
    ON_FULL,
    /**
     * 在每次有ack请求时触发
     */
    ON_ACK,
    /**
     * 定时触发，需要外部控制
     */
    ON_SCHEDULE;

    public boolean isOnFull() {
        return this.equals(StorageScavengeMode.ON_FULL);
    }

    public boolean isOnAck() {
        return this.equals(StorageScavengeMode.ON_ACK);
    }

    public boolean isOnSchedule() {
        return this.equals(StorageScavengeMode.ON_SCHEDULE);
    }

}
