package drds.data_propagate.instance.manager.model;

import drds.data_propagate.common.utils.ToStringStyle;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

public class Task implements Serializable {

    private static final long serialVersionUID = 8333284022624682754L;
    @Setter
    @Getter
    private Long id;
    @Setter
    @Getter
    private String name; // 对应的名字
    @Setter
    @Getter
    private String desc; // 描述

    @Setter
    @Getter
    private Parameter parameter; // 参数定义
    @Setter
    @Getter
    private Date gmtCreate; // 创建时间
    @Setter
    @Getter
    private Date gmtModified; // 修改时间

    public String toString() {
        return ToStringStyle.toString(this);
    }

}
