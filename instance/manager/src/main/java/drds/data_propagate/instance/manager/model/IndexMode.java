package drds.data_propagate.instance.manager.model;

public enum IndexMode {
    /**
     * 内存存储模式
     */
    MEMORY,
    /**
     * 文件存储模式
     */
    ZOOKEEPER,
    /**
     * 混合模式，内存+文件
     */
    MIXED,
    /**
     * 基于meta信息
     */
    META,
    /**
     * 基于内存+meta的failback实现
     */
    MEMORY_META_FAILBACK;

    public boolean isMemory() {
        return this.equals(IndexMode.MEMORY);
    }

    public boolean isZookeeper() {
        return this.equals(IndexMode.ZOOKEEPER);
    }

    public boolean isMixed() {
        return this.equals(IndexMode.MIXED);
    }

    public boolean isMeta() {
        return this.equals(IndexMode.META);
    }

    public boolean isMemoryMetaFailback() {
        return this.equals(IndexMode.MEMORY_META_FAILBACK);
    }
}
