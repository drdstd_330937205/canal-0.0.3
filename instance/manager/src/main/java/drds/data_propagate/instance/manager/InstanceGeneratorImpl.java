package drds.data_propagate.instance.manager;

import drds.data_propagate.instance.InstanceGenerator;
import drds.data_propagate.instance.manager.model.Task;


public class InstanceGeneratorImpl implements InstanceGenerator {

    private TaskManager taskManager;

    public drds.data_propagate.instance.Instance generate(String destination) {
        Task task = taskManager.findTask(destination);
        String filter = taskManager.findFilter(destination);
        return new Instance(task, filter);
    }


    public void setTaskManager(TaskManager taskManager) {
        this.taskManager = taskManager;
    }

}
