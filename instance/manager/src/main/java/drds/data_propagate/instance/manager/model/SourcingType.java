package drds.data_propagate.instance.manager.model;

public enum SourcingType {
    /**
     * mysql DB
     */
    MYSQL,


    /**
     * 多库合并模式
     */
    GROUP;

    public boolean isMysql() {
        return this.equals(SourcingType.MYSQL);
    }


    public boolean isGroup() {
        return this.equals(SourcingType.GROUP);
    }
}
