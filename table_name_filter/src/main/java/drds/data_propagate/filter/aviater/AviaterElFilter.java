package drds.data_propagate.filter.aviater;

import com.googlecode.aviator.AviatorEvaluator;
import drds.data_propagate.entry.Entry;
import drds.data_propagate.filter.EventFilter;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 基于aviater el表达式的匹配过滤
 */
public class AviaterElFilter implements EventFilter<Entry> {

    public static final String ROOT_KEY = "entry";
    private String expression;

    public AviaterElFilter(String expression) {
        this.expression = expression;
    }

    public boolean filter(Entry entry) {
        if (StringUtils.isEmpty(expression)) {
            return true;
        }

        Map<String, Object> env = new HashMap<String, Object>();
        env.put(ROOT_KEY, entry);
        return (Boolean) AviatorEvaluator.execute(expression, env);
    }

}
