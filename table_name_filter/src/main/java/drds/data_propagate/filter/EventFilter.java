package drds.data_propagate.filter;


/**
 * 数据过滤机制
 */
public interface EventFilter<Event> {

    boolean filter(Event event);
}
