package drds.data_propagate.binlog_event;

import drds.data_propagate.binlog_event.event.QueryEvent;
import drds.data_propagate.binlog_event.event.RotateEvent;
import drds.data_propagate.binlog_event.event.RowsQueryEvent;
import drds.data_propagate.binlog_event.event.XidEvent;
import drds.data_propagate.binlog_event.event.mariadb.AnnotateRowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.DeleteRowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.UpdateRowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.WriteRowsEvent;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

@Ignore
public class DirectAbstractFetcherTest extends BaseLogFetcherTest {

    @Test
    public void testSimple() {
        PacketReader fecther = new PacketReader();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306", "root", "hello");
            Statement statement = connection.createStatement();
            statement.execute("SET @master_binlog_checksum='@@global.binlog_checksum'");
            statement.execute("SET @mariadb_slave_capability='" + BinLogEvent.maria_slave_capability_mine + "'");

            fecther.open(connection, 2, "mysql-bin.000007", 89797036L);

            BinLogEventDecoder binLogEventDecoder = new BinLogEventDecoder(BinLogEvent.unknown_event, BinLogEvent.enum_end_event);
            BinLogEventsContext context = new BinLogEventsContext();
            while (fecther.fetchNextBinlogEvent()) {
                BinLogEvent event = binLogEventDecoder.decode(fecther, context);
                int eventType = event.getHeader().getEventType();
                switch (eventType) {
                    case BinLogEvent.rotate_event:
                        binlogFileName = ((RotateEvent) event).getFilename();
                        break;
                    case BinLogEvent.write_rows_event_v1:
                    case BinLogEvent.write_rows_event:
                        parseRowsEvent((WriteRowsEvent) event);
                        break;
                    case BinLogEvent.update_rows_event_v1:
                    case BinLogEvent.partial_update_rows_event:
                    case BinLogEvent.update_rows_event:
                        parseRowsEvent((UpdateRowsEvent) event);
                        break;
                    case BinLogEvent.delete_rows_event_v1:
                    case BinLogEvent.delete_rows_event:
                        parseRowsEvent((DeleteRowsEvent) event);
                        break;
                    case BinLogEvent.query_event:
                        parseQueryEvent((QueryEvent) event);
                        break;
                    case BinLogEvent.rows_query_log_event:
                        parseRowsQueryEvent((RowsQueryEvent) event);
                        break;
                    case BinLogEvent.annotate_rows_event:
                        parseAnnotateRowsEvent((AnnotateRowsEvent) event);
                        break;
                    case BinLogEvent.xid_event:
                        parseXidEvent((XidEvent) event);
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        } finally {
            try {
                fecther.close();
            } catch (IOException e) {
                Assert.fail(e.getMessage());
            }
        }

    }
}
