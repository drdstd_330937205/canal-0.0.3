package drds.data_propagate.binlog_event;

import drds.data_propagate.binlog_event.event.*;
import drds.data_propagate.binlog_event.event.mariadb.AnnotateRowsEvent;
import drds.data_propagate.binlog_event.event.rows_event.RowsEvent;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.BitSet;

public class BaseLogFetcherTest {

    protected String binlogFileName = "mysql-bin.000001";
    protected Charset charset = Charset.forName("utf-8");

    protected void parseQueryEvent(QueryEvent event) {
        System.out.println(String.format("================> binlog_event[%s:%s] , name[%s]", binlogFileName,
                event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength(),
                event.getCatalog()));
        System.out.println("sql : " + event.getQueryString());
    }

    protected void parseRowsQueryEvent(RowsQueryEvent event) throws Exception {
        System.out.println(String.format("================> binlog_event[%s:%s]", binlogFileName,
                event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength()));
        System.out.println("sql : " + new String(event.getRowsQuery().getBytes("ISO-8859-1"), charset.name()));
    }

    protected void parseAnnotateRowsEvent(AnnotateRowsEvent event) throws Exception {
        System.out.println(String.format("================> binlog_event[%s:%s]", binlogFileName,
                event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength()));
        System.out.println("sql : " + new String(event.getRowsQuery().getBytes("ISO-8859-1"), charset.name()));
    }

    protected void parseXidEvent(XidEvent event) throws Exception {
        System.out.println(String.format("================> binlog_event[%s:%s]", binlogFileName,
                event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength()));
        System.out.println("xid : " + event.getXid());
    }

    protected void parseRowsEvent(RowsEvent event) {
        try {
            System.out.println(String.format("================> binlog_event[%s:%s] , name[%s,%s]", binlogFileName,
                    event.getHeader().getNextBinLogEventPosition() - event.getHeader().getEventLength(),
                    event.getTableMapEvent().getSchemaName(), event.getTableMapEvent().getTableName()));
            RowsEventBuffer buffer = event.getRowsEventBuffer(charset.name());
            BitSet columns = event.getColumnBitSet();
            BitSet changeColumns = event.getChangeColumnBitSet();
            while (buffer.nextOneRow(columns)) {
                // 处理row记录
                int type = event.getHeader().getEventType();
                if (BinLogEvent.write_rows_event_v1 == type || BinLogEvent.write_rows_event == type) {
                    // insert的记录放在before字段中
                    parseOneRow(event, buffer, columns, true);
                } else if (BinLogEvent.delete_rows_event_v1 == type || BinLogEvent.delete_rows_event == type) {
                    // delete的记录放在before字段中
                    parseOneRow(event, buffer, columns, false);
                } else {
                    // update需要处理before/after
                    System.out.println("-------> before");
                    parseOneRow(event, buffer, columns, false);
                    if (!buffer.nextOneRow(changeColumns, true)) {
                        break;
                    }
                    System.out.println("-------> after");
                    parseOneRow(event, buffer, changeColumns, true);
                }

            }
        } catch (Exception e) {
            throw new RuntimeException("parse row data failed.", e);
        }
    }

    protected void parseOneRow(RowsEvent event, RowsEventBuffer buffer, BitSet cols, boolean isAfter)
            throws UnsupportedEncodingException {
        TableMapEvent map = event.getTableMapEvent();
        if (map == null) {
            throw new RuntimeException("not found TableMap with tid=" + event.getTableId());
        }

        final int columnCnt = map.getColumnCount();
        final TableMapEvent.ColumnInfo[] columnInfo = map.getColumnInfos();

        for (int i = 0; i < columnCnt; i++) {
            if (!cols.get(i)) {
                continue;
            }

            TableMapEvent.ColumnInfo info = columnInfo[i];
            buffer.nextValue(null, i, info.type, info.meta);

            if (buffer.isFNull()) {
                //
            } else {
                final Serializable value = buffer.getValue();
                if (value instanceof byte[]) {
                    System.out.println(new String((byte[]) value));
                } else {
                    System.out.println(value);
                }
            }
        }

    }
}
