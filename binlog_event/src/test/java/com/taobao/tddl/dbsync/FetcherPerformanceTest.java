package com.taobao.tddl.dbsync;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.PacketReader;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.concurrent.atomic.AtomicLong;

public class FetcherPerformanceTest {

    public static void main(String args[]) {
        PacketReader fetcher = new PacketReader();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306", "root", "hello");
            Statement statement = connection.createStatement();
            statement.execute("SET @master_binlog_checksum='@@global.binlog_checksum'");
            statement.execute("SET @mariadb_slave_capability='" + BinLogEvent.maria_slave_capability_mine + "'");

            fetcher.open(connection, 2, "mysql-bin.000006", 120L);

            AtomicLong sum = new AtomicLong(0);
            long start = System.currentTimeMillis();
            long last = 0;
            long end = 0;

            while (fetcher.fetchNextBinlogEvent()) {
                sum.incrementAndGet();
                long current = sum.get();
                if (current - last >= 100000) {
                    end = System.currentTimeMillis();
                    long tps = ((current - last) * 1000) / (end - start);
                    System.out.println(" total : " + sum + " , cost : " + (end - start) + " , tps : " + tps);
                    last = current;
                    start = end;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fetcher.close();
            } catch (IOException e) {
            }
        }
    }
}
