package drds.data_propagate.binlog_event;

import drds.data_propagate.binlog_event.event.FormatDescriptionEvent;
import drds.data_propagate.binlog_event.event.GtidEvent;
import drds.data_propagate.binlog_event.event.TableMapEvent;
import drds.data_propagate.driver.packets.GtidSet;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Document Me!! NOTE: Log context will NOT write multi-threaded.
 */
public final class BinLogEventsContext {

    @Setter
    @Getter
    private final Map<Long, TableMapEvent> tableIdToTableMapEventMap = new HashMap<Long, TableMapEvent>();
    /**
     * 共用
     */
    @Setter
    @Getter
    private FormatDescriptionEvent formatDescriptionEvent;
    @Setter
    @Getter
    private BinlogEventPosition binlogEventPosition;
    @Setter
    @Getter
    private GtidSet gtidSet;
    @Setter
    @Getter
    private GtidEvent gtidEvent; // save current gtid log event

    public BinLogEventsContext() {
        this.formatDescriptionEvent = FormatDescriptionEvent.format_description_event_5_x;
    }

    public final void clearAllTables() {
        tableIdToTableMapEventMap.clear();
    }
}
