package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Serializable;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.BitSet;

/**
 * Extracting JDBC eventType & value information from packed rows-bytes.
 *
 * @see mysql-5.1.60/sql/log_event.cc - Rows_log_event::print_verbose_one_row
 */
public final class RowsEventBuffer {

    public static final long datetimef_int_ofs = 0x8000000000l;
    public static final long timef_int_ofs = 0x800000l;
    public static final long timef_ofs = 0x800000000000l;
    protected static final Log logger = LogFactory.getLog(RowsEventBuffer.class);
    private static char[] digits = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    @Setter
    @Getter
    private final Buffer buffer;
    @Setter
    @Getter
    private final int columnLen;
    @Setter
    @Getter
    private final int jsonColumnCount;
    @Setter
    @Getter
    private final String charsetName;
    @Setter
    @Getter
    private final BitSet nullBits;
    // Read value_options if this is AI for PARTIAL_UPDATE_ROWS_EVENT
    @Setter
    @Getter
    private final boolean partial;
    @Setter
    @Getter
    private final BitSet partialBits;
    @Setter
    @Getter
    private int nullBitIndex;
    @Setter
    @Getter
    private boolean fNull;
    @Setter
    @Getter
    private int javaType;
    @Setter
    @Getter
    private int length;
    @Setter
    @Getter
    private Serializable value;

    public RowsEventBuffer(Buffer buffer, final int columnLen, String charsetName, int jsonColumnCount, boolean partial) {
        this.buffer = buffer;
        this.columnLen = columnLen;
        this.charsetName = charsetName;
        this.partial = partial;
        this.jsonColumnCount = jsonColumnCount;
        this.nullBits = new BitSet(columnLen);
        this.partialBits = new BitSet(1);
    }

    /**
     * Maps the given MySQL eventType decode the correct JDBC eventType.
     */
    static int mysqlToJavaType(int type, final int meta, boolean isBinary) {
        int javaType;

        if (type == BinLogEvent.mysql_type_string) {
            if (meta >= 256) {
                int byte0 = meta >> 8;
                if ((byte0 & 0x30) != 0x30) {
                    /* a long CHAR() field: see #37426 */
                    type = byte0 | 0x30;
                } else {
                    switch (byte0) {
                        case BinLogEvent.mysql_type_set:
                        case BinLogEvent.mysql_type_enum:
                        case BinLogEvent.mysql_type_string:
                            type = byte0;
                    }
                }
            }
        }

        switch (type) {
            case BinLogEvent.mysql_type_long:
                javaType = Types.INTEGER;
                break;

            case BinLogEvent.mysql_type_tiny:
                javaType = Types.TINYINT;
                break;

            case BinLogEvent.mysql_type_short:
                javaType = Types.SMALLINT;
                break;

            case BinLogEvent.mysql_type_int24:
                javaType = Types.INTEGER;
                break;

            case BinLogEvent.mysql_type_longlong:
                javaType = Types.BIGINT;
                break;

            case BinLogEvent.mysql_type_decimal:
                javaType = Types.DECIMAL;
                break;

            case BinLogEvent.mysql_type_newdecimal:
                javaType = Types.DECIMAL;
                break;

            case BinLogEvent.mysql_type_float:
                javaType = Types.REAL; // Types.FLOAT;
                break;

            case BinLogEvent.mysql_type_double:
                javaType = Types.DOUBLE;
                break;

            case BinLogEvent.mysql_type_bit:
                javaType = Types.BIT;
                break;

            case BinLogEvent.mysql_type_timestamp:
            case BinLogEvent.mysql_type_datetime:
            case BinLogEvent.mysql_type_timestamp2:
            case BinLogEvent.mysql_type_datetime2:
                javaType = Types.TIMESTAMP;
                break;

            case BinLogEvent.mysql_type_time:
            case BinLogEvent.mysql_type_time2:
                javaType = Types.TIME;
                break;

            case BinLogEvent.mysql_type_newdate:
            case BinLogEvent.mysql_type_date:
                javaType = Types.DATE;
                break;

            case BinLogEvent.mysql_type_year:
                javaType = Types.VARCHAR;
                break;

            case BinLogEvent.mysql_type_enum:
                javaType = Types.INTEGER;
                break;

            case BinLogEvent.mysql_type_set:
                javaType = Types.BINARY;
                break;

            case BinLogEvent.mysql_type_tiny_blob:
            case BinLogEvent.mysql_type_medium_blob:
            case BinLogEvent.mysql_type_long_blob:
            case BinLogEvent.mysql_type_blob:
                if (meta == 1) {
                    javaType = Types.VARBINARY;
                } else {
                    javaType = Types.LONGVARBINARY;
                }
                break;

            case BinLogEvent.mysql_type_varchar:
            case BinLogEvent.mysql_type_var_string:
                if (isBinary) {
                    // varbinary在binlog中为var_string类型
                    javaType = Types.VARBINARY;
                } else {
                    javaType = Types.VARCHAR;
                }
                break;

            case BinLogEvent.mysql_type_string:
                if (isBinary) {
                    // binary在binlog中为string类型
                    javaType = Types.BINARY;
                } else {
                    javaType = Types.CHAR;
                }
                break;

            case BinLogEvent.mysql_type_geometry:
                javaType = Types.BINARY;
                break;

            // case BinLogEvent.MYSQL_TYPE_BINARY:
            // javaType = Types.BINARY;
            // break;
            //
            // case BinLogEvent.MYSQL_TYPE_VARBINARY:
            // javaType = Types.VARBINARY;
            // break;

            default:
                javaType = Types.OTHER;
        }

        return javaType;
    }

    public static String usecondsToStr(int frac, int meta) {
        String sec = String.valueOf(frac);
        if (meta > 6) {
            throw new IllegalArgumentException("unknow useconds meta : " + meta);
        }

        if (sec.length() < 6) {
            StringBuilder result = new StringBuilder(6);
            int len = 6 - sec.length();
            for (; len > 0; len--) {
                result.append('0');
            }
            result.append(sec);
            sec = result.toString();
        }

        return sec.substring(0, meta);
    }

    public static void appendNumber4(StringBuilder builder, int d) {
        if (d >= 1000) {
            builder.append(digits[d / 1000]).append(digits[(d / 100) % 10]).append(digits[(d / 10) % 10])
                    .append(digits[d % 10]);
        } else {
            builder.append('0');
            appendNumber3(builder, d);
        }
    }

    public static void appendNumber3(StringBuilder builder, int d) {
        if (d >= 100) {
            builder.append(digits[d / 100]).append(digits[(d / 10) % 10]).append(digits[d % 10]);
        } else {
            builder.append('0');
            appendNumber2(builder, d);
        }
    }

    public static void appendNumber2(StringBuilder builder, int d) {
        if (d >= 10) {
            builder.append(digits[(d / 10) % 10]).append(digits[d % 10]);
        } else {
            builder.append('0').append(digits[d]);
        }
    }

    public final boolean nextOneRow(BitSet columns) {
        return nextOneRow(columns, false);
    }

    /**
     * Extracting next row from packed bytes.
     *
     * @see mysql-5.1.60/sql/log_event.cc - Rows_log_event::print_verbose_one_row
     */
    public final boolean nextOneRow(BitSet columns, boolean after) {
        final boolean hasOneRow = buffer.hasRemaining();

        if (hasOneRow) {
            int column = 0;

            for (int i = 0; i < columnLen; i++)
                if (columns.get(i)) {
                    column++;
                }

            if (after && partial) {
                partialBits.clear();
                long valueOptions = buffer.getPackedLength();
                int PARTIAL_JSON_UPDATES = 1;
                if ((valueOptions & PARTIAL_JSON_UPDATES) != 0) {
                    partialBits.set(1);
                    buffer.forward((jsonColumnCount + 7) / 8);
                }
            }
            nullBitIndex = 0;
            nullBits.clear();
            buffer.fillBitmap(nullBits, column);

        }
        return hasOneRow;
    }

    /**
     * Extracting next field value from packed bytes.
     *
     * @see mysql-5.1.60/sql/log_event.cc - Rows_log_event::print_verbose_one_row
     */
    public final Serializable nextValue(final String columName, final int columnIndex, final int type, final int meta) {
        return nextValue(columName, columnIndex, type, meta, false);
    }

    /**
     * Extracting next field value from packed bytes.
     *
     * @see mysql-5.1.60/sql/log_event.cc - Rows_log_event::print_verbose_one_row
     */
    public final Serializable nextValue(final String columName, final int columnIndex, final int type, final int meta,
                                        boolean isBinary) {
        fNull = nullBits.get(nullBitIndex++);

        if (fNull) {
            value = null;
            javaType = mysqlToJavaType(type, meta, isBinary);
            length = 0;
            return null;
        } else {
            // Extracting field value from packed bytes.
            return fetchValue(columName, columnIndex, type, meta, isBinary);
        }
    }

    /**
     * Extracting next field value from packed bytes.
     *
     * @see mysql-5.1.60/sql/log_event.cc - log_event_print_value
     */
    final Serializable fetchValue(String columnName, int columnIndex, int type, final int meta, boolean isBinary) {
        int len = 0;

        if (type == BinLogEvent.mysql_type_string) {
            if (meta >= 256) {
                int byte0 = meta >> 8;
                int byte1 = meta & 0xff;
                if ((byte0 & 0x30) != 0x30) {
                    /* a long CHAR() field: see #37426 */
                    len = byte1 | (((byte0 & 0x30) ^ 0x30) << 4);
                    type = byte0 | 0x30;
                } else {
                    switch (byte0) {
                        case BinLogEvent.mysql_type_set:
                        case BinLogEvent.mysql_type_enum:
                        case BinLogEvent.mysql_type_string:
                            type = byte0;
                            len = byte1;
                            break;
                        default:
                            throw new IllegalArgumentException(String.format(
                                    "!! Don't know how decode handle column eventType=%d meta=%d (%04X)", type, meta, meta));
                    }
                }
            } else {
                len = meta;
            }
        }

        switch (type) {
            case BinLogEvent.mysql_type_long: {
                // XXX: How decode check signed / unsigned?
                // value = unsigned ? Long.valueOf(bytes.getLittleEndian32UnsignedLong()) :
                // Integer.valueOf(bytes.getLittleEndian32SignedInt());
                value = Integer.valueOf(buffer.getNextLittleEndian32SignedInt());
                javaType = Types.INTEGER;
                length = 4;
                break;
            }
            case BinLogEvent.mysql_type_tiny: {
                // XXX: How decode check signed / unsigned?
                // value = Integer.valueOf(unsigned ? bytes.get8UnsignedInt() :
                // bytes.get8SignedInt());
                value = Integer.valueOf(buffer.getNext8SignedInt());
                javaType = Types.TINYINT; // java.sql.Types.INTEGER;
                length = 1;
                break;
            }
            case BinLogEvent.mysql_type_short: {
                // XXX: How decode check signed / unsigned?
                // value = Integer.valueOf(unsigned ? bytes.getLittleEndian16UnsignedInt() :
                // bytes.getLittleEndian16SignedInt());
                value = Integer.valueOf((short) buffer.getNextLittleEndian16SignedInt());
                javaType = Types.SMALLINT; // java.sql.Types.INTEGER;
                length = 2;
                break;
            }
            case BinLogEvent.mysql_type_int24: {
                // XXX: How decode check signed / unsigned?
                // value = Integer.valueOf(unsigned ? bytes.getLittleEndian24UnsignedInt() :
                // bytes.getLittleEndian24SignedInt());
                value = Integer.valueOf(buffer.getNextLittleEndian24SignedInt());
                javaType = Types.INTEGER;
                length = 3;
                break;
            }
            case BinLogEvent.mysql_type_longlong: {
                // XXX: How decode check signed / unsigned?
                // value = unsigned ? bytes.getLittleEndian64UnsignedLong()) :
                // Long.valueOf(bytes.getNextLittleEndian64SignedLong());
                value = Long.valueOf(buffer.getNextLittleEndian64SignedLong());
                javaType = Types.BIGINT; // Types.INTEGER;
                length = 8;
                break;
            }
            case BinLogEvent.mysql_type_decimal: {
                /*
                 * log_event.h : This enumeration value is only used internally and cannot exist
                 * in a binlog_event.
                 */
                logger.warn("MYSQL_TYPE_DECIMAL : This enumeration value is "
                        + "only used internally and cannot exist in a binlog_event!");
                javaType = Types.DECIMAL;
                value = null; /* unknown format */
                length = 0;
                break;
            }
            case BinLogEvent.mysql_type_newdecimal: {
                final int precision = meta >> 8;
                final int decimals = meta & 0xff;
                value = buffer.getDecimal(precision, decimals);
                javaType = Types.DECIMAL;
                length = precision;
                break;
            }
            case BinLogEvent.mysql_type_float: {
                value = Float.valueOf(buffer.getNextLittleEndian32Float());
                javaType = Types.REAL; // Types.FLOAT;
                length = 4;
                break;
            }
            case BinLogEvent.mysql_type_double: {
                value = Double.valueOf(buffer.getNextLittleEndian64Double());
                javaType = Types.DOUBLE;
                length = 8;
                break;
            }
            case BinLogEvent.mysql_type_bit: {
                /* Meta-data: bit_len, bytes_in_rec, 2 bytes */
                final int nbits = ((meta >> 8) * 8) + (meta & 0xff);
                len = (nbits + 7) / 8;
                if (nbits > 1) {
                    // byte[] bits = new byte[len];
                    // bytes.getBytes(bits, 0, len);
                    // 转化为unsign long
                    switch (len) {
                        case 1:
                            value = buffer.getNext8UnsignedInt();
                            break;
                        case 2:
                            value = buffer.getNextBigEndian16UnsignedInt();
                            break;
                        case 3:
                            value = buffer.getNextBigEndian24UnsignedInt();
                            break;
                        case 4:
                            value = buffer.getNextBigEndian32UnsignedLong();
                            break;
                        case 5:
                            value = buffer.getNextBigEndian40UnsignedLong();
                            break;
                        case 6:
                            value = buffer.getNextBigEndian48UnsignedLong();
                            break;
                        case 7:
                            value = buffer.getNextBigEndian56UnsignedLong();
                            break;
                        case 8:
                            value = buffer.getNextBigEndian64UnsignedLong();
                            break;
                        default:
                            throw new IllegalArgumentException("!! Unknown Bit len = " + len);
                    }
                } else {
                    final int bit = buffer.getNext8SignedInt();
                    // value = (bit != 0) ? Boolean.TRUE : Boolean.FALSE;
                    value = bit;
                }
                javaType = Types.BIT;
                length = nbits;
                break;
            }
            case BinLogEvent.mysql_type_timestamp: {
                // MYSQL DataTypes: TIMESTAMP
                // range is '1970-01-01 00:00:01' UTC decode '2038-01-19 03:14:07'
                // UTC
                // A TIMESTAMP cannot represent the value '1970-01-01 00:00:00'
                // because that is equivalent decode 0 seconds from the epoch and
                // the value 0 is reserved for representing '0000-00-00
                // 00:00:00', the “zero” TIMESTAMP value.
                final long i32 = buffer.getNextLittleEndian32UnsignedLong();
                if (i32 == 0) {
                    value = "0000-00-00 00:00:00";
                } else {
                    String v = new Timestamp(i32 * 1000).toString();
                    value = v.substring(0, v.length() - 2);
                }
                javaType = Types.TIMESTAMP;
                length = 4;
                break;
            }
            case BinLogEvent.mysql_type_timestamp2: {
                final long tv_sec = buffer.getNextBigEndian32UnsignedLong(); // big-endian
                int tv_usec = 0;
                switch (meta) {
                    case 0:
                        tv_usec = 0;
                        break;
                    case 1:
                    case 2:
                        tv_usec = buffer.getNext8SignedInt() * 10000;
                        break;
                    case 3:
                    case 4:
                        tv_usec = buffer.getNextBigEndian16SignedInt() * 100;
                        break;
                    case 5:
                    case 6:
                        tv_usec = buffer.getNextBigEndian24SignedInt();
                        break;
                    default:
                        tv_usec = 0;
                        break;
                }

                String second = null;
                if (tv_sec == 0) {
                    second = "0000-00-00 00:00:00";
                } else {
                    Timestamp time = new Timestamp(tv_sec * 1000);
                    second = time.toString();
                    second = second.substring(0, second.length() - 2);// 去掉毫秒精度.0
                }

                if (meta >= 1) {
                    String microSecond = usecondsToStr(tv_usec, meta);
                    microSecond = microSecond.substring(0, meta);
                    value = second + '.' + microSecond;
                } else {
                    value = second;
                }

                javaType = Types.TIMESTAMP;
                length = 4 + (meta + 1) / 2;
                break;
            }
            case BinLogEvent.mysql_type_datetime: {
                // MYSQL DataTypes: DATETIME
                // range is '0000-01-01 00:00:00' decode '9999-12-31 23:59:59'
                final long i64 = buffer.getNextLittleEndian64SignedLong(); /* YYYYMMDDhhmmss */
                if (i64 == 0) {
                    value = "0000-00-00 00:00:00";
                } else {
                    final int d = (int) (i64 / 1000000);
                    final int t = (int) (i64 % 1000000);
                    // if (cal == null) cal = Calendar.getInstance();
                    // cal.clear();
                    /* month is 0-based, 0 for january. */
                    // cal.set(d / 10000, (d % 10000) / 100 - 1, d % 100, t /
                    // 10000, (t % 10000) / 100, t % 100);
                    // value = new Timestamp(cal.getTimeInMillis());
                    // value = String.format("%04d-%02d-%02d %02d:%02d:%02d",
                    // d / 10000,
                    // (d % 10000) / 100,
                    // d % 100,
                    // t / 10000,
                    // (t % 10000) / 100,
                    // t % 100);

                    StringBuilder builder = new StringBuilder();
                    appendNumber4(builder, d / 10000);
                    builder.append('-');
                    appendNumber2(builder, (d % 10000) / 100);
                    builder.append('-');
                    appendNumber2(builder, d % 100);
                    builder.append(' ');
                    appendNumber2(builder, t / 10000);
                    builder.append(':');
                    appendNumber2(builder, (t % 10000) / 100);
                    builder.append(':');
                    appendNumber2(builder, t % 100);
                    value = builder.toString();
                }
                javaType = Types.TIMESTAMP;
                length = 8;
                break;
            }
            case BinLogEvent.mysql_type_datetime2: {
                /*
                 * DATETIME and DATE low-level memory and disk representation routines 1 bit
                 * sign (used executeTimeStamp on disk) 17 bits year*13+month (year 0-9999,
                 * month 0-12) 5 bits day (0-31) 5 bits hour (0-23) 6 bits minute (0-59) 6 bits
                 * second (0-59) 24 bits microseconds (0-999999) Total: 64 bits = 8 bytes
                 * SYYYYYYY.YYYYYYYY .YYdddddh.hhhhmmmm.mmssssss.ffffffff.ffffffff.ffffffff
                 */
                long intpart = buffer.getNextBigEndian40UnsignedLong() - datetimef_int_ofs; // big-endian
                int frac = 0;
                switch (meta) {
                    case 0:
                        frac = 0;
                        break;
                    case 1:
                    case 2:
                        frac = buffer.getNext8SignedInt() * 10000;
                        break;
                    case 3:
                    case 4:
                        frac = buffer.getNextBigEndian16SignedInt() * 100;
                        break;
                    case 5:
                    case 6:
                        frac = buffer.getNextBigEndian24SignedInt();
                        break;
                    default:
                        frac = 0;
                        break;
                }

                String second = null;
                if (intpart == 0) {
                    second = "0000-00-00 00:00:00";
                } else {
                    // 构造TimeStamp只处理到秒
                    long ymd = intpart >> 17;
                    long ym = ymd >> 5;
                    long hms = intpart % (1 << 17);

                    // if (cal == null) cal = Calendar.getInstance();
                    // cal.clear();
                    // cal.set((int) (ym / 13), (int) (ym % 13) - 1, (int) (ymd
                    // % (1 << 5)), (int) (hms >> 12),
                    // (int) ((hms >> 6) % (1 << 6)), (int) (hms % (1 << 6)));
                    // value = new Timestamp(cal.getTimeInMillis());
                    // second = String.format("%04d-%02d-%02d %02d:%02d:%02d",
                    // (int) (ym / 13),
                    // (int) (ym % 13),
                    // (int) (ymd % (1 << 5)),
                    // (int) (hms >> 12),
                    // (int) ((hms >> 6) % (1 << 6)),
                    // (int) (hms % (1 << 6)));

                    StringBuilder builder = new StringBuilder(26);
                    appendNumber4(builder, (int) (ym / 13));
                    builder.append('-');
                    appendNumber2(builder, (int) (ym % 13));
                    builder.append('-');
                    appendNumber2(builder, (int) (ymd % (1 << 5)));
                    builder.append(' ');
                    appendNumber2(builder, (int) (hms >> 12));
                    builder.append(':');
                    appendNumber2(builder, (int) ((hms >> 6) % (1 << 6)));
                    builder.append(':');
                    appendNumber2(builder, (int) (hms % (1 << 6)));
                    second = builder.toString();
                }

                if (meta >= 1) {
                    String microSecond = usecondsToStr(frac, meta);
                    microSecond = microSecond.substring(0, meta);
                    value = second + '.' + microSecond;
                } else {
                    value = second;
                }

                javaType = Types.TIMESTAMP;
                length = 5 + (meta + 1) / 2;
                break;
            }
            case BinLogEvent.mysql_type_time: {
                // MYSQL DataTypes: TIME
                // The range is '-838:59:59' decode '838:59:59'
                // final int i32 = bytes.getLittleEndian24UnsignedInt();
                final int i32 = buffer.getNextLittleEndian24SignedInt();
                final int u32 = Math.abs(i32);
                if (i32 == 0) {
                    value = "00:00:00";
                } else {
                    // if (cal == null) cal = Calendar.getInstance();
                    // cal.clear();
                    // cal.set(70, 0, 1, i32 / 10000, (i32 % 10000) / 100, i32 %
                    // 100);
                    // value = new Time(cal.getTimeInMillis());
                    // value = String.format("%s%02d:%02d:%02d",
                    // (i32 >= 0) ? "" : "-",
                    // u32 / 10000,
                    // (u32 % 10000) / 100,
                    // u32 % 100);

                    StringBuilder builder = new StringBuilder(17);
                    if (i32 < 0) {
                        builder.append('-');
                    }

                    int d = u32 / 10000;
                    if (d > 100) {
                        builder.append(String.valueOf(d));
                    } else {
                        appendNumber2(builder, d);
                    }
                    builder.append(':');
                    appendNumber2(builder, (u32 % 10000) / 100);
                    builder.append(':');
                    appendNumber2(builder, u32 % 100);
                    value = builder.toString();
                }
                javaType = Types.TIME;
                length = 3;
                break;
            }
            case BinLogEvent.mysql_type_time2: {
                /*
                 * TIME low-level memory and disk representation routines In-memory format: 1
                 * bit sign (Used for sign, executeTimeStamp on disk) 1 bit unused (Reserved for
                 * wider hour range, e.g. for intervalList) 10 bit hour (0-836) 6 bit minute (0-59)
                 * 6 bit second (0-59) 24 bits microseconds (0-999999) Total: 48 bits = 6 bytes
                 * Suhhhhhh.hhhhmmmm.mmssssss.ffffffff.ffffffff.ffffffff
                 */
                long intpart = 0;
                int frac = 0;
                long ltime = 0;
                switch (meta) {
                    case 0:
                        intpart = buffer.getNextBigEndian24UnsignedInt() - timef_int_ofs; // big-endian
                        ltime = intpart << 24;
                        break;
                    case 1:
                    case 2:
                        intpart = buffer.getNextBigEndian24UnsignedInt() - timef_int_ofs;
                        frac = buffer.getNext8UnsignedInt();
                        if (intpart < 0 && frac > 0) {
                            /*
                             * Negative values are stored with reverse fractional part order, for binary
                             * sort compatibility. Disk value intpart frac Time value Memory value 800000.00
                             * 0 0 00:00:00.00 0000000000.000000 7FFFFF.FF -1 255 -00:00:00.01
                             * FFFFFFFFFF.FFD8F0 7FFFFF.9D -1 99 -00:00:00.99 FFFFFFFFFF.F0E4D0 7FFFFF.00 -1
                             * 0 -00:00:01.00 FFFFFFFFFF.000000 7FFFFE.FF -1 255 -00:00:01.01
                             * FFFFFFFFFE.FFD8F0 7FFFFE.F6 -2 246 -00:00:01.10 FFFFFFFFFE.FE7960 Formula decode
                             * convert fractional part from disk format (now stored in "frac" variable) decode
                             * absolute value: "0x100 - frac". To reconstruct in-memory value, we shift decode
                             * the next integer value and then substruct fractional part.
                             */
                            intpart++; /* Shift decode the next integer value */
                            frac -= 0x100; /* -(0x100 - frac) */
                            // fraclong = frac * 10000;
                        }
                        frac = frac * 10000;
                        ltime = intpart << 24;
                        break;
                    case 3:
                    case 4:
                        intpart = buffer.getNextBigEndian24UnsignedInt() - timef_int_ofs;
                        frac = buffer.getNextBigEndian16UnsignedInt();
                        if (intpart < 0 && frac > 0) {
                            /*
                             * Fix reverse fractional part order: "0x10000 - frac". See comments for FSP=1
                             * and FSP=2 above.
                             */
                            intpart++; /* Shift decode the next integer value */
                            frac -= 0x10000; /* -(0x10000-frac) */
                            // fraclong = frac * 100;
                        }
                        frac = frac * 100;
                        ltime = intpart << 24;
                        break;
                    case 5:
                    case 6:
                        intpart = buffer.getNextBigEndian48UnsignedLong() - timef_ofs;
                        ltime = intpart;
                        frac = (int) (intpart % (1L << 24));
                        break;
                    default:
                        intpart = buffer.getNextBigEndian24UnsignedInt() - timef_int_ofs;
                        ltime = intpart << 24;
                        break;
                }

                String second = null;
                if (intpart == 0) {
                    second = "00:00:00";
                } else {
                    // 目前只记录秒，不处理us frac
                    // if (cal == null) cal = Calendar.getInstance();
                    // cal.clear();
                    // cal.set(70, 0, 1, (int) ((intpart >> 12) % (1 << 10)),
                    // (int) ((intpart >> 6) % (1 << 6)),
                    // (int) (intpart % (1 << 6)));
                    // value = new Time(cal.getTimeInMillis());
                    long ultime = Math.abs(ltime);
                    intpart = ultime >> 24;
                    // second = String.format("%s%02d:%02d:%02d",
                    // ltime >= 0 ? "" : "-",
                    // (int) ((intpart >> 12) % (1 << 10)),
                    // (int) ((intpart >> 6) % (1 << 6)),
                    // (int) (intpart % (1 << 6)));

                    StringBuilder builder = new StringBuilder(12);
                    if (ltime < 0) {
                        builder.append('-');
                    }

                    int d = (int) ((intpart >> 12) % (1 << 10));
                    if (d > 100) {
                        builder.append(String.valueOf(d));
                    } else {
                        appendNumber2(builder, d);
                    }
                    builder.append(':');
                    appendNumber2(builder, (int) ((intpart >> 6) % (1 << 6)));
                    builder.append(':');
                    appendNumber2(builder, (int) (intpart % (1 << 6)));
                    second = builder.toString();
                }

                if (meta >= 1) {
                    String microSecond = usecondsToStr(Math.abs(frac), meta);
                    microSecond = microSecond.substring(0, meta);
                    value = second + '.' + microSecond;
                } else {
                    value = second;
                }

                javaType = Types.TIME;
                length = 3 + (meta + 1) / 2;
                break;
            }
            case BinLogEvent.mysql_type_newdate: {
                /*
                 * log_event.h : This enumeration value is only used internally and cannot exist
                 * in a binlog_event.
                 */
                logger.warn("MYSQL_TYPE_NEWDATE : This enumeration value is "
                        + "only used internally and cannot exist in a binlog_event!");
                javaType = Types.DATE;
                value = null; /* unknown format */
                length = 0;
                break;
            }
            case BinLogEvent.mysql_type_date: {
                // MYSQL DataTypes:
                // range: 0000-00-00 ~ 9999-12-31
                final int i32 = buffer.getNextLittleEndian24UnsignedInt();
                if (i32 == 0) {
                    value = "0000-00-00";
                } else {
                    // if (cal == null) cal = Calendar.getInstance();
                    // cal.clear();
                    /* month is 0-based, 0 for january. */
                    // cal.set((i32 / (16 * 32)), (i32 / 32 % 16) - 1, (i32 %
                    // 32));
                    // value = new java.sql.Date(cal.getTimeInMillis());
                    // value = String.format("%04d-%02d-%02d", i32 / (16 * 32),
                    // i32 / 32 % 16, i32 % 32);

                    StringBuilder builder = new StringBuilder(12);
                    appendNumber4(builder, i32 / (16 * 32));
                    builder.append('-');
                    appendNumber2(builder, i32 / 32 % 16);
                    builder.append('-');
                    appendNumber2(builder, i32 % 32);
                    value = builder.toString();
                }
                javaType = Types.DATE;
                length = 3;
                break;
            }
            case BinLogEvent.mysql_type_year: {
                // MYSQL DataTypes: YEAR[(2|4)]
                // In four-digit format, values display as 1901 decode 2155, and
                // 0000.
                // In two-digit format, values display as 70 decode 69, representing
                // years from 1970 decode 2069.

                final int i32 = buffer.getNext8UnsignedInt();
                // If connection property 'YearIsDateType' has
                // set, value is java.sql.Date.
                /*
                 * if (cal == null) cal = Calendar.getInstance(); cal.clear();
                 * cal.set(Calendar.YEAR, i32 + 1900); value = new
                 * java.sql.Date(cal.getTimeInMillis());
                 */
                // The else, value is java.lang.Short.
                if (i32 == 0) {
                    value = "0000";
                } else {
                    value = String.valueOf((short) (i32 + 1900));
                }
                // It might seem more correct decode create a java.sql.Types.DATE
                // value
                // for this date, but it is much simpler decode pass the value as an
                // integer. The MySQL JDBC specification states that one can
                // pass a java int between 1901 and 2055. Creating a DATE value
                // causes truncation errors with certain SQL_MODES
                // (e.g."STRICT_TRANS_TABLES").
                javaType = Types.VARCHAR; // Types.INTEGER;
                length = 1;
                break;
            }
            case BinLogEvent.mysql_type_enum: {
                final int int32;
                /*
                 * log_event.h : This enumeration value is only used internally and cannot exist
                 * in a binlog_event.
                 */
                switch (len) {
                    case 1:
                        int32 = buffer.getNext8UnsignedInt();
                        break;
                    case 2:
                        int32 = buffer.getNextLittleEndian16UnsignedInt();
                        break;
                    default:
                        throw new IllegalArgumentException("!! Unknown ENUM packlen = " + len);
                }
                // logger.warn("MYSQL_TYPE_ENUM : This enumeration value is "
                // + "only used internally and cannot exist in a binlog_event!");
                value = Integer.valueOf(int32);
                javaType = Types.INTEGER;
                length = len;
                break;
            }
            case BinLogEvent.mysql_type_set: {
                final int nbits = (meta & 0xFF) * 8;
                len = (nbits + 7) / 8;
                if (nbits > 1) {
                    // byte[] bits = new byte[len];
                    // bytes.getBytes(bits, 0, len);
                    // 转化为unsign long
                    switch (len) {
                        case 1:
                            value = buffer.getNext8UnsignedInt();
                            break;
                        case 2:
                            value = buffer.getNextLittleEndian16UnsignedInt();
                            break;
                        case 3:
                            value = buffer.getNextLittleEndian24UnsignedInt();
                            break;
                        case 4:
                            value = buffer.getNextLittleEndian32UnsignedLong();
                            break;
                        case 5:
                            value = buffer.getNextLittleEndian40UnsignedLong();
                            break;
                        case 6:
                            value = buffer.getNextLittleEndian48UnsignedLong();
                            break;
                        case 7:
                            value = buffer.getNextLittleEndian56UnsignedLong();
                            break;
                        case 8:
                            value = buffer.getNextLittleEndian64UnsignedLong();
                            break;
                        default:
                            throw new IllegalArgumentException("!! Unknown Set len = " + len);
                    }
                } else {
                    final int bit = buffer.getNext8SignedInt();
                    // value = (bit != 0) ? Boolean.TRUE : Boolean.FALSE;
                    value = bit;
                }

                javaType = Types.BIT;
                length = len;
                break;
            }
            case BinLogEvent.mysql_type_tiny_blob: {
                /*
                 * log_event.h : This enumeration value is only used internally and cannot exist
                 * in a binlog_event.
                 */
                logger.warn("MYSQL_TYPE_TINY_BLOB : This enumeration value is "
                        + "only used internally and cannot exist in a binlog_event!");
            }
            case BinLogEvent.mysql_type_medium_blob: {
                /*
                 * log_event.h : This enumeration value is only used internally and cannot exist
                 * in a binlog_event.
                 */
                logger.warn("MYSQL_TYPE_MEDIUM_BLOB : This enumeration value is "
                        + "only used internally and cannot exist in a binlog_event!");
            }
            case BinLogEvent.mysql_type_long_blob: {
                /*
                 * log_event.h : This enumeration value is only used internally and cannot exist
                 * in a binlog_event.
                 */
                logger.warn("MYSQL_TYPE_LONG_BLOB : This enumeration value is "
                        + "only used internally and cannot exist in a binlog_event!");
            }
            case BinLogEvent.mysql_type_blob: {
                /*
                 * BLOB or TEXT datatype
                 */
                switch (meta) {
                    case 1: {
                        /* TINYBLOB/TINYTEXT */
                        final int len8 = buffer.getNext8UnsignedInt();
                        byte[] binary = new byte[len8];
                        buffer.getBytes(binary, 0, len8);
                        value = binary;
                        javaType = Types.VARBINARY;
                        length = len8;
                        break;
                    }
                    case 2: {
                        /* BLOB/TEXT */
                        final int len16 = buffer.getNextLittleEndian16UnsignedInt();
                        byte[] binary = new byte[len16];
                        buffer.getBytes(binary, 0, len16);
                        value = binary;
                        javaType = Types.LONGVARBINARY;
                        length = len16;
                        break;
                    }
                    case 3: {
                        /* MEDIUMBLOB/MEDIUMTEXT */
                        final int len24 = buffer.getNextLittleEndian24UnsignedInt();
                        byte[] binary = new byte[len24];
                        buffer.getBytes(binary, 0, len24);
                        value = binary;
                        javaType = Types.LONGVARBINARY;
                        length = len24;
                        break;
                    }
                    case 4: {
                        /* LONGBLOB/LONGTEXT */
                        final int len32 = (int) buffer.getNextLittleEndian32UnsignedLong();
                        byte[] binary = new byte[len32];
                        buffer.getBytes(binary, 0, len32);
                        value = binary;
                        javaType = Types.LONGVARBINARY;
                        length = len32;
                        break;
                    }
                    default:
                        throw new IllegalArgumentException("!! Unknown BLOB packlen = " + meta);
                }
                break;
            }
            case BinLogEvent.mysql_type_varchar:
            case BinLogEvent.mysql_type_var_string: {
                /*
                 * Except for the data length calculation, MYSQL_TYPE_VARCHAR,
                 * MYSQL_TYPE_VAR_STRING and MYSQL_TYPE_STRING are handled the same way.
                 */
                len = meta;
                if (len < 256) {
                    len = buffer.getNext8UnsignedInt();
                } else {
                    len = buffer.getNextLittleEndian16UnsignedInt();
                }

                if (isBinary) {
                    // fixed issue #66 ,binary类型在binlog中为var_string
                    /* fill binary */
                    byte[] binary = new byte[len];
                    buffer.getBytes(binary, 0, len);

                    javaType = Types.VARBINARY;
                    value = binary;
                } else {
                    value = buffer.getFixLengthStringWithoutNullTerminateCheck(len, charsetName);
                    javaType = Types.VARCHAR;
                }

                length = len;
                break;
            }
            case BinLogEvent.mysql_type_string: {
                if (len < 256) {
                    len = buffer.getNext8UnsignedInt();
                } else {
                    len = buffer.getNextLittleEndian16UnsignedInt();
                }

                if (isBinary) {
                    /* fill binary */
                    byte[] binary = new byte[len];
                    buffer.getBytes(binary, 0, len);

                    javaType = Types.BINARY;
                    value = binary;
                } else {
                    value = buffer.getFixLengthStringWithoutNullTerminateCheck(len, charsetName);
                    javaType = Types.CHAR; // Types.VARCHAR;
                }
                length = len;
                break;
            }
            case BinLogEvent.mysql_type_json:
                throw new UnsupportedOperationException("不支持json");
            case BinLogEvent.mysql_type_geometry: {
                /*
                 * MYSQL_TYPE_GEOMETRY: copy from BLOB or TEXT
                 */
                switch (meta) {
                    case 1:
                        len = buffer.getNext8UnsignedInt();
                        break;
                    case 2:
                        len = buffer.getNextLittleEndian16UnsignedInt();
                        break;
                    case 3:
                        len = buffer.getNextLittleEndian24UnsignedInt();
                        break;
                    case 4:
                        len = (int) buffer.getNextLittleEndian32UnsignedLong();
                        break;
                    default:
                        throw new IllegalArgumentException("!! Unknown MYSQL_TYPE_GEOMETRY packlen = " + meta);
                }
                /* fill binary */
                byte[] binary = new byte[len];
                buffer.getBytes(binary, 0, len);

                /* Warning unsupport cloumn eventType */
                // logger.warn(String.format("!! Unsupport column eventType MYSQL_TYPE_GEOMETRY:
                // meta=%d (%04X), len = %d",
                // meta,
                // meta,
                // len));
                javaType = Types.BINARY;
                value = binary;
                length = len;
                break;
            }
            default:
                logger.error(
                        String.format("!! Don't know how decode handle column eventType=%d meta=%d (%04X)", type, meta, meta));
                javaType = Types.OTHER;
                value = null;
                length = 0;
        }

        return value;
    }


}
