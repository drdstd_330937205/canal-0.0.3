package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;

public class XaPrepareEvent extends BinLogEvent {
    @Setter
    @Getter
    private boolean onePhase;
    @Setter
    @Getter
    private int formatId;
    @Setter
    @Getter
    private int gtridLength;
    @Setter
    @Getter
    private int bqualLength;
    @Setter
    @Getter
    private byte[] data;

    public XaPrepareEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        final int commonHeaderLength = formatDescriptionEvent.getCommonHeaderLength();
        final int eventPostHeaderLength = formatDescriptionEvent.getEventPostHeaderLength()[header.getEventType() - 1];

        int offset = commonHeaderLength + eventPostHeaderLength;
        buffer.newEffectiveInitialIndex(offset);

        onePhase = (buffer.getNext8SignedInt() == 0x00 ? false : true);

        formatId = buffer.getNextLittleEndian32SignedInt();
        gtridLength = buffer.getNextLittleEndian32SignedInt();
        bqualLength = buffer.getNextLittleEndian32SignedInt();

        int MY_XIDDATASIZE = 128;
        if (MY_XIDDATASIZE >= gtridLength + bqualLength && //
                gtridLength >= 0 && //
                gtridLength <= 64 && //
                bqualLength >= 0 && //
                bqualLength <= 64) {//
            data = buffer.getBytes(gtridLength + bqualLength);
        } else {
            formatId = -1;
            gtridLength = 0;
            bqualLength = 0;
        }
    }


}
