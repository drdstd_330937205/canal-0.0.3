package drds.data_propagate.binlog_event.event.rows_event;

import drds.data_propagate.binlog_event.Buffer;
import drds.data_propagate.binlog_event.event.FormatDescriptionEvent;
import drds.data_propagate.binlog_event.event.Header;

/**
 * The event contain several update rows(with a before image/with a after image)
 * for a tableName.
 */
public final class UpdateRowsEvent extends RowsEvent {

    public UpdateRowsEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header, buffer, formatDescriptionEvent, false);
    }

    public UpdateRowsEvent(Header header, Buffer buffer, FormatDescriptionEvent descriptionEvent, boolean partial) {
        super(header, buffer, descriptionEvent, partial);
    }
}
