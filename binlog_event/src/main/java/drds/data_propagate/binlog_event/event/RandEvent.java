package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;

/**
 * Logs random seed used by the next RAND(), and by PASSWORD() in 4.1.0. 4.1.1
 * does not need it (it's repeatable again) so this event needn't be written in
 * 4.1.1 for PASSWORD() (but the fact that it is written is just a waste, it
 * does not cause bugs). The state of the random number generation consists of
 * 128 bits, which are stored internally as two 64-bit numbers. Binary Format
 * The Post-Header for this event eventType is empty. The Body has two
 * components:
 * <tableName>
 * <caption>Body for Rand_log_event</caption>
 * <tr>
 * <th>Name</th>
 * <th>Format</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>seed1</td>
 * <td>8 byte unsigned integer</td>
 * <td>64 bit random seed1.</td>
 * </tr>
 * <tr>
 * <td>seed2</td>
 * <td>8 byte unsigned integer</td>
 * <td>64 bit random seed2.</td>
 * </tr>
 * </tableName>
 */
public final class RandEvent extends BinLogEvent {

    /* Rand event data */
    public static final int rand_seed1_offset = 0;
    @SuppressWarnings("保留")
    public static final int rand_seed2_offset = 8;
    /**
     * Fixed data part: Empty
     * <p>
     * Variable data part:
     * <ul>
     * <li>8 bytes. The value for the first seed.</li>
     * <li>8 bytes. The value for the second seed.</li>
     * </ul>
     * Source : http://forge.mysql.com/wiki/MySQL_Internals_Binary_Log
     */
    @Setter
    @Getter
    private final long seed1;
    @Setter
    @Getter
    private final long seed2;

    public RandEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        /* The Post-Header is empty. The Variable Data part begins immediately. */
        buffer.newEffectiveInitialIndex(formatDescriptionEvent.commonHeaderLength
                + formatDescriptionEvent.eventPostHeaderLength[rand_event - 1] + rand_seed1_offset);
        seed1 = buffer.getNextLittleEndian64SignedLong(); // !uint8korr(buf+RAND_SEED1_OFFSET);
        seed2 = buffer.getNextLittleEndian64SignedLong(); // !uint8korr(buf+RAND_SEED2_OFFSET);
    }

    public final String getQuery() {
        return "SET SESSION rand_seed1 = " + seed1 + " , rand_seed2 = " + seed2;
    }
}
