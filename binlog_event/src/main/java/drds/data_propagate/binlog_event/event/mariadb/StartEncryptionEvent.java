package drds.data_propagate.binlog_event.event.mariadb;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import drds.data_propagate.binlog_event.event.FormatDescriptionEvent;
import drds.data_propagate.binlog_event.event.Header;

public class StartEncryptionEvent extends BinLogEvent {

    public StartEncryptionEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);
    }
}
