package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;

/**
 * Delete_file_log_event.
 *
 * @author <a href="mailto:changyuan.lh@taobao.com">Changyuan.lh</a>
 * @version 1.0
 */
public final class DeleteFileEvent extends BinLogEvent {

    /* df = "delete file" */
    public static final int df_file_id_offset = 0;
    @Setter
    @Getter
    private final long fileId;

    public DeleteFileEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        final int commonHeaderLength = formatDescriptionEvent.commonHeaderLength;
        buffer.newEffectiveInitialIndex(commonHeaderLength + df_file_id_offset);
        fileId = buffer.getNextLittleEndian32UnsignedLong(); // DF_FILE_ID_OFFSET
    }

}
