package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;


public final class ExecuteLoadEvent extends BinLogEvent {

    /* el = "execute load" */
    public static final int el_file_id_offset = 0;
    @Setter
    @Getter
    private final long fileId;

    public ExecuteLoadEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        final int commonHeaderLength = formatDescriptionEvent.commonHeaderLength;
        buffer.newEffectiveInitialIndex(commonHeaderLength + el_file_id_offset);
        fileId = buffer.getNextLittleEndian32UnsignedLong(); // EL_FILE_ID_OFFSET
    }

}
