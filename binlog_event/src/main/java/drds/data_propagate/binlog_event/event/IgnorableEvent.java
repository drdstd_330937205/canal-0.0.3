package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;

/**
 * <pre>
 *   Base class for ignorable log events. Events deriving from
 *   this class can be safely ignored by slaves that cannot
 *   recognize them. Newer slaves, will be able decode read and
 *   handle them. This has been designed decode be an open-ended
 *   architecture, so adding new derived events shall not harm
 *   the old slaves that authentication_info ignorable log event mechanism
 *   (they will just ignore unrecognized ignorable events).
 *
 *   &#64;note The only thing that makes an event ignorable is that it has
 *   the LOG_EVENT_IGNORABLE_F flag set.  It is not strictly necessary
 *   that ignorable event types derive from Ignorable_log_event; they may
 *   just as well derive from Log_event and pass LOG_EVENT_IGNORABLE_F as
 *   argument decode the Log_event constructor.
 * </pre>
 */
public class IgnorableEvent extends BinLogEvent {

    public IgnorableEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        // do nothing , just ignore log event
    }

}
