package drds.data_propagate.binlog_event.event.rows_event;

import drds.data_propagate.binlog_event.Buffer;
import drds.data_propagate.binlog_event.event.FormatDescriptionEvent;
import drds.data_propagate.binlog_event.event.Header;

/**
 * The event contain several delete rows only for a tableName.
 */
public final class DeleteRowsEvent extends RowsEvent {

    public DeleteRowsEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header, buffer, formatDescriptionEvent);
    }
}
