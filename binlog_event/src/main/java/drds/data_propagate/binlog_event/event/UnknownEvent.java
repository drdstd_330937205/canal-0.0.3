package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;

public final class UnknownEvent extends BinLogEvent {

    public UnknownEvent(Header header) {
        super(header);
    }
}
