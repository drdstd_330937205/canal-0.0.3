package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;

public class AppendBlockEvent extends BinLogEvent {

    /* AB = "Append Block" */
    public static final int ab_file_id_offset = 0;
    @Setter
    @Getter
    private final Buffer buffer;
    @Setter
    @Getter
    private final int blockLength;
    @Setter
    @Getter
    private final long fileId;

    public AppendBlockEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        final int commonHeaderLen = formatDescriptionEvent.commonHeaderLength;
        final int postHeaderLen = formatDescriptionEvent.eventPostHeaderLength[header.eventType - 1];
        final int totalHeaderLen = commonHeaderLen + postHeaderLen;

        buffer.newEffectiveInitialIndex(commonHeaderLen + ab_file_id_offset);
        fileId = buffer.getNextLittleEndian32UnsignedLong();

        buffer.newEffectiveInitialIndex(postHeaderLen);
        blockLength = buffer.limit() - totalHeaderLen;
        this.buffer = buffer.duplicate(blockLength);
    }

    public final byte[] getData() {
        return buffer.bytesCopy();
    }
}
