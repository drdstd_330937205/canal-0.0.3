package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.Buffer;

/**
 * Event for the first block of file decode be loaded, its only difference from
 * Append_block event is that this event creates or truncates existing file
 * before writing data.
 */
public final class BeginLoadQueryEvent extends AppendBlockEvent {

    public BeginLoadQueryEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header, buffer, formatDescriptionEvent);
    }
}
