package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;

public class RowsQueryEvent extends IgnorableEvent {
    @Setter
    @Getter
    private String rowsQuery;

    public RowsQueryEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header, buffer, formatDescriptionEvent);

        final int commonHeaderLength = formatDescriptionEvent.commonHeaderLength;
        final int eventPostHeaderLength = formatDescriptionEvent.eventPostHeaderLength[header.eventType - 1];

        /*
         * m_rows_query length is stored using only one byte, but that length is ignored
         * and the complete queryString is read.
         */
        int offset = commonHeaderLength + eventPostHeaderLength + 1;
        int length = buffer.limit() - offset;
        rowsQuery = buffer.getFixLengthStringWithoutNullTerminateCheckFromEffectiveInitialIndex(offset, length,
                Buffer.ISO_8859_1);
    }


}
