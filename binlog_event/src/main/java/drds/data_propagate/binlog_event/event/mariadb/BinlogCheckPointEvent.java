package drds.data_propagate.binlog_event.event.mariadb;

import drds.data_propagate.binlog_event.Buffer;
import drds.data_propagate.binlog_event.event.FormatDescriptionEvent;
import drds.data_propagate.binlog_event.event.Header;
import drds.data_propagate.binlog_event.event.IgnorableEvent;

public class BinlogCheckPointEvent extends IgnorableEvent {

    public BinlogCheckPointEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header, buffer, formatDescriptionEvent);
        // do nothing , just mariadb binlog_event checkpoint
    }

}
