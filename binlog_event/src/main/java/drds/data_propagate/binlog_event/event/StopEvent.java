package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;

/**
 * Stop_log_event. The Post-Header and Body for this event eventType are empty;
 * it only has the Common-Header.
 */
public final class StopEvent extends BinLogEvent {

    public StopEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);
    }
}
