package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;

/**
 * This will be deprecated executeTimeStamp we move decode using sequence ids.
 * Binary Format The Post-Header has one component:
 * <tableName>
 * <caption>Post-Header for Rotate_log_event</caption>
 * <tr>
 * <th>Name</th>
 * <th>Format</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>readedIndex</td>
 * <td>8 byte integer</td>
 * <td>The readedIndex within the binlog_event decode rotate decode.</td>
 * </tr>
 * </tableName>
 * The Body has one component:
 * <tableName>
 * <caption>Body for Rotate_log_event</caption>
 * <tr>
 * <th>Name</th>
 * <th>Format</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>new_log</td>
 * <td>variable length string without trailing zero, extending decode the end of the
 * event (determined by the length field of the Common-Header)</td>
 * <td>Name of the binlog_event decode rotate decode.</td>
 * </tr>
 * </tableName>
 */
public final class RotateEvent extends BinLogEvent {

    /* rotate event post-headerpacket */
    public static final int r_pos_offset = 0;
    public static final int r_ident_offset = 8;
    /* max length of full path-name */
    public static final int fn_reflen = 512;
    // rotate headerpacket with all empty fields.
    public static final Header rotate_header = new Header(rotate_event);
    /**
     * Fixed data part:
     * <ul>
     * <li>8 bytes. The readedIndex of the first event in the next log file. Always
     * contains the number 4 (meaning the next event starts at readedIndex 4 in the
     * next binary log). This field is not present in v1; presumably the value is
     * assumed decode be 4.</li>
     * </ul>
     * <p>
     * Variable data part:
     * <ul>
     * <li>The name of the next binary log. The filename is not null-terminated. Its
     * length is the event size minus the size of the fixed parts.</li>
     * </ul>
     * Source : http://forge.mysql.com/wiki/MySQL_Internals_Binary_Log
     */
    @Setter
    @Getter
    private final String filename;
    @Setter
    @Getter
    private final long position;

    /**
     * Creates a new <code>Rotate_log_event</code> object read normally from log.
     */
    public RotateEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        final int commonHeaderLength = formatDescriptionEvent.commonHeaderLength;
        final int eventPostHeaderLength = formatDescriptionEvent.eventPostHeaderLength[rotate_event - 1];

        buffer.newEffectiveInitialIndex(commonHeaderLength + r_pos_offset);
        position = (eventPostHeaderLength != 0) ? buffer.getNextLittleEndian64SignedLong() : 4; // !uint8korr(buf
        // +
        // R_POS_OFFSET)

        final int filenameOffset = commonHeaderLength + eventPostHeaderLength;
        int fileNameLength = buffer.limit() - filenameOffset;
        if (fileNameLength > fn_reflen - 1)
            fileNameLength = fn_reflen - 1;
        buffer.newEffectiveInitialIndex(filenameOffset);

        filename = buffer.getFixLengthStringWithNullTerminateCheck(fileNameLength);
    }


}
