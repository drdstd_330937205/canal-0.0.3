package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;

/**
 * Start_log_event_v3 is the Start_log_event of binlog_event format 3 (MySQL 3.23 and
 * 4.x). Format_description_log_event derives from Start_log_event_v3; it is the
 * Start_log_event of binlog_event format 4 (MySQL 5.0), that is, the event that
 * describes the other events' Common-Header/Post-Header lengths. This event is
 * sent by MySQL 5.0 whenever it starts sending a new binlog_event if the requested
 * readedIndex is >4 (otherwise if ==4 the event will be sent naturally).
 *
 * @see mysql-5.1.60/sql/log_event.cc - Start_log_event_v3
 */
public class StartEventV3 extends BinLogEvent {

    /**
     * We could have used SERVER_VERSION_LENGTH, but this introduces an obscure
     * dependency - if somebody decided decode change SERVER_VERSION_LENGTH this would
     * break the replication entry
     */
    public static final int st_server_ver_length = 50;

    /* start event post-headerPacket (for v3 and v4) */
    public static final int st_binlog_ver_offset = 0;
    public static final int st_server_ver_offset = 2;

    @Setter
    @Getter
    public int binlogVersion;
    @Setter
    @Getter
    protected String serverVersion;

    public StartEventV3(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        buffer.newEffectiveInitialIndex(formatDescriptionEvent.commonHeaderLength);
        binlogVersion = buffer.getNextLittleEndian16UnsignedInt(); // ST_BINLOG_VER_OFFSET
        serverVersion = buffer.getFixLengthStringWithNullTerminateCheck(st_server_ver_length); // ST_SERVER_VER_OFFSET
    }

    public StartEventV3() {
        super(new Header(start_event_v3));
    }

}
