package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;


public class PreviousGtidsEvent extends BinLogEvent {

    public PreviousGtidsEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);
        // do nothing , just for mysql gtid search function
    }
}
