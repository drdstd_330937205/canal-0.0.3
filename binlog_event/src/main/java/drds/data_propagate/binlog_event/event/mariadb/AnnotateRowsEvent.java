package drds.data_propagate.binlog_event.event.mariadb;

import drds.data_propagate.binlog_event.Buffer;
import drds.data_propagate.binlog_event.event.FormatDescriptionEvent;
import drds.data_propagate.binlog_event.event.Header;
import drds.data_propagate.binlog_event.event.IgnorableEvent;
import lombok.Getter;
import lombok.Setter;

public class AnnotateRowsEvent extends IgnorableEvent {
    @Setter
    @Getter
    private String rowsQuery;

    public AnnotateRowsEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header, buffer, formatDescriptionEvent);

        final int commonHeaderLen = formatDescriptionEvent.getCommonHeaderLength();
        final int postHeaderLen = formatDescriptionEvent.getEventPostHeaderLength()[header.getEventType() - 1];

        int offset = commonHeaderLen + postHeaderLen;
        int len = buffer.limit() - offset;
        rowsQuery = buffer.getFixLengthStringWithoutNullTerminateCheckFromEffectiveInitialIndex(offset, len,
                Buffer.ISO_8859_1);
    }

}
