package drds.data_propagate.binlog_event.event.mariadb;

import drds.data_propagate.binlog_event.Buffer;
import drds.data_propagate.binlog_event.event.FormatDescriptionEvent;
import drds.data_propagate.binlog_event.event.Header;
import drds.data_propagate.binlog_event.event.IgnorableEvent;
import lombok.Getter;
import lombok.Setter;

public class MariaGtidEvent extends IgnorableEvent {
    @Setter
    @Getter
    private long gtid;

    /**
     * <pre>
     * mariadb gtidlog event format
     *     uint<8> GTID sequence
     *     uint<4> Replication Domain ID
     *     uint<1> Flags
     *
     * 	if flag & FL_GROUP_COMMIT_ID
     * 	    uint<8> commit_id
     * 	else
     * 	    uint<6> 0
     * </pre>
     */

    public MariaGtidEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header, buffer, formatDescriptionEvent);
        gtid = buffer.getNextLittleEndian64UnsignedLong().longValue();
        // do nothing , just ignore log event
    }
}
