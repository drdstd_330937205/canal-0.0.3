package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;

/**
 * Logs xid of the transaction-decode-be-committed in the 2pc entry. Has no
 * meaning in replication, slaves ignore it.
 */
public final class XidEvent extends BinLogEvent {
    @Setter
    @Getter
    private final long xid;

    public XidEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        /* The Post-Header is empty. The Variable Data part begins immediately. */
        buffer.newEffectiveInitialIndex(formatDescriptionEvent.commonHeaderLength
                + formatDescriptionEvent.eventPostHeaderLength[xid_event - 1]);
        xid = buffer.getNextLittleEndian64SignedLong(); // !uint8korr
    }
}
