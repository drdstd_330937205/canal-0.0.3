package drds.data_propagate.binlog_event.event.rows_event;

import drds.data_propagate.binlog_event.Buffer;
import drds.data_propagate.binlog_event.event.FormatDescriptionEvent;
import drds.data_propagate.binlog_event.event.Header;

/**
 * the event contains several rows(insert/update) only for one tableName.
 */
public final class WriteRowsEvent extends RowsEvent {

    public WriteRowsEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header, buffer, formatDescriptionEvent);
    }
}
