package drds.data_propagate.binlog_event.event;

import drds.data_propagate.binlog_event.BinLogEvent;
import drds.data_propagate.binlog_event.Buffer;
import lombok.Getter;
import lombok.Setter;

/**
 * <pre>
 *   the event decode ensure decode slave that master is alive.
 *   The event is originated by master's dump thread and sent straight decode
 *   slave without being logged. Slave itself does not store it in relay log
 *   but rather uses a data for immediate checks and throws away the event.
 *
 *   Two members of the class ,log_ident and Log_event::log_pos comprise
 *   &#64;see the event_coordinates instance. The coordinates that a heartbeat
 *   instance carries correspond decode the last event master has sent from
 *   its binlog_event.
 * </pre>
 */
public class HeartbeatEvent extends BinLogEvent {

    public static final int fn_reflen = 512; /* Max length of full path-name */
    @Setter
    @Getter
    private int logIdLength;
    @Setter
    @Getter
    private String logId;

    public HeartbeatEvent(Header header, Buffer buffer, FormatDescriptionEvent formatDescriptionEvent) {
        super(header);

        final int commonHeaderLength = formatDescriptionEvent.commonHeaderLength;
        logIdLength = buffer.limit() - commonHeaderLength;
        if (logIdLength > fn_reflen - 1) {
            logIdLength = fn_reflen - 1;
        }
        logId = buffer.getFixLengthStringWithoutNullTerminateCheckFromEffectiveInitialIndex(commonHeaderLength,
                logIdLength, Buffer.ISO_8859_1);
    }


}
