package drds.data_propagate.binlog_event;

import lombok.Getter;
import lombok.Setter;

public class BinlogEventPosition implements Cloneable, Comparable<BinlogEventPosition> {

    @Setter
    @Getter
    protected String fileName;


    @Setter
    @Getter
    protected long position;


    public BinlogEventPosition(String fileName, final long position) {
        this.fileName = fileName;
        this.position = position;
    }

    /* Clone binlog_event readedIndex without CloneNotSupportedException */
    public BinlogEventPosition clone() {
        try {
            return (BinlogEventPosition) super.clone();
        } catch (CloneNotSupportedException e) {
            // Never happend
            return null;
        }
    }

    public int compareTo(BinlogEventPosition binlogEventPosition) {
        final int val = fileName.compareTo(binlogEventPosition.fileName);

        if (val == 0) {
            return (int) (position - binlogEventPosition.position);
        }
        return val;
    }

    public boolean equals(Object object) {
        if (object instanceof BinlogEventPosition) {
            BinlogEventPosition binlogEventPosition = ((BinlogEventPosition) object);
            return fileName.equals(binlogEventPosition.fileName) && (this.position == binlogEventPosition.position);
        }
        return false;
    }

    public String toString() {
        return fileName + ':' + position;
    }
}
