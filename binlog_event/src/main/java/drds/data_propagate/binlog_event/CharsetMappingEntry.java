package drds.data_propagate.binlog_event;

final class CharsetMappingEntry {

    protected final int charsetId;
    protected final String mysqlCharset;
    protected final String mysqlCollation;
    protected final String javaCharset;

    CharsetMappingEntry(final int id, String mysqlCharset, String mysqlCollation, String javaCharset) {
        this.charsetId = id;
        this.mysqlCharset = mysqlCharset;
        this.mysqlCollation = mysqlCollation;
        this.javaCharset = javaCharset;
    }
}
