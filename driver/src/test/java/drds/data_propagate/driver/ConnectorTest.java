package drds.data_propagate.driver;

import drds.data_propagate.driver.packets.server.ResultSetPacket;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.InetSocketAddress;

@Ignore
public class ConnectorTest {

    @Test
    public void testQuery() {

        Connector connector = new Connector(new InetSocketAddress("127.0.0.1", 3306), "xxxxx", "xxxxx");
        try {
            connector.connect();
            QueryExecutor executor = new QueryExecutor(connector);
            ResultSetPacket result = executor.query("show variables like '%char%';");
            System.out.println(result);
            result = executor.query("select * from test.test1");
            System.out.println(result);
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        } finally {
            try {
                connector.disconnect();
            } catch (IOException e) {
                Assert.fail(e.getMessage());
            }
        }
    }

    // @Test
    public void testUpdate() {

        Connector connector = new Connector(new InetSocketAddress("127.0.0.1", 3306), "xxxxx", "xxxxx");
        try {
            connector.connect();
            UpdateExecutor executor = new UpdateExecutor(connector);
            executor.update("insert into test.test2(id,name,score,text_value) values(null,'中文1',10,'中文2')");
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        } finally {
            try {
                connector.disconnect();
            } catch (IOException e) {
                Assert.fail(e.getMessage());
            }
        }
    }
}
