package drds.data_propagate.driver;

import drds.data_propagate.driver.packets.GtidSet;
import drds.data_propagate.driver.packets.GtidSetImpl;
import drds.data_propagate.driver.packets.UuidSet;
import org.junit.Test;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by hiwjd on 2018/4/25. hiwjd0@gmail.com
 */
public class GtidSetImplTest {

    @Test
    public void testEncode() throws IOException {
        GtidSet gtidSet = GtidSetImpl.parse("726757ad-4455-11e8-ae04-0242ac110002:1");
        byte[] bytes = gtidSet.encode();

        byte[] expected = {0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x72, 0x67, 0x57, (byte) 0xad, 0x44, 0x55,
                0x11, (byte) 0xe8, (byte) 0xae, 0x04, 0x02, 0x42, (byte) 0xac, 0x11, 0x00, 0x02, 0x01, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00};

        for (int i = 0; i < bytes.length; i++) {
            assertEquals(expected[i], bytes[i]);
        }
    }

    @Test
    public void testParse() {
        Map<String, GtidSetImpl> cases = new HashMap<String, GtidSetImpl>(5);
        cases.put("726757ad-4455-11e8-ae04-0242ac110002:1",
                buildForTest(new Material("726757ad-4455-11e8-ae04-0242ac110002", 1, 2)));
        cases.put("726757ad-4455-11e8-ae04-0242ac110002:1-3",
                buildForTest(new Material("726757ad-4455-11e8-ae04-0242ac110002", 1, 4)));
        cases.put("726757ad-4455-11e8-ae04-0242ac110002:1-3:4",
                buildForTest(new Material("726757ad-4455-11e8-ae04-0242ac110002", 1, 5)));
        cases.put("726757ad-4455-11e8-ae04-0242ac110002:1-3:7-9",
                buildForTest(new Material("726757ad-4455-11e8-ae04-0242ac110002", 1, 4, 7, 10)));
        cases.put("726757ad-4455-11e8-ae04-0242ac110002:1-3,726757ad-4455-11e8-ae04-0242ac110003:4",
                buildForTest(Arrays.asList(new Material("726757ad-4455-11e8-ae04-0242ac110002", 1, 4),
                        new Material("726757ad-4455-11e8-ae04-0242ac110003", 4, 5))));

        for (Map.Entry<String, GtidSetImpl> entry : cases.entrySet()) {
            GtidSetImpl expected = entry.getValue();
            GtidSetImpl actual = GtidSetImpl.parse(entry.getKey());

            assertEquals(expected, actual);
        }
    }

    private GtidSetImpl buildForTest(Material material) {
        return buildForTest(Arrays.asList(material));
    }

    private GtidSetImpl buildForTest(List<Material> materials) {
        Map<String, UuidSet> sets = new HashMap<String, UuidSet>();
        for (Material a : materials) {
            UuidSet.Interval interval = new UuidSet.Interval();
            interval.start = a.start;
            interval.stop = a.stop;
            List<UuidSet.Interval> intervals = new ArrayList<UuidSet.Interval>();
            intervals.add(interval);

            if (a.start1 > 0 && a.stop1 > 0) {
                UuidSet.Interval interval1 = new UuidSet.Interval();
                interval1.start = a.start1;
                interval1.stop = a.stop1;
                intervals.add(interval1);
            }

            UuidSet us = new UuidSet();
            us.sid = UUID.fromString(a.uuid);
            us.intervalList = intervals;

            sets.put(a.uuid, us);
        }

        GtidSetImpl gs = new GtidSetImpl();
        gs.sidToUuidSetMap = sets;

        return gs;
    }

    private static class Material {

        public String uuid;
        public long start;
        public long stop;
        public long start1;
        public long stop1;

        public Material(String uuid, long start, long stop) {
            this.uuid = uuid;
            this.start = start;
            this.stop = stop;
            this.start1 = 0;
            this.stop1 = 0;
        }

        public Material(String uuid, long start, long stop, long start1, long stop1) {
            this.uuid = uuid;
            this.start = start;
            this.stop = stop;
            this.start1 = start1;
            this.stop1 = stop1;
        }
    }
}
