package drds.data_propagate.driver.packets.server.command_packet;

import drds.data_propagate.driver.packets.CommandPacket;
import drds.data_propagate.driver.utils.ByteHelper;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

public class AuthSwitchRequestMoreData extends CommandPacket {
    @Setter
    @Getter
    public int status;
    @Setter
    @Getter
    public byte[] authData;

    public void decode(byte[] bytes) {
        int index = 0;
        // 1. read status
        status = bytes[index];
        index += 1;
        authData = ByteHelper.readNullTerminatedBytes(bytes, index);
    }

    public byte[] encode() throws IOException {
        return null;
    }

}
