package drds.data_propagate.driver.packets.server;

import drds.data_propagate.driver.packets.AbstractPacket;
import drds.data_propagate.driver.utils.ByteHelper;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

public class ErrorPacket extends AbstractPacket {
    @Setter
    @Getter
    public byte fieldCount;
    @Setter
    @Getter
    public int errorNumber;
    @Setter
    @Getter
    public byte sqlStateMarker;
    @Setter
    @Getter
    public byte[] sqlState;
    @Setter
    @Getter
    public String message;

    /**
     * <pre>
     * VERSION 4.1
     *  Bytes                       Name
     *  -----                       ----
     *  1                           field_count, always = 0xff
     *  2                           errno
     *  1                           (sqlstate marker), always '#'
     *  5                           sqlstate (5 characters)
     *  n                           message
     *
     * </pre>
     */
    public void decode(byte[] bytes) {
        int index = 0;
        // 1. read field count
        this.fieldCount = bytes[0];
        index++;
        // 2. read error no.
        this.errorNumber = ByteHelper.readUnsignedShortLittleEndian(bytes, index);
        index += 2;
        // 3. read marker
        this.sqlStateMarker = bytes[index];
        index++;
        // 4. read sqlState
        this.sqlState = ByteHelper.readFixedLengthBytes(bytes, index, 5);
        index += 5;
        // 5. read message
        this.message = new String(ByteHelper.readFixedLengthBytes(bytes, index, bytes.length - index));
        // end read
    }

    public byte[] encode() throws IOException {
        return null;
    }


}
