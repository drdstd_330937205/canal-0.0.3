package drds.data_propagate.driver.packets.client.command_packet;

import drds.data_propagate.driver.packets.CommandPacket;
import lombok.Getter;
import lombok.Setter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class QueryPacket extends CommandPacket {
    @Setter
    @Getter
    private String queryString;

    public QueryPacket() {
        setCommand((byte) 0x03);
    }

    public void decode(byte[] data) throws IOException {
    }

    public byte[] encode() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(getCommand());
        byteArrayOutputStream.write(getQueryString().getBytes("UTF-8"));// 链接建立时默认指定编码为UTF-8
        return byteArrayOutputStream.toByteArray();
    }


}
