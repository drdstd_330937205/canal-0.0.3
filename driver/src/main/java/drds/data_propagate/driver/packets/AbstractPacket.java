package drds.data_propagate.driver.packets;

import drds.data_propagate.common.utils.ToStringStyle;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("PacketWithHeaderPacket")
public abstract class AbstractPacket implements Packet {
    @Setter
    @Getter
    protected HeaderPacket headerPacket;

    protected AbstractPacket() {
    }

    protected AbstractPacket(HeaderPacket headerPacket) {
        setHeaderPacket(headerPacket);
    }

    public String toString() {
        return ToStringStyle.toString(this);
    }

}
