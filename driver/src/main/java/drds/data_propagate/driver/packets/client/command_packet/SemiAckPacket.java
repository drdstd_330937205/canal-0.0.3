package drds.data_propagate.driver.packets.client.command_packet;

import drds.data_propagate.driver.packets.CommandPacket;
import drds.data_propagate.driver.utils.ByteHelper;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * semi ack command
 */
public class SemiAckPacket extends CommandPacket {
    @Setter
    @Getter
    public long binlogPosition;
    @Setter
    @Getter
    public String binlogFileName;

    public SemiAckPacket() {

    }

    public void decode(byte[] data) throws IOException {
    }

    /**
     * <pre>
     * Bytes                        Name
     *  --------------------------------------------------------
     *  Bytes                        Name
     *  -----                        ----
     *  1                            semi mark
     *  8                            binlog_event position to start at (little endian)
     *  n                            binlog_event file name
     *
     * </pre>
     */
    public byte[] encode() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // 0 write semi mark
        byteArrayOutputStream.write(0xef);
        // 1 write 8 bytes for position
        ByteHelper.write8ByteUnsignedIntLittleEndian(binlogPosition, byteArrayOutputStream);

        // 2 write binlog_event filename
        if (StringUtils.isNotEmpty(binlogFileName)) {
            byteArrayOutputStream.write(binlogFileName.getBytes());
        }
        return byteArrayOutputStream.toByteArray();
    }

}
