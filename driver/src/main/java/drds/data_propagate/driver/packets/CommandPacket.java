package drds.data_propagate.driver.packets;

import drds.data_propagate.common.utils.ToStringStyle;
import lombok.Getter;
import lombok.Setter;

public abstract class CommandPacket implements Packet {
    @Setter
    @Getter
    private byte command;

    public String toString() {
        return ToStringStyle.toString(this);
    }
}
