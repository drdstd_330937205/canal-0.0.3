package drds.data_propagate.driver.packets;

import java.io.IOException;


public interface GtidSet {

    /**
     * 序列化成字节数组
     */
    byte[] encode() throws IOException;

    /**
     * 更新当前实例
     */
    void update(String string);
}
