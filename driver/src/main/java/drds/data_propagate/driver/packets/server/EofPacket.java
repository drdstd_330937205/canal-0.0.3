package drds.data_propagate.driver.packets.server;

import drds.data_propagate.driver.packets.AbstractPacket;
import drds.data_propagate.driver.utils.ByteHelper;
import lombok.Getter;
import lombok.Setter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class EofPacket extends AbstractPacket {
    @Setter
    @Getter
    public byte fieldCount;
    @Setter
    @Getter
    public int warningCount;
    @Setter
    @Getter
    public int statusFlag;

    /**
     * <pre>
     *  VERSION 4.1
     *  Bytes                 Name
     *  -----                 ----
     *  1                     field_count, always = 0xfe
     *  2                     warning_count
     *  2                     Status Flags
     * </pre>
     */
    public void decode(byte[] bytes) {
        int index = 0;
        // 1. read field count
        fieldCount = bytes[index];
        index++;
        // 2. read warning count
        this.warningCount = ByteHelper.readUnsignedShortLittleEndian(bytes, index);
        index += 2;
        // 3. read status flag
        this.statusFlag = ByteHelper.readUnsignedShortLittleEndian(bytes, index);
        // end read
    }

    public byte[] encode() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(5);
        byteArrayOutputStream.write(this.fieldCount);
        ByteHelper.writeUnsignedShortLittleEndian(this.warningCount, byteArrayOutputStream);
        ByteHelper.writeUnsignedShortLittleEndian(this.statusFlag, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

}
