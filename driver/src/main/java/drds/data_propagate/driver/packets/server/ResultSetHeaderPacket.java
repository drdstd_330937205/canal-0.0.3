package drds.data_propagate.driver.packets.server;

import drds.data_propagate.driver.packets.AbstractPacket;
import drds.data_propagate.driver.utils.ByteHelper;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

/**
 * <pre>
 * Type Of Result Packet       Hexadecimal Value Of First Byte (field_count)
 * ---------------------------------------------------------------------------
 * Result Set Packet           1-250 (first byte of Length-Coded Binary)
 * </pre>
 * <p>
 * The sequence of result set packet:
 *
 * <pre>
 * (Result Set Header Packet) the number of columns (Field Packets) column
 * descriptors (EOF Packet) marker: end of Field Packets (Row Data Packets) row
 * contents (EOF Packet) marker: end of Data Packets
 *
 * <pre>
 */
public class ResultSetHeaderPacket extends AbstractPacket {
    @Setter
    @Getter
    private long columnCount;
    @Setter
    @Getter
    private long extra;

    public void decode(byte[] data) throws IOException {
        int index = 0;
        byte[] colCountBytes = ByteHelper.readBinaryCodedLengthBytes(data, index);
        columnCount = ByteHelper.readLengthCodedBinary(colCountBytes, index);
        index += colCountBytes.length;
        if (index < data.length - 1) {
            extra = ByteHelper.readLengthCodedBinary(data, index);
        }
    }

    public byte[] encode() throws IOException {
        return null;
    }


}
