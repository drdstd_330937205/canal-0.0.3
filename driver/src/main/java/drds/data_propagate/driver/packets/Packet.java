package drds.data_propagate.driver.packets;

import java.io.IOException;

/**
 * Top Abstraction for network packet.<br>
 * it exposes 2 behaviors for sub-class implementation which will be used to
 * marshal data into bytes before sending and to un-marshal data from data after
 * receiving.<br>
 */
public interface Packet {
    byte[] encode() throws IOException;

    void decode(byte[] data) throws IOException;
}
