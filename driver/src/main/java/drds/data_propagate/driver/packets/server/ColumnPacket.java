package drds.data_propagate.driver.packets.server;

import drds.data_propagate.driver.packets.AbstractPacket;
import drds.data_propagate.driver.utils.ByteHelper;
import drds.data_propagate.driver.utils.LengthCodedStringReader;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

public class ColumnPacket extends AbstractPacket {
    @Setter
    @Getter
    private String catalogName;
    @Setter
    @Getter
    private String dataBaseName;
    @Setter
    @Getter
    private String tableName;
    @Setter
    @Getter
    private String originalTableName;
    @Setter
    @Getter
    private String columnName;
    @Setter
    @Getter
    private String originalColumnName;
    @Setter
    @Getter
    private int character;
    @Setter
    @Getter
    private long length;
    @Setter
    @Getter
    private byte type;
    @Setter
    @Getter
    private int flags;
    @Setter
    @Getter
    private byte decimals;
    @Setter
    @Getter
    private String definition;

    /**
     * <pre>
     *  VERSION 4.1
     *  Bytes                      Name
     *  -----                      ----
     *  n (Length Coded String)    catalogName
     *  n (Length Coded String)    dataBaseName
     *  n (Length Coded String)    tableName
     *  n (Length Coded String)    org_table
     *  n (Length Coded String)    name
     *  n (Length Coded String)    org_name
     *  1                          (filler)
     *  2                          charsetnr
     *  4                          length
     *  1                          type
     *  2                          flags
     *  1                          decimals
     *  2                          (filler), always 0x00
     *  n (Length Coded Binary)    default
     *
     * </pre>
     */
    public void decode(byte[] bytes) throws IOException {

        int index = 0;
        LengthCodedStringReader lengthCodedStringReader = new LengthCodedStringReader(null, index);
        // 1.
        catalogName = lengthCodedStringReader.readLengthCodedString(bytes);
        // 2.
        dataBaseName = lengthCodedStringReader.readLengthCodedString(bytes);
        //
        this.tableName = lengthCodedStringReader.readLengthCodedString(bytes);
        this.originalTableName = lengthCodedStringReader.readLengthCodedString(bytes);
        //
        this.columnName = lengthCodedStringReader.readLengthCodedString(bytes);
        this.originalColumnName = lengthCodedStringReader.readLengthCodedString(bytes);
        //
        index = lengthCodedStringReader.getIndex();
        //
        index++;
        //
        this.character = ByteHelper.readUnsignedShortLittleEndian(bytes, index);
        index += 2;
        //
        this.length = ByteHelper.readUnsignedIntLittleEndian(bytes, index);
        index += 4;
        //
        this.type = bytes[index];
        index++;
        //
        this.flags = ByteHelper.readUnsignedShortLittleEndian(bytes, index);
        index += 2;
        //
        this.decimals = bytes[index];
        index++;
        //
        index += 2;// skip filter
        //
        if (index < bytes.length) {
            lengthCodedStringReader.setIndex(index);
            this.definition = lengthCodedStringReader.readLengthCodedString(bytes);
        }
    }

    public byte[] encode() throws IOException {
        return null;
    }


}
