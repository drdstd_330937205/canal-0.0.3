package drds.data_propagate.driver.packets.server;

import drds.data_propagate.driver.packets.AbstractPacket;
import drds.data_propagate.driver.utils.LengthCodedStringReader;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RowDataPacket extends AbstractPacket {
    @Setter
    @Getter
    private List<String> valueList = new ArrayList<String>();

    public void decode(byte[] bytes) throws IOException {
        int index = 0;
        LengthCodedStringReader lengthCodedStringReader = new LengthCodedStringReader(null, index);
        do {
            getValueList().add(lengthCodedStringReader.readLengthCodedString(bytes));
        } while (lengthCodedStringReader.getIndex() < bytes.length);
    }

    public byte[] encode() throws IOException {
        return null;
    }


}
