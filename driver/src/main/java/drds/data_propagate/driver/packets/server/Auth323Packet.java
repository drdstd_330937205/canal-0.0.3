package drds.data_propagate.driver.packets.server;

import drds.data_propagate.driver.packets.AbstractPacket;
import drds.data_propagate.driver.utils.ByteHelper;
import lombok.Getter;
import lombok.Setter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Auth323Packet extends AbstractPacket {
    @Setter
    @Getter
    public byte[] seed;

    public void decode(byte[] data) throws IOException {

    }

    public byte[] encode() throws IOException {
        if (seed == null) {
            return new byte[]{(byte) 0};
        } else {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ByteHelper.writeNullTerminated(seed, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        }
    }

}
