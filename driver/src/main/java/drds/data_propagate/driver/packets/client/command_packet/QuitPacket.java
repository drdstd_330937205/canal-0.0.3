package drds.data_propagate.driver.packets.client.command_packet;

import drds.data_propagate.driver.packets.CommandPacket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * quit cmd
 */
public class QuitPacket extends CommandPacket {

    public static final byte[] QUIT = new byte[]{1, 0, 0, 0, 1};

    public QuitPacket() {
        setCommand((byte) 0x01);
    }

    public void decode(byte[] bytes) throws IOException {

    }

    public byte[] encode() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(getCommand());
        byteArrayOutputStream.write(QUIT);
        return byteArrayOutputStream.toByteArray();
    }

}
