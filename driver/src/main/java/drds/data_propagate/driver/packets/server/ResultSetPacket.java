package drds.data_propagate.driver.packets.server;

import lombok.Getter;
import lombok.Setter;

import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;

public class ResultSetPacket {
    @Setter
    @Getter
    private SocketAddress socketAddress;
    @Setter
    @Getter
    private List<ColumnPacket> columnPacketList = new ArrayList<ColumnPacket>();
    @Setter
    @Getter
    private List<String> valueList = new ArrayList<String>();


}
