package drds.data_propagate.entry;

import lombok.Getter;
import lombok.Setter;


public enum EventType {

    insert(0, 1),

    update(1, 2),

    delete(2, 3),

    create(3, 4),

    alter(4, 5),

    erase(5, 6),

    query(6, 7),

    truncate(7, 8),

    rename(8, 9),

    create_index(9, 10),

    delete_index(10, 11),

    gtid(11, 12),

    xa_commit(12, 13),

    xa_rollback(13, 14),

    mheartbeat(14, 15),
    ;


    @Setter
    @Getter
    private final int index;
    @Setter
    @Getter
    private final int value;

    private EventType(int index, int value) {
        this.index = index;
        this.value = value;
    }

    public static EventType valueOf(int value) {
        switch (value) {
            case 1:
                return insert;
            case 2:
                return update;
            case 3:
                return delete;
            case 4:
                return create;
            case 5:
                return alter;
            case 6:
                return erase;
            case 7:
                return query;
            case 8:
                return truncate;
            case 9:
                return rename;
            case 10:
                return create_index;
            case 11:
                return delete_index;
            case 12:
                return gtid;
            case 13:
                return xa_commit;
            case 14:
                return xa_rollback;
            case 15:
                return mheartbeat;
            default:
                return null;
        }
    }

    public int getIndex() {
        return index;
    }

    public int getValue() {
        return value;
    }


}
