package drds.data_propagate.entry;

import lombok.Getter;
import lombok.Setter;

/**
 * <pre>
 * *每个字段的数据结构*
 * </pre>
 */
public final class Column {

    @Setter
    @Getter
    private int index;
    @Setter
    @Getter
    private int sqlType;
    @Setter
    @Getter
    private String name;
    @Setter
    @Getter
    private boolean isKey;
    @Setter
    @Getter
    private boolean updated;
    @Setter
    @Getter
    private boolean isNull;
    @Setter
    @Getter
    private java.util.List<KeyValuePair> props;
    @Setter
    @Getter
    private Object value;
    @Setter
    @Getter
    private int length;
    @Setter
    @Getter
    private Object mysqlType;

}
