package drds.data_propagate.entry;

import lombok.Getter;
import lombok.Setter;

/**
 * <pre>
 * *打散后的事件类型，主要用于标识事务的开始，变更数据，结束*
 * </pre>
 */
public enum EntryType {

    transaction_begin(0, 1),

    row_data(1, 2),

    transaction_end(2, 3),
    /**
     * <pre>
     * * 心跳类型，内部使用，外部暂不可见，可忽略 *
     * </pre>
     */
    heartbeat(3, 4),

    gtid_log(4, 5),
    ;


    @Setter
    @Getter
    private final int index;
    @Setter
    @Getter
    private final int value;

    private EntryType(int index, int value) {
        this.index = index;
        this.value = value;
    }

    public static EntryType valueOf(int value) {
        switch (value) {
            case 1:
                return transaction_begin;
            case 2:
                return row_data;
            case 3:
                return transaction_end;
            case 4:
                return heartbeat;
            case 5:
                return gtid_log;
            default:
                return null;
        }
    }

    public int getIndex() {
        return index;
    }

    public int getValue() {
        return value;
    }


}
