package drds.data_propagate.entry;

import lombok.Getter;
import lombok.Setter;

/**
 * <pre>
 * *message row 每行变更数据的数据结构*
 * </pre>
 */
public final class RowChange {

    @Setter
    @Getter
    private long tableId;
    @Setter
    @Getter
    private EventType eventType;
    @Setter
    @Getter
    private boolean ddl;
    @Setter
    @Getter
    private Object sql;
    @Setter
    @Getter
    private java.util.List<RowData> rowDataList;
    @Setter
    @Getter
    private java.util.List<KeyValuePair> propsList;
    @Setter
    @Getter
    private Object ddlSchemaName;

}
