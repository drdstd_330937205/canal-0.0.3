package drds.data_propagate.entry;

import lombok.Getter;
import lombok.Setter;

public final class Entry {

    @Setter
    @Getter
    private EntryHeader entryHeader;
    @Setter
    @Getter
    private EntryType entryType;
    @Setter
    @Getter
    private Object value;

}
