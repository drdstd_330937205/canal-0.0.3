package drds.data_propagate.entry;


import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


public class Message {

    @Setter
    @Getter
    private long id;
    @Setter
    @Getter
    private List<Entry> entryList = new ArrayList<Entry>();


    public Message(long id, List<Entry> entryList) {
        this.id = id;
        this.entryList = entryList == null ? new ArrayList<Entry>() : entryList;

    }

}
