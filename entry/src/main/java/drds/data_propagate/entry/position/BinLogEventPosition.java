package drds.data_propagate.entry.position;

import lombok.Getter;
import lombok.Setter;


public class BinLogEventPosition extends Position {

    private static final long serialVersionUID = 3875012010277005819L;
    @Setter
    @Getter
    private SlavePosition slaveId;
    @Setter
    @Getter
    private EntryPosition entryPosition;


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((slaveId == null) ? 0 : slaveId.hashCode());
        result = prime * result + ((entryPosition == null) ? 0 : entryPosition.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (!(object instanceof BinLogEventPosition)) {
            return false;
        }
        BinLogEventPosition other = (BinLogEventPosition) object;
        if (slaveId == null) {
            if (other.slaveId != null) {
                return false;
            }
        } else if (!slaveId.equals(other.slaveId)) {
            return false;
        }
        if (entryPosition == null) {
            if (other.entryPosition != null) {
                return false;
            }
        } else if (!entryPosition.equals(other.entryPosition)) {
            return false;
        }
        return true;
    }

}
