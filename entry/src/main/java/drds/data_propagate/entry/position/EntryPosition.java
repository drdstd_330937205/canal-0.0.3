package drds.data_propagate.entry.position;

import lombok.Getter;
import lombok.Setter;

/**
 * 数据库对象的唯一标示
 */
public class EntryPosition extends TimePosition {

    private static final long serialVersionUID = 81432665066427482L;

    @Setter
    @Getter
    private String journalName;
    @Setter
    @Getter
    private Long position;
    //
    @Setter
    @Getter
    private Long serverId = null; // 记录一下位点对应的serverId
    @Setter
    @Getter
    private String gtid = null;
    @Setter
    @Getter
    private boolean included = false;

    public EntryPosition() {
        super(null);
    }

    public EntryPosition(Long timestamp) {
        this(null, null, timestamp);
    }

    public EntryPosition(String journalName, Long position) {
        this(journalName, position, null);
    }

    public EntryPosition(String journalName, Long position, Long timestamp) {
        super(timestamp);
        this.journalName = journalName;
        this.position = position;
    }

    public EntryPosition(String journalName, Long position, Long timestamp, Long serverId) {
        this(journalName, position, timestamp);
        this.serverId = serverId;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((journalName == null) ? 0 : journalName.hashCode());
        result = prime * result + ((position == null) ? 0 : position.hashCode());
        // 手写equals，自动生成时需注意
        result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!super.equals(object)) {
            return false;
        }
        if (!(object instanceof EntryPosition)) {
            return false;
        }
        EntryPosition other = (EntryPosition) object;
        if (journalName == null) {
            if (other.journalName != null) {
                return false;
            }
        } else if (!journalName.equals(other.journalName)) {
            return false;
        }
        if (position == null) {
            if (other.position != null) {
                return false;
            }
        } else if (!position.equals(other.position)) {
            return false;
        }
        // 手写equals，自动生成时需注意
        if (timestamp == null) {
            if (other.timestamp != null) {
                return false;
            }
        } else if (!timestamp.equals(other.timestamp)) {
            return false;
        }
        return true;
    }


    public int compareTo(EntryPosition entryId) {
        final int compareTo = journalName.compareTo(entryId.journalName);

        if (compareTo == 0) {
            return (int) (position - entryId.position);
        }
        return compareTo;
    }

}
