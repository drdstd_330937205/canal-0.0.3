package drds.data_propagate.entry.position;

import drds.data_propagate.common.utils.ToStringStyle;

import java.io.Serializable;

/**
 * 事件唯一标示
 */
public abstract class Position implements Serializable {

    private static final long serialVersionUID = 2332798099928474975L;

    public String toString() {
        return ToStringStyle.toString(this);
    }

}
