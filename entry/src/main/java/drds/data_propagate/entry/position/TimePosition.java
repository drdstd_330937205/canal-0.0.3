package drds.data_propagate.entry.position;

import lombok.Getter;
import lombok.Setter;

/**
 * 基于时间的位置，position数据不唯一
 */
public class TimePosition extends Position {

    private static final long serialVersionUID = 6185261261064226380L;
    @Setter
    @Getter
    protected Long timestamp;

    public TimePosition(Long timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((timestamp == null) ? 0 : timestamp.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (!(object instanceof TimePosition)) {
            return false;
        }
        //
        TimePosition other = (TimePosition) object;
        if (timestamp == null) {
            if (other.timestamp != null) {
                return false;
            }
        } else if (!timestamp.equals(other.timestamp)) {
            return false;
        }
        return true;
    }

}
