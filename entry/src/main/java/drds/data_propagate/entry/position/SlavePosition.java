package drds.data_propagate.entry.position;

import lombok.Getter;
import lombok.Setter;

import java.net.InetSocketAddress;


public class SlavePosition extends Position {

    private static final long serialVersionUID = 5530225131455662581L;
    @Setter
    @Getter
    private InetSocketAddress serverInetSocketAddress; // 链接服务器的地址
    @Setter
    @Getter
    private Long slaveId; // 对应的slaveId


    public SlavePosition(InetSocketAddress serverInetSocketAddress, Long slaveId) {
        this.serverInetSocketAddress = serverInetSocketAddress;
        this.slaveId = slaveId;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((slaveId == null) ? 0 : slaveId.hashCode());
        result = prime * result + ((serverInetSocketAddress == null) ? 0 : serverInetSocketAddress.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;
        if (object == null)
            return false;
        if (getClass() != object.getClass())
            return false;
        SlavePosition other = (SlavePosition) object;
        if (slaveId == null) {
            if (other.slaveId != null)
                return false;
        } else if (slaveId.longValue() != (other.slaveId.longValue()))
            return false;
        if (serverInetSocketAddress == null) {
            if (other.serverInetSocketAddress != null)
                return false;
        } else if (!serverInetSocketAddress.equals(other.serverInetSocketAddress))
            return false;
        return true;
    }

}
