package drds.data_propagate.entry.position;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 描述一个position范围
 */
public class PositionRange<$Position extends Position> implements Serializable {

    private static final long serialVersionUID = -9162037079815694784L;
    @Setter
    @Getter
    private $Position start;
    //用于记录一个可被ack的位置，保证每次提交到cursor中的位置是一个完整事务的结束
    @Setter
    @Getter
    private $Position ack;
    @Setter
    @Getter
    private $Position end;

    public PositionRange() {
    }

    public PositionRange($Position start, $Position end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ack == null) ? 0 : ack.hashCode());
        result = prime * result + ((end == null) ? 0 : end.hashCode());
        result = prime * result + ((start == null) ? 0 : start.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null) {
            return false;
        }
        if (!(object instanceof PositionRange)) {
            return false;
        }
        PositionRange other = (PositionRange) object;
        if (ack == null) {
            if (other.ack != null) {
                return false;
            }
        } else if (!ack.equals(other.ack)) {
            return false;
        }
        if (end == null) {
            if (other.end != null) {
                return false;
            }
        } else if (!end.equals(other.end)) {
            return false;
        }
        if (start == null) {
            if (other.start != null) {
                return false;
            }
        } else if (!start.equals(other.start)) {
            return false;
        }
        return true;
    }

}
