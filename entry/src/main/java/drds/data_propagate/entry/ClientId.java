package drds.data_propagate.entry;

import drds.common.$;
import drds.data_propagate.common.utils.ToStringStyle;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class ClientId implements Serializable {

    private static final long serialVersionUID = -8262100681930834834L;
    @Setter
    @Getter
    private String destination;
    @Setter
    @Getter
    private short clientId;
    @Setter
    @Getter
    private String filter;

    public ClientId() {

    }

    public ClientId(String destination, short clientId) {
        this.clientId = clientId;
        this.destination = destination;
    }

    public ClientId(String destination, short clientId, String filter) {
        this.clientId = clientId;
        this.destination = destination;
        this.filter = filter;
    }

    public Boolean hasFilter() {
        if (filter == null) {
            return false;
        }
        return $.isNotNullAndNotEmpty(filter);
    }


    public String toString() {
        return ToStringStyle.toString(this);
    }

    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + clientId;
        result = prime * result + ((destination == null) ? 0 : destination.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ClientId)) {
            return false;
        }
        ClientId other = (ClientId) obj;
        if (clientId != other.clientId) {
            return false;
        }
        if (destination == null) {
            if (other.destination != null) {
                return false;
            }
        } else if (!destination.equals(other.destination)) {
            return false;
        }
        return true;
    }

}
