package drds.data_propagate.entry;


import lombok.Getter;
import lombok.Setter;

public class RowData {
    @Setter
    @Getter
    private java.util.List<Column> beforeColumnList;
    @Setter
    @Getter
    private java.util.List<Column> afterColumnList;
    @Setter
    @Getter
    private java.util.List<KeyValuePair> props;


}
