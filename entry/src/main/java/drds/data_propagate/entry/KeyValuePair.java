package drds.data_propagate.entry;


import lombok.Getter;
import lombok.Setter;

public final class KeyValuePair {

    @Setter
    @Getter
    private String key;
    @Setter
    @Getter
    private Object value;

}
