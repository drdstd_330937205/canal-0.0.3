package drds.data_propagate.entry;


import lombok.Getter;
import lombok.Setter;

public class TransactionBegin {

    @Setter
    @Getter
    private long executeTime;
    @Setter
    @Getter
    private Object transactionId;
    @Setter
    @Getter
    private java.util.List<KeyValuePair> props;
    @Setter
    @Getter
    private long threadId;


}
