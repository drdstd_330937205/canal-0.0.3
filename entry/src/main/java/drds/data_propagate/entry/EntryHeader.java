package drds.data_propagate.entry;


import lombok.Getter;
import lombok.Setter;

public final class EntryHeader {

    @Setter
    @Getter
    private int version;
    @Setter
    @Getter
    private String logFileName;
    @Setter
    @Getter
    private long logfileOffset;
    @Setter
    @Getter
    private long serverId;
    @Setter
    @Getter
    private Object serverenCode;
    @Setter
    @Getter
    private long executeTime;
    @Setter
    @Getter
    private Object schemaName;
    @Setter
    @Getter
    private Object tableName;
    @Setter
    @Getter
    private long eventLength;
    @Setter
    @Getter
    private EventType eventType;
    @Setter
    @Getter
    private java.util.List<KeyValuePair> props;
    @Setter
    @Getter
    private String gtid;


}
