package drds.data_propagate.metadata;

import drds.data_propagate.entry.position.PositionRange;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Map;

@Ignore
public class MemoryMetaDataManagerTest extends AbstractMetaDataManagerTest {

    @Test
    public void testSubscribeAll() {
        MemoryMetaDataManager metaManager = new MemoryMetaDataManager();
        metaManager.start();
        doSubscribeTest(metaManager);
        metaManager.stop();
    }

    @Test
    public void testBatchAll() {
        MemoryMetaDataManager metaManager = new MemoryMetaDataManager();
        metaManager.start();
        doBatchTest(metaManager);

        metaManager.clearAllBatchs(clientId);
        Map<Long, PositionRange> ranges = metaManager.listAllBatchs(clientId);
        Assert.assertEquals(0, ranges.size());
        metaManager.stop();
    }

    @Test
    public void testCursorAll() {
        MemoryMetaDataManager metaManager = new MemoryMetaDataManager();
        metaManager.start();
        doCursorTest(metaManager);
        metaManager.stop();
    }
}
