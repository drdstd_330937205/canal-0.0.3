package drds.data_propagate.metadata;

import drds.data_propagate.entry.ClientId;
import drds.data_propagate.entry.position.*;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Ignore
public class AbstractMetaDataManagerTest extends AbstractZkTest {

    private static final String MYSQL_ADDRESS = "127.0.0.1";
    protected ClientId clientId = new ClientId(destination, (short) 1);
    ;

    @Test
    public void doSubscribeTest(MetaDataManager metaDataManager) {
        ClientId client1 = new ClientId(destination, (short) 1);
        metaDataManager.subscribe(client1);
        metaDataManager.subscribe(client1); // 重复调用
        ClientId client2 = new ClientId(destination, (short) 2);
        metaDataManager.subscribe(client2);

        List<ClientId> clients = metaDataManager.listAllSubscribeInfo(destination);
        Assert.assertEquals(Arrays.asList(client1, client2), clients);

        metaDataManager.unsubscribe(client2);
        ClientId client3 = new ClientId(destination, (short) 3);
        metaDataManager.subscribe(client3);

        clients = metaDataManager.listAllSubscribeInfo(destination);
        Assert.assertEquals(Arrays.asList(client1, client3), clients);

    }

    @Test
    public void doBatchTest(MetaDataManager metaDataManager) {
        metaDataManager.subscribe(clientId);

        PositionRange first = metaDataManager.getFirstPositionRange(clientId);
        PositionRange lastest = metaDataManager.getLastestPositionRange(clientId);

        Assert.assertNull(first);
        Assert.assertNull(lastest);

        PositionRange range1 = buildRange(1);
        Long batchId1 = metaDataManager.addPositionRange(clientId, range1);

        PositionRange range2 = buildRange(2);
        Long batchId2 = metaDataManager.addPositionRange(clientId, range2);
        Assert.assertEquals((batchId1.longValue() + 1), batchId2.longValue());

        // 验证get
        PositionRange getRange1 = metaDataManager.getPositionRange(clientId, batchId1);
        Assert.assertEquals(range1, getRange1);
        PositionRange getRange2 = metaDataManager.getPositionRange(clientId, batchId2);
        Assert.assertEquals(range2, getRange2);

        PositionRange range3 = buildRange(3);
        Long batchId3 = batchId2 + 1;
        metaDataManager.addPositionRange(clientId, range3, batchId3);

        PositionRange range4 = buildRange(4);
        Long batchId4 = metaDataManager.addPositionRange(clientId, range4);
        Assert.assertEquals((batchId3.longValue() + 1), batchId4.longValue());

        // 验证remove
        metaDataManager.removePositionRange(clientId, batchId1);
        range1 = metaDataManager.getPositionRange(clientId, batchId1);
        Assert.assertNull(range1);

        // 验证first / lastest
        first = metaDataManager.getFirstPositionRange(clientId);
        lastest = metaDataManager.getLastestPositionRange(clientId);

        Assert.assertEquals(range2, first);
        Assert.assertEquals(range4, lastest);

        Map<Long, PositionRange> ranges = metaDataManager.listAllBatchs(clientId);
        Assert.assertEquals(3, ranges.size());
    }

    public Position doCursorTest(MetaDataManager metaDataManager) {
        metaDataManager.subscribe(clientId);

        Position position1 = metaDataManager.getPosition(clientId);
        Assert.assertNull(position1);

        PositionRange range = buildRange(1);

        metaDataManager.updatePosition(clientId, range.getStart());
        Position position2 = metaDataManager.getPosition(clientId);
        Assert.assertEquals(range.getStart(), position2);

        metaDataManager.updatePosition(clientId, range.getEnd());
        Position position3 = metaDataManager.getPosition(clientId);
        Assert.assertEquals(range.getEnd(), position3);

        return position3;
    }

    private PositionRange<BinLogEventPosition> buildRange(int number) {
        BinLogEventPosition start = new BinLogEventPosition();
        start.setSlaveId(new SlavePosition(new InetSocketAddress(MYSQL_ADDRESS, 3306), 1234L));
        start.setEntryPosition(new EntryPosition("mysql-bin.000000" + number, 106L, new Date().getTime()));

        BinLogEventPosition end = new BinLogEventPosition();
        end.setSlaveId(new SlavePosition(new InetSocketAddress(MYSQL_ADDRESS, 3306), 1234L));
        end.setEntryPosition(
                new EntryPosition("mysql-bin.000000" + (number + 1), 106L, (new Date().getTime()) + 1000 * 1000L));
        return new PositionRange<BinLogEventPosition>(start, end);
    }
}
