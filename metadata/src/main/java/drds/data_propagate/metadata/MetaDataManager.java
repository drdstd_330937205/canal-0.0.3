package drds.data_propagate.metadata;

import drds.data_propagate.common.LifeCycle;
import drds.data_propagate.entry.ClientId;
import drds.data_propagate.entry.position.Position;
import drds.data_propagate.entry.position.PositionRange;
import drds.data_propagate.metadata.exception.DataPropagateMetaManagerException;

import java.util.List;
import java.util.Map;


public interface MetaDataManager extends LifeCycle {

    /**
     * 增加一个 client订阅 <br/>
     * 如果 client已经存在，则不做任何修改
     */
    void subscribe(ClientId clientId) throws DataPropagateMetaManagerException;

    /**
     * 判断是否订阅
     */
    boolean hasSubscribe(ClientId clientId) throws DataPropagateMetaManagerException;

    /**
     * 取消client订阅
     */
    void unsubscribe(ClientId clientId) throws DataPropagateMetaManagerException;
    //

    /**
     * 获取 cursor 游标
     */
    Position getPosition(ClientId clientId) throws DataPropagateMetaManagerException;

    /**
     * 更新 cursor 游标
     */
    void updatePosition(ClientId clientId, Position position) throws DataPropagateMetaManagerException;

    /**
     * 根据指定的destination列出当前所有的clientIdentity信息
     */
    List<ClientId> listAllSubscribeInfo(String destination) throws DataPropagateMetaManagerException;

    /**
     * 获得该client最新的一个位置
     */
    PositionRange getFirstPositionRange(ClientId clientId) throws DataPropagateMetaManagerException;

    /**
     * 获得该clientId最新的一个位置
     */
    PositionRange getLastestPositionRange(ClientId clientId) throws DataPropagateMetaManagerException;

    /**
     * 为 client 产生一个唯一、递增的id
     */
    Long addPositionRange(ClientId clientId, PositionRange positionRange) throws DataPropagateMetaManagerException;

    /**
     * 指定batchId，插入batch数据
     */
    void addPositionRange(ClientId clientId, PositionRange positionRange, Long batchId)
            throws DataPropagateMetaManagerException;

    /**
     * 根据唯一messageId，查找对应的数据起始信息
     */
    PositionRange getPositionRange(ClientId clientId, Long batchId) throws DataPropagateMetaManagerException;

    /**
     * 对一个batch的确认
     */
    PositionRange removePositionRange(ClientId clientId, Long batchId) throws DataPropagateMetaManagerException;

    /**
     * 查询当前的所有batch信息
     */
    Map<Long, PositionRange> listAllBatchs(ClientId clientId) throws DataPropagateMetaManagerException;

    /**
     * 清除对应的batch信息
     */
    void clearAllBatchs(ClientId clientId) throws DataPropagateMetaManagerException;

}
