package drds.data_propagate.metadata.exception;

import drds.data_propagate.common.DataPropagateException;


public class DataPropagateMetaManagerException extends DataPropagateException {

    private static final long serialVersionUID = -654893533794556357L;

    public DataPropagateMetaManagerException(String errorCode) {
        super(errorCode);
    }

    public DataPropagateMetaManagerException(String errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    public DataPropagateMetaManagerException(String errorCode, String errorDesc) {
        super(errorCode + ":" + errorDesc);
    }

    public DataPropagateMetaManagerException(String errorCode, String errorDesc, Throwable cause) {
        super(errorCode + ":" + errorDesc, cause);
    }

    public DataPropagateMetaManagerException(Throwable cause) {
        super(cause);
    }

}
