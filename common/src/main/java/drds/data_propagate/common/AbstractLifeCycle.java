package drds.data_propagate.common;

/**
 * 基本实现
 */
public abstract class AbstractLifeCycle implements LifeCycle {

    protected volatile boolean running = false; // 是否处于运行中

    public void start() {
        if (running) {
            throw new DataPropagateException(this.getClass().getName() + " has startup , don't repeat start");
        }

        running = true;
    }

    public boolean isStart() {
        return running;
    }

    public void stop() {
        if (!running) {
            throw new DataPropagateException(this.getClass().getName() + " isn't start , please check");
        }

        running = false;
    }

}
