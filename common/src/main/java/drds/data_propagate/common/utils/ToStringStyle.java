package drds.data_propagate.common.utils;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <pre>
 * 输出格式：
 * Person[name=John Doe,age=33,smoker=false ,time=2010-04-01 00:00:00]
 * </pre>
 */
public class ToStringStyle extends org.apache.commons.lang.builder.ToStringStyle {
    public static final org.apache.commons.lang.builder.ToStringStyle data_time_style = new ToStringStyle();
    //
    private static final long serialVersionUID = 5208917932254652886L;
    private static final String data_time_style_formate = "yyyy-MM-dd HH:mm:ss";
    ;

    public ToStringStyle() {
        super();
        this.setUseShortClassName(true);
        this.setUseIdentityHashCode(false);

    }

    public static String toString(Object object) {
        return ToStringBuilder.reflectionToString(object, ToStringStyle.data_time_style);
    }

    protected void appendDetail(StringBuffer sb, String fieldName, Object fileValue) {
        // 增加自定义的date对象处理
        if (fileValue instanceof Date) {
            fileValue = new SimpleDateFormat(data_time_style_formate).format(fileValue);
        }
        // 后续可以增加其他自定义对象处理
        sb.append(fileValue);
    }

}
