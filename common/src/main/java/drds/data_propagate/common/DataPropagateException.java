package drds.data_propagate.common;

import org.apache.commons.lang.exception.NestableRuntimeException;

public class DataPropagateException extends NestableRuntimeException {

    private static final long serialVersionUID = -654893533794556357L;

    public DataPropagateException(String errorCode) {
        super(errorCode);
    }

    public DataPropagateException(String errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    public DataPropagateException(String errorCode, String errorDesc) {
        super(errorCode + ":" + errorDesc);
    }

    public DataPropagateException(String errorCode, String errorDesc, Throwable cause) {
        super(errorCode + ":" + errorDesc, cause);
    }

    public DataPropagateException(Throwable cause) {
        super(cause);
    }

    public Throwable fillInStackTrace() {
        return this;
    }

}
