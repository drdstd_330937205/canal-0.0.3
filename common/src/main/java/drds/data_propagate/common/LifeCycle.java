package drds.data_propagate.common;

public interface LifeCycle {

    void start();

    void stop();

    boolean isStart();
}
