package drds.data_propagate.server;

import drds.data_propagate.common.LifeCycle;

/**
 * 对应canal整个服务实例，一个jvm实例只有一份server
 */
public interface Server extends LifeCycle {

}
