package drds.data_propagate.server;

import drds.data_propagate.entry.ClientId;
import drds.data_propagate.entry.Message;

import java.util.concurrent.TimeUnit;

public interface Service {

    void subscribe(ClientId clientId) throws Exception;

    void unsubscribe(ClientId clientId) throws Exception;

    Message get(ClientId clientId, int batchSize) throws Exception;

    Message get(ClientId clientId, int batchSize, Long timeout, TimeUnit timeUnit) throws Exception;

    Message getWithoutAck(ClientId clientId, int batchSize) throws Exception;

    Message getWithoutAck(ClientId clientId, int batchSize, Long timeout, TimeUnit timeUnit)
            throws Exception;

    void ack(ClientId clientId, long batchId) throws Exception;

    void rollback(ClientId clientId) throws Exception;

    void rollback(ClientId clientId, Long batchId) throws Exception;
}
