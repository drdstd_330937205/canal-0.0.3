package drds.data_propagate.server.embedded;

import drds.data_propagate.entry.ClientId;
import drds.data_propagate.entry.Message;
import drds.data_propagate.instance.InstanceGenerator;
import drds.data_propagate.instance.manager.Instance;
import drds.data_propagate.instance.manager.model.Task;
import drds.data_propagate.parse.EventParser;
import drds.data_propagate.parse.HaSwitchable;
import drds.data_propagate.server.ServerImpl;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.util.CollectionUtils;

@Ignore
public abstract class BaseTaskServerImplWithEmbededTest {

    protected static final String cluster1 = "127.0.0.1:2188";
    protected static final String DESTINATION = "example";
    protected static final String DETECTING_SQL = "insert into retl.xdual values(1,now()) on duplicateFromEffectiveInitialIndexOffset primaryKey update x=now()";
    protected static final String MYSQL_ADDRESS = "127.0.0.1";
    protected static final String USERNAME = "canal";
    protected static final String PASSWORD = "canal";
    protected static final String FILTER = ".*\\\\..*";

    private ServerImpl serverImpl;
    private ClientId clientId = new ClientId(DESTINATION, (short) 1);
    ;

    @Before
    public void setUp() {
        serverImpl = ServerImpl.instance();
        serverImpl.setInstanceGenerator(new InstanceGenerator() {

            public drds.data_propagate.instance.Instance generate(String destination) {
                Task task = buildTask();
                return new Instance(task, FILTER);
            }
        });
        serverImpl.start();
        serverImpl.start(DESTINATION);
    }

    @After
    public void tearDown() {
        serverImpl.stop();
    }

    @Test
    public void testGetWithoutAck() throws Exception {
        int maxEmptyCount = 10;
        int emptyCount = 0;
        int totalCount = 0;
        serverImpl.subscribe(clientId);
        while (emptyCount < maxEmptyCount) {
            Message message = serverImpl.getWithoutAck(clientId, 11);
            if (CollectionUtils.isEmpty(message.getEntryList())) {
                emptyCount++;
                try {
                    Thread.sleep(emptyCount * 300L);
                } catch (InterruptedException e) {
                    Assert.fail();
                }

                System.out.println("empty count : " + emptyCount);
            } else {
                emptyCount = 0;
                totalCount += message.getEntryList().size();
                serverImpl.ack(clientId, message.getId());
            }
        }

        System.out.println("!!!!!! testGetWithoutAck totalCount : " + totalCount);
        serverImpl.unsubscribe(clientId);
    }

    @Test
    public void testGet() throws Exception {
        int maxEmptyCount = 10;
        int emptyCount = 0;
        int totalCount = 0;
        serverImpl.subscribe(clientId);
        while (emptyCount < maxEmptyCount) {
            Message message = serverImpl.get(clientId, 11);
            if (CollectionUtils.isEmpty(message.getEntryList())) {
                emptyCount++;
                try {
                    Thread.sleep(emptyCount * 300L);
                } catch (InterruptedException e) {
                    Assert.fail();
                }

                System.out.println("empty count : " + emptyCount);
            } else {
                emptyCount = 0;
                totalCount += message.getEntryList().size();
            }
        }

        System.out.println("!!!!!! testGet totalCount : " + totalCount);
        serverImpl.unsubscribe(clientId);
    }

    // @Test
    public void testRollback() throws Exception {
        int maxEmptyCount = 10;
        int emptyCount = 0;
        int totalCount = 0;
        serverImpl.subscribe(clientId);
        while (emptyCount < maxEmptyCount) {
            Message message = serverImpl.getWithoutAck(clientId, 11);
            if (CollectionUtils.isEmpty(message.getEntryList())) {
                emptyCount++;
                try {
                    Thread.sleep(emptyCount * 300L);
                } catch (InterruptedException e) {
                    Assert.fail();
                }

                System.out.println("empty count : " + emptyCount);
            } else {
                emptyCount = 0;
                totalCount += message.getEntryList().size();
            }
        }
        System.out.println("!!!!!! testRollback totalCount : " + totalCount);

        serverImpl.rollback(clientId);// 直接rollback掉，再取一次
        emptyCount = 0;
        totalCount = 0;
        while (emptyCount < maxEmptyCount) {
            Message message = serverImpl.getWithoutAck(clientId, 11);
            if (CollectionUtils.isEmpty(message.getEntryList())) {
                emptyCount++;
                try {
                    Thread.sleep(emptyCount * 300L);
                } catch (InterruptedException e) {
                    Assert.fail();
                }

                System.out.println("empty count : " + emptyCount);
            } else {
                emptyCount = 0;
                totalCount += message.getEntryList().size();
            }
        }

        System.out.println("!!!!!! testRollback after rollback ,  totalCount : " + totalCount);
        serverImpl.unsubscribe(clientId);
    }

    // @Test
    public void testSwitch() throws Exception {
        int maxEmptyCount = 10;
        int emptyCount = 0;
        int totalCount = 0;

        int thresold = 50;
        int batchSize = 11;
        serverImpl.subscribe(clientId);
        while (emptyCount < maxEmptyCount) {
            Message message = serverImpl.get(clientId, batchSize);
            if (CollectionUtils.isEmpty(message.getEntryList())) {
                emptyCount++;
                try {
                    Thread.sleep(emptyCount * 300L);
                } catch (InterruptedException e) {
                    Assert.fail();
                }

                System.out.println("empty count : " + emptyCount);
            } else {
                emptyCount = 0;
                totalCount += message.getEntryList().size();

                if ((totalCount + 1) % 100 >= thresold && (totalCount + 1) % 100 <= thresold + batchSize) {
                    EventParser eventParser = serverImpl.getDestinationToInstanceMap().get(DESTINATION).getEventParser();
                    if (eventParser instanceof HaSwitchable) {
                        ((HaSwitchable) eventParser).doSwitch();// 执行切换
                        try {
                            Thread.sleep(5 * 1000); // 等待parser启动
                        } catch (InterruptedException e) {
                            Assert.fail();
                        }
                    }
                }
            }
        }

        System.out.println("!!!!!! testGet totalCount : " + totalCount);
        serverImpl.unsubscribe(clientId);
    }

    abstract protected Task buildTask();
}
