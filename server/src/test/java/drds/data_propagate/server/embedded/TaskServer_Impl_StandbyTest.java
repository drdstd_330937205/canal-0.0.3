package drds.data_propagate.server.embedded;

import drds.data_propagate.common.zookeeper.ZookeeperPathUtils;
import drds.data_propagate.instance.manager.model.IndexMode;
import drds.data_propagate.instance.manager.model.Parameter;
import drds.data_propagate.instance.manager.model.SourcingType;
import drds.data_propagate.instance.manager.model.Task;
import org.I0Itec.zkclient.ZkClient;
import org.junit.Before;
import org.junit.Ignore;

import java.net.InetSocketAddress;
import java.util.Arrays;

@Ignore
public class TaskServer_Impl_StandbyTest extends BaseTaskServerImplWithEmbededTest {

    private ZkClient zkClient = new ZkClient(cluster1);

    @Before
    public void setUp() {
        zkClient.deleteRecursive(ZookeeperPathUtils.CANAL_ROOT_NODE);
        super.setUp();
    }

    protected Task buildTask() {
        Task task = new Task();
        task.setId(1L);
        task.setName(DESTINATION);
        task.setDesc("test");

        Parameter parameter = new Parameter();

        parameter.setZkClusters(Arrays.asList("127.0.0.1:2188"));

        parameter.setIndexMode(IndexMode.META);// 内存版store，需要选择meta做为index


        parameter.setMemoryStorageBufferSize(32 * 1024);

        parameter.setSourcingType(SourcingType.MYSQL);
        parameter.setDbAddresses(
                Arrays.asList(new InetSocketAddress(MYSQL_ADDRESS, 3306), new InetSocketAddress(MYSQL_ADDRESS, 3306)));
        parameter.setDbUsername(USERNAME);
        parameter.setDbPassword(PASSWORD);
        parameter.setPositions(Arrays.asList(
                "{\"journalName\":\"mysql-bin.000001\",\"readedIndex\":6163L,\"timestamp\":1322803601000L}",
                "{\"journalName\":\"mysql-bin.000001\",\"readedIndex\":6163L,\"timestamp\":1322803601000L}"));

        parameter.setSlaveId(1234L);

        parameter.setDefaultConnectionTimeoutInSeconds(30);
        parameter.setConnectionCharset("UTF-8");
        parameter.setConnectionCharsetNumber((byte) 33);
        parameter.setReceiveBufferSize(8 * 1024);
        parameter.setSendBufferSize(8 * 1024);

        parameter.setDetectingEnable(false);
        parameter.setDetectingIntervalInSeconds(10);
        parameter.setDetectingRetryTimes(3);
        parameter.setDetectingSQL(DETECTING_SQL);

        task.setParameter(parameter);
        return task;
    }
}
