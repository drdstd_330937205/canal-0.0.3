package drds.data_propagate.server.embedded;

import drds.data_propagate.instance.manager.model.IndexMode;
import drds.data_propagate.instance.manager.model.Parameter;
import drds.data_propagate.instance.manager.model.SourcingType;
import drds.data_propagate.instance.manager.model.Task;
import org.junit.Ignore;

import java.net.InetSocketAddress;
import java.util.Arrays;

@Ignore
public class TaskServer_Impl_FileModeTest extends BaseTaskServerImplWithEmbededTest {

    protected Task buildTask() {
        Task task = new Task();
        task.setId(1L);
        task.setName(DESTINATION);
        task.setDesc("my standalone server test ");

        Parameter parameter = new Parameter();

        //parameter.setMetaMode(MetaMode.LOCAL_FILE);
        parameter.setDataDir("./conf");
        parameter.setMetaFileFlushPeriod(1000);

        parameter.setIndexMode(IndexMode.MEMORY_META_FAILBACK);


        parameter.setMemoryStorageBufferSize(32 * 1024);

        parameter.setSourcingType(SourcingType.MYSQL);
        parameter.setDbAddresses(
                Arrays.asList(new InetSocketAddress(MYSQL_ADDRESS, 3306), new InetSocketAddress(MYSQL_ADDRESS, 3306)));
        parameter.setDbUsername(USERNAME);
        parameter.setDbPassword(PASSWORD);
        parameter.setPositions(Arrays.asList(
                "{\"journalName\":\"mysql-bin.000001\",\"readedIndex\":332L,\"timestamp\":\"1505998863000\"}",
                "{\"journalName\":\"mysql-bin.000001\",\"readedIndex\":332L,\"timestamp\":\"1505998863000\"}"));

        parameter.setSlaveId(1234L);

        parameter.setDefaultConnectionTimeoutInSeconds(30);
        parameter.setConnectionCharset("UTF-8");
        parameter.setConnectionCharsetNumber((byte) 33);
        parameter.setReceiveBufferSize(8 * 1024);
        parameter.setSendBufferSize(8 * 1024);

        parameter.setDetectingEnable(false);
        parameter.setDetectingIntervalInSeconds(10);
        parameter.setDetectingRetryTimes(3);
        parameter.setDetectingSQL(DETECTING_SQL);

        task.setParameter(parameter);
        return task;
    }
}
