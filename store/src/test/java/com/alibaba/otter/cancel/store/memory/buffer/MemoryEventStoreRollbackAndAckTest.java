package com.alibaba.otter.cancel.store.memory.buffer;

import drds.data_propagate.entry.position.Position;
import drds.data_propagate.store.Event;
import drds.data_propagate.store.EventUtils;
import drds.data_propagate.store.Events;
import drds.data_propagate.store.MemoryEventStore;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试下rollback / ack的操作
 *
 * @author jianghang 2012-6-19 下午09:49:28
 * @version 1.0.0
 */
public class MemoryEventStoreRollbackAndAckTest extends MemoryEventStoreBase {

    @Test
    public void testRollback() {
        int bufferSize = 16;
        MemoryEventStore eventStore = new MemoryEventStore();
        eventStore.setBufferSize(bufferSize);
        eventStore.start();

        for (int i = 0; i < bufferSize / 2; i++) {
            boolean result = eventStore.tryPutOneTime(buildEvent("1", 1L, 1L + i));
            sleep(100L);
            Assert.assertTrue(result);
        }

        sleep(50L);
        Position first = eventStore.getFirstBinLogEventPosition();
        Position lastest = eventStore.getLatestBinLogEventPosition();
        Assert.assertEquals(first, EventUtils.createPosition(buildEvent("1", 1L, 1L)));
        Assert.assertEquals(lastest, EventUtils.createPosition(buildEvent("1", 1L, 1L + bufferSize / 2 - 1)));

        System.out.println("start get");
        Events<Event> entrys1 = eventStore.tryGet(first, bufferSize);
        System.out.println("first get size : " + entrys1.getEventList().size());

        eventStore.rollback();

        entrys1 = eventStore.tryGet(first, bufferSize);
        System.out.println("after rollback get size : " + entrys1.getEventList().size());
        Assert.assertTrue(entrys1.getEventList().size() == bufferSize / 2);

        // 继续造数据
        for (int i = bufferSize / 2; i < bufferSize; i++) {
            boolean result = eventStore.tryPutOneTime(buildEvent("1", 1L, 1L + i));
            sleep(100L);
            Assert.assertTrue(result);
        }

        Events<Event> entrys2 = eventStore.tryGet(entrys1.getPositionRange().getEnd(), bufferSize);
        System.out.println("second get size : " + entrys2.getEventList().size());

        eventStore.rollback();

        entrys2 = eventStore.tryGet(entrys1.getPositionRange().getEnd(), bufferSize);
        System.out.println("after rollback get size : " + entrys2.getEventList().size());
        Assert.assertTrue(entrys2.getEventList().size() == bufferSize);

        first = eventStore.getFirstBinLogEventPosition();
        lastest = eventStore.getLatestBinLogEventPosition();
        List<Event> entrys = new ArrayList<Event>(entrys2.getEventList());
        Assert.assertTrue(entrys.size() == bufferSize);
        Assert.assertEquals(first, entrys2.getPositionRange().getStart());
        Assert.assertEquals(lastest, entrys2.getPositionRange().getEnd());

        Assert.assertEquals(first, EventUtils.createPosition(entrys.get(0)));
        Assert.assertEquals(lastest, EventUtils.createPosition(entrys.get(bufferSize - 1)));
        eventStore.stop();
    }

    @Test
    public void testAck() {
        int bufferSize = 16;
        MemoryEventStore eventStore = new MemoryEventStore();
        eventStore.setBufferSize(bufferSize);
        eventStore.start();

        for (int i = 0; i < bufferSize / 2; i++) {
            boolean result = eventStore.tryPutOneTime(buildEvent("1", 1L, 1L + i));
            sleep(100L);
            Assert.assertTrue(result);
        }

        sleep(50L);
        Position first = eventStore.getFirstBinLogEventPosition();
        Position lastest = eventStore.getLatestBinLogEventPosition();
        Assert.assertEquals(first, EventUtils.createPosition(buildEvent("1", 1L, 1L)));
        Assert.assertEquals(lastest, EventUtils.createPosition(buildEvent("1", 1L, 1L + bufferSize / 2 - 1)));

        System.out.println("start get");
        Events<Event> entrys1 = eventStore.tryGet(first, bufferSize);
        System.out.println("first get size : " + entrys1.getEventList().size());

        eventStore.cleanUntil(entrys1.getPositionRange().getEnd());
        sleep(50L);

        // 继续造数据
        for (int i = bufferSize / 2; i < bufferSize; i++) {
            boolean result = eventStore.tryPutOneTime(buildEvent("1", 1L, 1L + i));
            sleep(100L);
            Assert.assertTrue(result);
        }

        Events<Event> entrys2 = eventStore.tryGet(entrys1.getPositionRange().getEnd(), bufferSize);
        System.out.println("second get size : " + entrys2.getEventList().size());

        eventStore.rollback();

        entrys2 = eventStore.tryGet(entrys1.getPositionRange().getEnd(), bufferSize);
        System.out.println("after rollback get size : " + entrys2.getEventList().size());

        first = eventStore.getFirstBinLogEventPosition();
        lastest = eventStore.getLatestBinLogEventPosition();
        List<Event> entrys = new ArrayList<Event>(entrys2.getEventList());
        Assert.assertEquals(first, entrys2.getPositionRange().getStart());
        Assert.assertEquals(lastest, entrys2.getPositionRange().getEnd());

        Assert.assertEquals(first, EventUtils.createPosition(entrys.get(0)));
        Assert.assertEquals(lastest, EventUtils.createPosition(entrys.get(entrys.size() - 1)));

        // 全部ack掉
        eventStore.cleanUntil(entrys2.getPositionRange().getEnd());

        // 最后就拿不到数据
        Events<Event> entrys3 = eventStore.tryGet(entrys1.getPositionRange().getEnd(), bufferSize);
        System.out.println("third get size : " + entrys3.getEventList().size());
        Assert.assertEquals(0, entrys3.getEventList().size());

        eventStore.stop();
    }

}
