package com.alibaba.otter.cancel.store.memory.buffer;

import drds.data_propagate.entry.Entry;
import drds.data_propagate.entry.EntryHeader;
import drds.data_propagate.entry.position.SlavePosition;
import drds.data_propagate.store.Event;
import org.junit.Assert;

import java.net.InetSocketAddress;

public class MemoryEventStoreBase {

    private static final String MYSQL_ADDRESS = "127.0.0.1";

    protected void sleep(Long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            Assert.fail();
        }
    }

    protected Event buildEvent(String binlogFile, long offset, long timestamp) {
        EntryHeader entryHeaderBuilder = new EntryHeader();
        entryHeaderBuilder.setLogFileName(binlogFile);
        entryHeaderBuilder.setLogfileOffset(offset);
        entryHeaderBuilder.setExecuteTime(timestamp);
        entryHeaderBuilder.setEventLength(1024);
        Entry entryBuilder = new Entry();
        entryBuilder.setEntryHeader(entryHeaderBuilder);
        Entry entry = entryBuilder;

        return new Event(new SlavePosition(new InetSocketAddress(MYSQL_ADDRESS, 3306), 1234L), entry);
    }

    protected Event buildEvent(String binlogFile, long offset, long timestamp, long eventLenght) {
        EntryHeader entryHeaderBuilder = new EntryHeader();
        entryHeaderBuilder.setLogFileName(binlogFile);
        entryHeaderBuilder.setLogfileOffset(offset);
        entryHeaderBuilder.setExecuteTime(timestamp);
        entryHeaderBuilder.setEventLength(eventLenght);
        Entry entryBuilder = new Entry();
        entryBuilder.setEntryHeader(entryHeaderBuilder);
        Entry entry = entryBuilder;

        return new Event(new SlavePosition(new InetSocketAddress(MYSQL_ADDRESS, 3306), 1234L), entry);
    }
}
