package drds.data_propagate.store;

import drds.data_propagate.common.DataPropagateException;


public class StoreException extends DataPropagateException {

    private static final long serialVersionUID = -7288830284122672209L;

    public StoreException(String errorCode) {
        super(errorCode);
    }

    public StoreException(String errorCode, Throwable cause) {
        super(errorCode, cause);
    }

    public StoreException(String errorCode, String errorDesc) {
        super(errorCode + ":" + errorDesc);
    }

    public StoreException(String errorCode, String errorDesc, Throwable cause) {
        super(errorCode + ":" + errorDesc, cause);
    }

    public StoreException(Throwable cause) {
        super(cause);
    }

}
