package drds.data_propagate.store;

import drds.data_propagate.entry.position.PositionRange;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 代表一组数据对象的集合
 */
public class Events<Event> implements Serializable {

    private static final long serialVersionUID = -7337454954300706044L;

    @Setter
    @Getter
    private PositionRange positionRange = new PositionRange();
    @Setter
    @Getter
    private List<Event> eventList = new ArrayList<Event>();
}
