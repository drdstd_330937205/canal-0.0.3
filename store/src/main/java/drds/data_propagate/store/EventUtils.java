package drds.data_propagate.store;

import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.entry.position.EntryPosition;
import org.apache.commons.lang.StringUtils;


public class EventUtils {

    /**
     * 找出一个最小的position位置，相等的情况返回position1
     */
    public static BinLogEventPosition min(BinLogEventPosition position1, BinLogEventPosition position2) {
        if (position1.getSlaveId().equals(position2.getSlaveId())) {
            // 首先根据文件进行比较
            if (position1.getEntryPosition().getJournalName().compareTo(position2.getEntryPosition().getJournalName()) > 0) {
                return position2;
            } else if (position1.getEntryPosition().getJournalName().compareTo(position2.getEntryPosition().getJournalName()) < 0) {
                return position1;
            } else {
                // 根据offest进行比较
                if (position1.getEntryPosition().getPosition() > position2.getEntryPosition().getPosition()) {
                    return position2;
                } else {
                    return position1;
                }
            }
        } else {
            // 不同的主备库，根据时间进行比较
            if (position1.getEntryPosition().getTimestamp() > position2.getEntryPosition().getTimestamp()) {
                return position2;
            } else {
                return position1;
            }
        }
    }

    /**
     * 根据entry创建对应的Position对象
     */
    public static BinLogEventPosition createPosition(Event event) {
        EntryPosition entryPosition = new EntryPosition();
        entryPosition.setJournalName(event.getJournalName());
        entryPosition.setPosition(event.getPosition());
        entryPosition.setTimestamp(event.getExecuteTime());
        // add serverId at 2016-06-28
        entryPosition.setServerId(event.getServerId());
        // add gtid
        entryPosition.setGtid(event.getGtid());

        BinLogEventPosition binLogEventPosition = new BinLogEventPosition();
        binLogEventPosition.setEntryPosition(entryPosition);
        binLogEventPosition.setSlaveId(event.getSlaveId());
        return binLogEventPosition;
    }

    /**
     * 根据entry创建对应的Position对象
     */
    public static BinLogEventPosition createPosition(Event event, boolean included) {
        EntryPosition entryPosition = new EntryPosition();
        entryPosition.setJournalName(event.getJournalName());
        entryPosition.setPosition(event.getPosition());
        entryPosition.setTimestamp(event.getExecuteTime());
        entryPosition.setIncluded(included);

        BinLogEventPosition binLogEventPosition = new BinLogEventPosition();
        binLogEventPosition.setEntryPosition(entryPosition);
        binLogEventPosition.setSlaveId(event.getSlaveId());
        return binLogEventPosition;
    }

    /**
     * 判断当前的entry和position是否相同
     */
    public static boolean checkPosition(Event event, BinLogEventPosition binLogEventPosition) {
        EntryPosition entryPosition = binLogEventPosition.getEntryPosition();
        boolean result = entryPosition.getTimestamp().equals(event.getExecuteTime());

        boolean exactely = (StringUtils.isBlank(entryPosition.getJournalName()) && entryPosition.getPosition() == null);
        if (!exactely) {// 精确匹配
            result &= entryPosition.getPosition().equals(event.getPosition());
            if (result) {// short path
                result &= StringUtils.equals(event.getJournalName(), entryPosition.getJournalName());
            }
        }

        return result;
    }
}
