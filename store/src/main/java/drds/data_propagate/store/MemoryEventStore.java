package drds.data_propagate.store;

import drds.data_propagate.entry.EntryType;
import drds.data_propagate.entry.EventType;
import drds.data_propagate.entry.position.BinLogEventPosition;
import drds.data_propagate.entry.position.Position;
import drds.data_propagate.entry.position.PositionRange;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 基于内存buffer构建内存memory store
 *
 * <pre>
 * 变更记录：
 * 1. 新增BatchMode类型，支持按内存大小获取批次数据，内存大小更加可控.
 *   a. put操作，会首先根据bufferSize进行控制，然后再进行bufferSize * bufferMemUnit进行控制. 因存储的内容是以Event，如果纯依赖于memsize进行控制，会导致RingBuffer出现动态伸缩
 * </pre>
 */
public class MemoryEventStore extends AbstractStoreScavenge implements EventStore<Event>, StoreScavenge {

    private static final long INIT_SEQUENCE = -1;
    private int bufferSize = 16 * 1024;
    private int bufferMemUnit = 1024; // memsize的单位，默认为1kb大小
    private int indexMask;
    private Event[] events;

    // 记录下put/get/ack操作的三个下标
    private AtomicLong putIndex = new AtomicLong(INIT_SEQUENCE); // 代表当前put操作最后一次写操作发生的位置
    private AtomicLong getIndex = new AtomicLong(INIT_SEQUENCE); // 代表当前get操作读取的最后一条的位置
    private AtomicLong ackIndex = new AtomicLong(INIT_SEQUENCE); // 代表当前ack操作的最后一条的位置

    // 记录下put/get/ack操作的三个memsize大小
    private AtomicLong putMemorySize = new AtomicLong(0);
    private AtomicLong getMemorySize = new AtomicLong(0);
    private AtomicLong ackMemorySize = new AtomicLong(0);

    // 记录下put/get/ack操作的三个execTime
    private AtomicLong putExecuteTime = new AtomicLong(System.currentTimeMillis());
    private AtomicLong getExecuteTime = new AtomicLong(System.currentTimeMillis());
    private AtomicLong ackExecuteTime = new AtomicLong(System.currentTimeMillis());

    // 记录下put/get/ack操作的三个table rows
    private AtomicLong putTableRowCount = new AtomicLong(0);
    private AtomicLong getTableRowCount = new AtomicLong(0);
    private AtomicLong ackTableRowCount = new AtomicLong(0);

    // 阻塞put/get操作控制信号
    private ReentrantLock lock = new ReentrantLock();
    private Condition notEmpty = lock.newCondition();
    private Condition notFull = lock.newCondition();

    private BatchMode batchMode = BatchMode.item_size; // 默认为内存大小模式
    private boolean ddlIsolation = false;
    private boolean raw = true; // 针对entry是否开启raw模式

    public MemoryEventStore() {

    }

    public MemoryEventStore(BatchMode batchMode) {
        this.batchMode = batchMode;
    }

    public void start() throws StoreException {
        super.start();
        if (Integer.bitCount(bufferSize) != 1) {
            throw new IllegalArgumentException("bufferSize must be a power of 2");
        }

        indexMask = bufferSize - 1;
        events = new Event[bufferSize];
    }

    public void stop() throws StoreException {
        super.stop();

        cleanAll();
    }

    public void put(List<Event> eventList) throws InterruptedException, StoreException {
        if (eventList == null || eventList.isEmpty()) {
            return;
        }

        final ReentrantLock lock = this.lock;
        lock.lockInterruptibly();
        try {
            try {
                while (!checkFreeSlotAt(putIndex.get() + eventList.size())) { // 检查是否有空位
                    notFull.await(); // wait until not full
                }
            } catch (InterruptedException ie) {
                notFull.signal(); // propagate to non-interrupted thread
                throw ie;
            }
            doPut(eventList);
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
        } finally {
            lock.unlock();
        }
    }

    public boolean put(List<Event> eventList, long timeout, TimeUnit timeUnit)
            throws InterruptedException, StoreException {
        if (eventList == null || eventList.isEmpty()) {
            return true;
        }

        long nanos = timeUnit.toNanos(timeout);
        final ReentrantLock lock = this.lock;
        lock.lockInterruptibly();
        try {
            for (; ; ) {
                if (checkFreeSlotAt(putIndex.get() + eventList.size())) {
                    doPut(eventList);
                    return true;
                }
                if (nanos <= 0) {
                    return false;
                }

                try {
                    nanos = notFull.awaitNanos(nanos);
                } catch (InterruptedException ie) {
                    notFull.signal(); // propagate to non-interrupted thread
                    throw ie;
                }
            }
        } finally {
            lock.unlock();
        }
    }

    public boolean tryPutOneTime(List<Event> eventList) throws StoreException {
        if (eventList == null || eventList.isEmpty()) {
            return true;
        }

        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            if (!checkFreeSlotAt(putIndex.get() + eventList.size())) {
                return false;
            } else {
                doPut(eventList);
                return true;
            }
        } finally {
            lock.unlock();
        }
    }

    public void put(Event event) throws InterruptedException, StoreException {
        put(Arrays.asList(event));
    }

    public boolean put(Event event, long timeout, TimeUnit timeUnit) throws InterruptedException, StoreException {
        return put(Arrays.asList(event), timeout, timeUnit);
    }

    public boolean tryPutOneTime(Event event) throws StoreException {
        return tryPutOneTime(Arrays.asList(event));
    }

    /**
     * 执行具体的put操作
     */
    private void doPut(List<Event> eventList) {
        long putIndex = this.putIndex.get();
        long end = putIndex + eventList.size();

        // 先写数据，再更新对应的cursor,并发度高的情况，putSequence会被get请求可见，拿出了ringbuffer中的老的Entry值
        for (long index = putIndex + 1; index <= end; index++) {
            events[getIndex(index)] = eventList.get((int) (index - putIndex - 1));
        }

        this.putIndex.set(end);

        // 记录一下gets memsize信息，方便快速检索
        if (batchMode.isMemorySize()) {
            long size = 0;
            for (Event event : eventList) {
                size += calculateSize(event);
            }
            putMemorySize.getAndAdd(size);
        }
        profiling(eventList, OP.PUT);
        // tell other threads that store is not empty
        notEmpty.signal();
    }

    public Events<Event> get(Position position, int batchSize) throws InterruptedException, StoreException {
        final ReentrantLock lock = this.lock;
        lock.lockInterruptibly();
        try {
            try {
                while (!checkUnGetSlotAt((BinLogEventPosition) position, batchSize))
                    notEmpty.await();
            } catch (InterruptedException ie) {
                notEmpty.signal(); // propagate to non-interrupted thread
                throw ie;
            }

            return doGet(position, batchSize);
        } finally {
            lock.unlock();
        }
    }

    public Events<Event> get(Position position, int batchSize, long timeout, TimeUnit timeUnit)
            throws InterruptedException, StoreException {
        long nanos = timeUnit.toNanos(timeout);
        final ReentrantLock lock = this.lock;
        lock.lockInterruptibly();
        try {
            for (; ; ) {
                if (checkUnGetSlotAt((BinLogEventPosition) position, batchSize)) {
                    return doGet(position, batchSize);
                }

                if (nanos <= 0) {
                    // 如果时间到了，有多少取多少
                    return doGet(position, batchSize);
                }

                try {
                    nanos = notEmpty.awaitNanos(nanos);
                } catch (InterruptedException ie) {
                    notEmpty.signal(); // propagate to non-interrupted thread
                    throw ie;
                }

            }
        } finally {
            lock.unlock();
        }
    }

    public Events<Event> tryGet(Position position, int batchSize) throws StoreException {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            return doGet(position, batchSize);
        } finally {
            lock.unlock();
        }
    }

    private Events<Event> doGet(Position position, int batchSize) throws StoreException {
        BinLogEventPosition binLogEventPosition = (BinLogEventPosition) position;

        long getIndex = this.getIndex.get();
        long putIndex = this.putIndex.get();
        long index = getIndex;
        long end = getIndex;
        // 如果startPosition为null，说明是第一次，默认+1处理
        if (binLogEventPosition == null || !binLogEventPosition.getEntryPosition().isIncluded()) { // 第一次订阅之后，需要包含一下start位置，防止丢失第一条记录
            index = index + 1;
        }

        if (getIndex >= putIndex) {
            return new Events<Event>();
        }

        Events<Event> events = new Events<Event>();
        List<Event> eventList = events.getEventList();
        long memsize = 0;
        if (batchMode.isItemSize()) {
            end = (index + batchSize - 1) < putIndex ? (index + batchSize - 1) : putIndex;
            // 提取数据并返回
            for (; index <= end; index++) {
                Event event = this.events[getIndex(index)];
                if (ddlIsolation && isDdl(event.getEventType())) {
                    // 如果是ddl隔离，直接返回
                    if (eventList.size() == 0) {
                        eventList.add(event);// 如果没有DML事件，加入当前的DDL事件
                        end = index; // 更新end为当前
                    } else {
                        // 如果之前已经有DML事件，直接返回了，因为不包含当前next这记录，需要回退一个位置
                        end = index - 1; // next-1一定大于current，不需要判断
                    }
                    break;
                } else {
                    eventList.add(event);
                }
            }
        } else {
            long maxMemSize = batchSize * bufferMemUnit;
            for (; memsize <= maxMemSize && index <= putIndex; index++) {
                // 永远保证可以取出第一条的记录，避免死锁
                Event event = this.events[getIndex(index)];
                if (ddlIsolation && isDdl(event.getEventType())) {
                    // 如果是ddl隔离，直接返回
                    if (eventList.size() == 0) {
                        eventList.add(event);// 如果没有DML事件，加入当前的DDL事件
                        end = index; // 更新end为当前
                    } else {
                        // 如果之前已经有DML事件，直接返回了，因为不包含当前next这记录，需要回退一个位置
                        end = index - 1; // next-1一定大于current，不需要判断
                    }
                    break;
                } else {
                    eventList.add(event);
                    memsize += calculateSize(event);
                    end = index;// 记录end位点
                }
            }

        }

        PositionRange<BinLogEventPosition> positionRange = new PositionRange<BinLogEventPosition>();
        events.setPositionRange(positionRange);

        positionRange.setStart(EventUtils.createPosition(eventList.get(0)));
        positionRange.setEnd(EventUtils.createPosition(eventList.get(events.getEventList().size() - 1)));
        // 记录一下是否存在可以被ack的点

        for (int i = eventList.size() - 1; i >= 0; i--) {
            Event event = eventList.get(i);
            // GTID模式,ack的位点必须是事务结尾,因为下一次订阅的时候mysql会发送这个gtid之后的next,如果在事务头就记录了会丢这最后一个事务
            if ((EntryType.transaction_begin == event.getEntryType() && StringUtils.isEmpty(event.getGtid()))
                    || EntryType.transaction_end == event.getEntryType() || isDdl(event.getEventType())) {
                // 将事务头/尾设置可被为ack的点
                positionRange.setAck(EventUtils.createPosition(event));
                break;
            }
        }

        if (this.getIndex.compareAndSet(getIndex, end)) {
            getMemorySize.addAndGet(memsize);
            notFull.signal();
            profiling(events.getEventList(), OP.GET);
            return events;
        } else {
            return new Events<Event>();
        }
    }

    public BinLogEventPosition getFirstBinLogEventPosition() throws StoreException {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            long firstSeqeuence = ackIndex.get();
            if (firstSeqeuence == INIT_SEQUENCE && firstSeqeuence < putIndex.get()) {
                // 没有ack过数据
                Event event = events[getIndex(firstSeqeuence + 1)]; // 最后一次ack为-1，需要移动到下一条,included
                // = false
                return EventUtils.createPosition(event, false);
            } else if (firstSeqeuence > INIT_SEQUENCE && firstSeqeuence < putIndex.get()) {
                // ack未追上put操作
                Event event = events[getIndex(firstSeqeuence + 1)]; // 最后一次ack的位置数据
                // + 1
                return EventUtils.createPosition(event, true);
            } else if (firstSeqeuence > INIT_SEQUENCE && firstSeqeuence == putIndex.get()) {
                // 已经追上，store中没有数据
                Event event = events[getIndex(firstSeqeuence)]; // 最后一次ack的位置数据，和last为同一条，included
                // = false
                return EventUtils.createPosition(event, false);
            } else {
                // 没有任何数据
                return null;
            }
        } finally {
            lock.unlock();
        }
    }

    public BinLogEventPosition getLatestBinLogEventPosition() throws StoreException {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            long latestSequence = putIndex.get();
            if (latestSequence > INIT_SEQUENCE && latestSequence != ackIndex.get()) {
                Event event = events[(int) putIndex.get() & indexMask]; // 最后一次写入的数据，最后一条未消费的数据
                return EventUtils.createPosition(event, true);
            } else if (latestSequence > INIT_SEQUENCE && latestSequence == ackIndex.get()) {
                // ack已经追上了put操作
                Event event = events[(int) putIndex.get() & indexMask]; // 最后一次写入的数据，included
                // =
                // false
                return EventUtils.createPosition(event, false);
            } else {
                // 没有任何数据
                return null;
            }
        } finally {
            lock.unlock();
        }
    }

    public void ack(Position position) throws StoreException {
        cleanUntil(position);
    }

    public void cleanUntil(Position position) throws StoreException {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            long sequence = ackIndex.get();
            long maxSequence = getIndex.get();

            boolean hasMatch = false;
            long memsize = 0;
            // ack没有list，但有已存在的foreach，还是节省一下list的开销
            long localExecTime = 0L;
            int deltaRows = 0;
            for (long next = sequence + 1; next <= maxSequence; next++) {
                Event event = events[getIndex(next)];
                if (localExecTime == 0 && event.getExecuteTime() > 0) {
                    localExecTime = event.getExecuteTime();
                }
                deltaRows += event.getRowsCount();
                memsize += calculateSize(event);
                boolean match = EventUtils.checkPosition(event, (BinLogEventPosition) position);
                if (match) {// 找到对应的position，更新ack seq
                    hasMatch = true;

                    if (batchMode.isMemorySize()) {
                        ackMemorySize.addAndGet(memsize);
                        // 尝试清空buffer中的内存，将ack之前的内存全部释放掉
                        for (long index = sequence + 1; index < next; index++) {
                            events[getIndex(index)] = null;// 设置为null
                        }
                    }

                    if (ackIndex.compareAndSet(sequence, next)) {// 避免并发ack
                        notFull.signal();
                        ackTableRowCount.addAndGet(deltaRows);
                        if (localExecTime > 0) {
                            ackExecuteTime.lazySet(localExecTime);
                        }
                        return;
                    }
                }
            }
            if (!hasMatch) {// 找不到对应需要ack的position
                throw new StoreException("no match ack position" + position.toString());
            }
        } finally {
            lock.unlock();
        }
    }

    public void rollback() throws StoreException {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            getIndex.set(ackIndex.get());
            getMemorySize.set(ackMemorySize.get());
        } finally {
            lock.unlock();
        }
    }

    public void cleanAll() throws StoreException {
        final ReentrantLock lock = this.lock;
        lock.lock();
        try {
            putIndex.set(INIT_SEQUENCE);
            getIndex.set(INIT_SEQUENCE);
            ackIndex.set(INIT_SEQUENCE);

            putMemorySize.set(0);
            getMemorySize.set(0);
            ackMemorySize.set(0);
            events = null;
            // for (int i = 0; i < events.length; i++) {
            // events[i] = null;
            // }
        } finally {
            lock.unlock();
        }
    }

    // =================== helper method =================

    private long getMinimumGetOrAck() {
        long get = getIndex.get();
        long ack = ackIndex.get();
        return ack <= get ? ack : get;
    }

    /**
     * 查询是否有空位
     */
    private boolean checkFreeSlotAt(final long sequence) {
        final long wrapPoint = sequence - bufferSize;
        final long minPoint = getMinimumGetOrAck();
        if (wrapPoint > minPoint) { // 刚好追上一轮
            return false;
        } else {
            // 在bufferSize模式上，再增加memSize控制
            if (batchMode.isMemorySize()) {
                final long memsize = putMemorySize.get() - ackMemorySize.get();
                if (memsize < bufferSize * bufferMemUnit) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
    }

    /**
     * 检查是否存在需要get的数据,并且数量>=batchSize
     */
    private boolean checkUnGetSlotAt(BinLogEventPosition startPosition, int batchSize) {
        if (batchMode.isItemSize()) {
            long current = getIndex.get();
            long maxAbleSequence = putIndex.get();
            long next = current;
            if (startPosition == null || !startPosition.getEntryPosition().isIncluded()) { // 第一次订阅之后，需要包含一下start位置，防止丢失第一条记录
                next = next + 1;// 少一条数据
            }

            if (current < maxAbleSequence && next + batchSize - 1 <= maxAbleSequence) {
                return true;
            } else {
                return false;
            }
        } else {
            // 处理内存大小判断
            long currentSize = getMemorySize.get();
            long maxAbleSize = putMemorySize.get();

            if (maxAbleSize - currentSize >= batchSize * bufferMemUnit) {
                return true;
            } else {
                return false;
            }
        }
    }

    private long calculateSize(Event event) {
        // 直接返回binlog中的事件大小
        return event.getRawLength();
    }

    private int getIndex(long sequcnce) {
        return (int) sequcnce & indexMask;
    }

    private boolean isDdl(EventType type) {
        return type == EventType.alter || type == EventType.create || type == EventType.erase
                || type == EventType.rename || type == EventType.truncate || type == EventType.create_index
                || type == EventType.delete_index;
    }

    private void profiling(List<Event> events, OP op) {
        long localExecTime = 0L;
        int deltaRows = 0;
        if (events != null && !events.isEmpty()) {
            for (Event e : events) {
                if (localExecTime == 0 && e.getExecuteTime() > 0) {
                    localExecTime = e.getExecuteTime();
                }
                deltaRows += e.getRowsCount();
            }
        }
        switch (op) {
            case PUT:
                putTableRowCount.addAndGet(deltaRows);
                if (localExecTime > 0) {
                    putExecuteTime.lazySet(localExecTime);
                }
                break;
            case GET:
                getTableRowCount.addAndGet(deltaRows);
                if (localExecTime > 0) {
                    getExecuteTime.lazySet(localExecTime);
                }
                break;
            case ACK:
                ackTableRowCount.addAndGet(deltaRows);
                if (localExecTime > 0) {
                    ackExecuteTime.lazySet(localExecTime);
                }
                break;
            default:
                break;
        }
    }

    // ================ setter / getter ==================
    public int getBufferSize() {
        return this.bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public void setBufferMemUnit(int bufferMemUnit) {
        this.bufferMemUnit = bufferMemUnit;
    }

    public void setDdlIsolation(boolean ddlIsolation) {
        this.ddlIsolation = ddlIsolation;
    }

    public boolean isRaw() {
        return raw;
    }

    public void setRaw(boolean raw) {
        this.raw = raw;
    }

    public AtomicLong getPutIndex() {
        return putIndex;
    }

    public AtomicLong getAckIndex() {
        return ackIndex;
    }

    public AtomicLong getPutMemorySize() {
        return putMemorySize;
    }

    public AtomicLong getAckMemorySize() {
        return ackMemorySize;
    }

    public BatchMode getBatchMode() {
        return batchMode;
    }

    public void setBatchMode(BatchMode batchMode) {
        this.batchMode = batchMode;
    }

    public AtomicLong getPutExecuteTime() {
        return putExecuteTime;
    }

    public AtomicLong getGetExecuteTime() {
        return getExecuteTime;
    }

    public AtomicLong getAckExecuteTime() {
        return ackExecuteTime;
    }

    public AtomicLong getPutTableRowCount() {
        return putTableRowCount;
    }

    public AtomicLong getGetTableRowCount() {
        return getTableRowCount;
    }

    public AtomicLong getAckTableRowCount() {
        return ackTableRowCount;
    }

    private enum OP {
        PUT, GET, ACK
    }

}
