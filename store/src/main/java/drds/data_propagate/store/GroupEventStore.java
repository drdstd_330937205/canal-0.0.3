package drds.data_propagate.store;

/**
 * 提供给上层统一的 store 视图，内部则支持多种store混合，并且维持着多个store供上层进行路由
 */
public interface GroupEventStore<T> extends EventStore<T> {

    void addStoreInfo(StoreInfo storeInfo);
}
