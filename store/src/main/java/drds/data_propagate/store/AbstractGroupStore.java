package drds.data_propagate.store;

import com.google.common.collect.MapMaker;
import drds.data_propagate.common.AbstractLifeCycle;

import java.util.Map;


public abstract class AbstractGroupStore<T> extends AbstractLifeCycle implements GroupEventStore<T> {

    protected Map<String, StoreInfo> stores = new MapMaker().makeMap();


    public void addStoreInfo(StoreInfo storeInfo) {

        stores.put(storeInfo.getStoreName(), storeInfo);
    }


}
