package drds.data_propagate.store;

import drds.data_propagate.entry.position.Position;

/**
 * store空间回收机制，信息采集以及控制何时调用{@linkplain EventStore}.cleanUtil()接口
 */
public interface StoreScavenge {

    /**
     * 清理position之前的数据
     */
    void cleanUntil(Position position) throws StoreException;

    /**
     * 删除所有的数据
     */
    void cleanAll() throws StoreException;
}
