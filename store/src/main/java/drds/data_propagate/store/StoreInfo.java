package drds.data_propagate.store;


import lombok.Getter;
import lombok.Setter;

public class StoreInfo {
    @Setter
    @Getter
    private String storeName;
    @Setter
    @Getter
    private String filter;


}
