package drds.data_propagate.store;

/**
 * 批处理模式
 */
public enum BatchMode {

    /**
     * 对象数量
     */
    item_size,

    /**
     * 内存大小
     */
    memory_size;

    public boolean isItemSize() {
        return this == BatchMode.item_size;
    }

    public boolean isMemorySize() {
        return this == BatchMode.memory_size;
    }
}
