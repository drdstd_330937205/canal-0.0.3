package drds.data_propagate.sink;

import drds.data_propagate.common.AbstractLifeCycle;
import drds.data_propagate.filter.EventFilter;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


public abstract class AbstractEventSink<T> extends AbstractLifeCycle implements EventSink<T> {
    @Setter
    @Getter
    protected EventFilter eventFilter;
    @Setter
    @Getter
    protected List<EventDownStreamHandler> eventDownStreamHandlerList = new ArrayList<EventDownStreamHandler>();

    public void addHandler(EventDownStreamHandler eventDownStreamHandler) {
        this.eventDownStreamHandlerList.add(eventDownStreamHandler);
    }

    public void interrupt() {
        // do nothing
    }

}
