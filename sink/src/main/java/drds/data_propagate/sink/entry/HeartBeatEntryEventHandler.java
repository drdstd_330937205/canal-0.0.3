package drds.data_propagate.sink.entry;

import drds.data_propagate.entry.EntryType;
import drds.data_propagate.sink.AbstractEventDownStreamHandler;
import drds.data_propagate.store.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * 处理一下一下heartbeat数据
 */
public class HeartBeatEntryEventHandler extends AbstractEventDownStreamHandler<List<Event>> {

    public List<Event> before(List<Event> eventList) {
        boolean existHeartBeat = false;
        for (Event event : eventList) {
            if (event.getEntryType() == EntryType.heartbeat) {
                existHeartBeat = true;
            }
        }

        if (!existHeartBeat) {
            return eventList;
        } else {
            // 目前heartbeat和其他事件是分离的，保险一点还是做一下检查处理
            List<Event> newEventList = new ArrayList<Event>();
            for (Event event : eventList) {
                if (event.getEntryType() != EntryType.heartbeat) {
                    newEventList.add(event);
                }
            }

            return newEventList;
        }
    }

}
