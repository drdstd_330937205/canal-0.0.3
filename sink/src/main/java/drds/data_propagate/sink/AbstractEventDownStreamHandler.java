package drds.data_propagate.sink;

import drds.data_propagate.common.AbstractLifeCycle;

/**
 * 默认的实现
 */
public class AbstractEventDownStreamHandler<T> extends AbstractLifeCycle implements EventDownStreamHandler<T> {

    public T before(T events) {
        return events;
    }

    public T retry(T events) {
        return events;
    }

    public T after(T events) {
        return events;
    }

}
