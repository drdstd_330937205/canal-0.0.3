package drds.data_propagate.sink.stub;

import drds.data_propagate.entry.position.Position;
import drds.data_propagate.store.Event;
import drds.data_propagate.store.EventStore;
import drds.data_propagate.store.Events;
import drds.data_propagate.store.StoreException;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class DummyEventStore implements EventStore<Event> {

    public void ack(Position position) throws StoreException {

    }

    public Events get(Position start, int batchSize) throws InterruptedException, StoreException {
        return null;
    }

    public Events get(Position start, int batchSize, long timeout, TimeUnit unit)
            throws InterruptedException, StoreException {
        return null;
    }

    public Position getFirstBinLogEventPosition() throws StoreException {
        return null;
    }

    public Position getLatestBinLogEventPosition() throws StoreException {
        return null;
    }

    public void rollback() throws StoreException {

    }

    public Events tryGet(Position start, int batchSize) throws StoreException {
        return null;
    }

    public boolean isStart() {
        return false;
    }

    public void start() {

    }

    public void stop() {

    }

    public void cleanAll() throws StoreException {
    }

    public void cleanUntil(Position position) throws StoreException {

    }

    public void put(Event data) throws InterruptedException, StoreException {
        System.out.println("time:" + data.getExecuteTime());
    }

    public boolean put(Event data, long timeout, TimeUnit unit) throws InterruptedException, StoreException {
        System.out.println("time:" + data.getExecuteTime());
        return true;
    }

    public boolean tryPutOneTime(Event data) throws StoreException {
        System.out.println("time:" + data.getExecuteTime());
        return true;
    }

    public void put(List<Event> datas) throws InterruptedException, StoreException {
        Event data = datas.get(0);
        System.out.println("time:" + data.getExecuteTime());
    }

    public boolean put(List<Event> datas, long timeout, TimeUnit unit) throws InterruptedException, StoreException {
        Event data = datas.get(0);
        System.out.println("time:" + data.getExecuteTime());
        return true;
    }

    public boolean tryPutOneTime(List<Event> datas) throws StoreException {
        Event data = datas.get(0);
        System.out.println("time:" + data.getExecuteTime());
        return true;
    }

}
